# TwinCAT ADS Rust

> SPDX-License-Identifier: [MIT](https://spdx.org/licenses/MIT.html)  
> SPDX-FileCopyrightText: 2021 [Nils Johannsen](mailto:n.johannsen@beckhoff.com)  

Learning Rust by implementing TwinCAT ADS.

- This is only my private playground to learn about Rust.
- This is not any stable crate or official implementation.
- Implementations and interfaces are unstable and likely to change!
- Implementations and examples are tested under [TwinCAT/BSD](https://www.beckhoff.com/TwinCAT-BSD)

## Crates

- `ads-def` generic ADS definitions and traits
	- `AdsConnection` trait to send/recv ADS requests/responses
	- `AdsClient` trait for ADS clients
- `ads-lib` ADS over TwinCAT Router (FFI to to TcAdsDll)
	- `AdsLibClient` ADS client interface
- `ads-tcp` ADS over TCP socket
	- `AdsTcpConnection` stream to send/recv ADS over TCP socket
	- `AdsTcpClient` ADS client over TCP socket
- `ads-tls` ADS over TLS socket
	- `AdsTlsConnection` stream to send/recv ADS over TLS socket
	- `AdsTlsClient` ADS client over TLS socket
- `ads-disco` ADS discovery over UDP socket
	- `AdsDiscoClient` ADS discovery client over UDP socket
- `ads-devices` ADS client impelemtations to TwinCAT devices and services
	- `AdsSystemClient` ADS client to TwinCAT System Service
	- `AdsRouterClient` ADS client to TwinCAT Router
	- `AdsSymbolClient` ADS client for symbols access to e.g. TwinCAT PLC Instances
	- `AdsFileClient` ADS client to TwinCAT File Access
	- `AdsMdpClient` ADS client to MDP Service
	- `AdsSumReadClient`, `AdsSumWriteClient` and `AdsSumReadWriteClient` to proceed sum commands
- `ads-client` collection of the ADS clients over TCP, TLS, ...

#### outstanding...

- `ads-server` ADS server port over TwinCAT Router (FFI to TcAmsServer)
- `mdp-def` generic MDP definitions and traits
- `mdp-lib` MDP client to local MDP service (FFI to MdpApi)

## References

- [Beckhoff: ADS Documentation](https://infosys.beckhoff.com/content/1033/tc3_ads_intro/index.html)
- [Beckhoff: ADS Client in C++](https://github.com/Beckhoff/ADS)
- [bidinzky: AdsServer](https://github.com/bidinzky/AdsServer)
- [birkenfeld: Rust crate for Beckhoff ADS](https://github.com/birkenfeld/ads-rs)
- [birkenfeld: Beckhoff ADS forwarder in rust](https://github.com/birkenfeld/ads_forwarder_rs)
- [mattsse: Beckhoff ADS protocol in rust](https://github.com/mattsse/rust-ads)
- [windelbouwan: ADS Client in rust](https://github.com/windelbouwman/rads)

