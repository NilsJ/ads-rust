// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)] // TODO
#![allow(non_camel_case_types)]
use std::net::Ipv4Addr;

use ads_def::*;

// IdxGrp = SYSTEMSERVICE_IPHELPERAPI (701)
//	IdxOff = IPHELPERAPI_SENDARP (5)
//	IdxOff = IPHELPERAPI_IPADDRTABLE (6)
//	IdxOff = IPHELPERAPI_IPADDRBYADAPTERNAME (7)
//	IdxOff = IPHELPERAPI_IPADDRBYADAPTERNAMEWCHAR (8)
//	IdxOff = IPHELPERAPI_GETIPFORWARDTABLE (9)
//	IdxOff = IPHELPERAPI_SETIPFORWARDENTRY (10)
//	IdxOff = IPHELPERAPI_DELETEIPFORWARDENTRY (11)
//	IdxOff = IPHELPERAPI_ADDIPADDRESS (12)
//	IdxOff = IPHELPERAPI_SETIPADDRESS (13)
// IdxGrp = SYSTEMSERVICE_IPADDRINFO (703), IdxOff = 0

pub struct AdsSystemIpClient {
    client: AdsClientRef,
    target: AdsAddr,
}

impl AdsSystemIpClient {
    pub fn new(client: AdsClientRef, target: AdsNetId) -> Self {
        AdsSystemIpClient { client, target: AdsAddr::new(target, 10000) }
    }

    pub fn get_state(&mut self) -> AdsResult<AdsState> {
        self.client.read_state(self.target).map(|(a, _)| a)
    }

    pub fn read_host_name(&mut self) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_IPHOSTNAME
        self.client.read_utf8(self.target, 702, 0, 128)
    }

    pub fn read_host_addr(&mut self) -> AdsResult<Ipv4Addr> {
        // IdxGrp = SYSTEMSERVICE_IPHELPERAPI, IdxOff = IPHELPERAPI_IPADDRBYHOSTNAME
        let hostname = self.read_host_name()?;
        let write_buf = AdsBuffer::from_utf8(&hostname);
        let mut read_buf = AdsBuffer::with_size(4);
        self.client.read_write(self.target, 701, 4, write_buf.as_slice(), read_buf.as_mut_slice())?;
        Ok(Ipv4Addr::new(read_buf.dec_into_u8()?, read_buf.dec_into_u8()?, read_buf.dec_into_u8()?, read_buf.dec_into_u8()?))
    }

    pub fn read_host_netid(&mut self) -> AdsResult<AdsNetId> {
        // IdxGrp = SYSTEMSERVICE_IPHELPERAPI, IdxOff = IPHELPERAPI_NETIDBYHOSTNAME
        let hostname = self.read_host_name()?;
        let write_buf = AdsBuffer::from_utf8(&hostname);
        let mut read_buf = AdsBuffer::with_size_of::<AdsNetId>();
        self.client.read_write(self.target, 701, 4, write_buf.as_slice(), read_buf.as_mut_slice())?;
        read_buf.dec_into_type::<AdsNetId>()
    }

    pub fn read_adapter_best(&mut self, addr: &Ipv4Addr) -> AdsResult<Ipv4Addr> {
        // IdxGrp = SYSTEMSERVICE_IPHELPERAPI, IdxOff = IPHELPERAPI_BESTINTERFACE
        let mut write_buf = AdsBuffer::with_capacity(4);
        for n in addr.octets() {
            write_buf.enc_u8(n)?;
        }
        let mut read_buf = AdsBuffer::with_size(4);
        self.client.read_write(self.target, 701, 2, write_buf.as_slice(), read_buf.as_mut_slice())?;
        Ok(Ipv4Addr::new(read_buf.dec_into_u8()?, read_buf.dec_into_u8()?, read_buf.dec_into_u8()?, read_buf.dec_into_u8()?))
    }

    pub fn read_adapter_list(&mut self) -> AdsResult<Vec<AdsAdapterInfo>> {
        // IdxGrp = SYSTEMSERVICE_IPHELPERAPI, IdxOff = IPHELPERAPI_ADAPTERSINFO
        let read_len = self.client.read_u32(self.target, 701, 1)?;
        let mut read_buf = AdsBuffer::with_size(read_len as usize);
        self.client.read(self.target, 701, 1, read_buf.as_mut_slice())?;

        let mut adapters = Vec::<AdsAdapterInfo>::new();
        loop {
            let info = read_buf.dec_into_type::<AdsIpAdapterInfo>()?;
            let skip = info.next as isize - info.size() as isize;
            adapters.push(AdsAdapterInfo::from(info));
            if skip > 0 {
                read_buf.skip(skip as usize);
            } else {
                break;
            }
        }
        Ok(adapters)
    }
}

#[derive(Default, Debug, Clone)]
pub struct AdsAdapterInfo {
    pub name: String,
    pub description: String,
    pub address: String,
    pub subnetmask: String,
    pub gateway: String,
    //TMacAddress mMac;
    //TIpAddress mDhcp;
    //TIpAddress mDns1;
    //TIpAddress mDns2;
}

impl From<AdsIpAdapterInfo> for AdsAdapterInfo {
    fn from(info: AdsIpAdapterInfo) -> Self {
        AdsAdapterInfo {
            name: info.name,
            description: info.description,
            address: info.nic_addr.addr,
            subnetmask: info.nic_addr.mask,
            gateway: info.nic_gateway.addr,
        }
    }
}

#[derive(Default, Debug, Clone)]
struct AdsIpAdapterAddr {
    addr: String,
    mask: String,
}

impl AdsCodable for AdsIpAdapterAddr {
    fn size(&self) -> usize {
        40
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        todo!()
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.skip(4); // next
        buf.dec_utf8_len(&mut self.addr, 16)?;
        buf.dec_utf8_len(&mut self.mask, 16)?;
        buf.skip(4); // context
        Ok(())
    }
}

#[derive(Default, Debug, Clone)]
struct AdsIpAdapterInfo {
    next: u32, // offset to next entry, if zero then it's the last entry
    combo: u32,
    name: String,
    description: String,
    mac_len: u32,
    mac_addr: [u8; 8],
    nic_index: u32,
    nic_type: u32,
    dhcp: u32,
    current: u32,
    nic_addr: AdsIpAdapterAddr,
    nic_gateway: AdsIpAdapterAddr,
    nic_dhcp: AdsIpAdapterAddr,
    dns: i32,
    nic_dns1: AdsIpAdapterAddr,
    nic_dns2: AdsIpAdapterAddr,
    lease_obtained: u32,
    lease_expires: u32,
}

impl AdsCodable for AdsIpAdapterInfo {
    fn size(&self) -> usize {
        640
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        todo!()
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.next)?;
        buf.dec_u32(&mut self.combo)?;
        buf.dec_utf8_len(&mut self.name, 260)?;
        buf.dec_utf8_len(&mut self.description, 132)?;
        buf.dec_u32(&mut self.mac_len)?;
        buf.dec_slice(&mut self.mac_addr)?;
        buf.dec_u32(&mut self.nic_index)?;
        buf.dec_u32(&mut self.nic_type)?;
        buf.dec_u32(&mut self.dhcp)?;
        buf.dec_u32(&mut self.current)?;
        buf.dec_type(&mut self.nic_addr)?;
        buf.dec_type(&mut self.nic_gateway)?;
        buf.dec_type(&mut self.nic_dhcp)?;
        buf.dec_i32(&mut self.dns)?;
        buf.dec_type(&mut self.nic_dns1)?;
        buf.dec_type(&mut self.nic_dns2)?;
        buf.dec_u32(&mut self.lease_obtained)?;
        buf.dec_u32(&mut self.lease_expires)?;
        Ok(())
    }
}
