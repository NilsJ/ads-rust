// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
#![allow(dead_code)]
use ads_def::*;
use std::mem::size_of;

pub struct AdsSymbolClient {
    client: AdsClientRef,
    target: AdsAddr,
}

impl AdsSymbolClient {
    pub fn new(client: AdsClientRef, target: AdsAddr) -> Self {
        AdsSymbolClient { client, target }
    }

    pub fn get_state(&mut self) -> AdsResult<AdsState> {
        self.client.read_state(self.target).map(|(a, _)| a)
    }

    pub fn set_start(&mut self) -> AdsResult<()> {
        self.client.write_control(self.target, AdsState::ReStart, 0, &[])
    }

    pub fn set_config(&mut self) -> AdsResult<()> {
        self.client.write_control(self.target, AdsState::ReConfig, 0, &[])
    }

    pub fn read_version(&mut self) -> AdsResult<u8> {
        // ADSIGRP_SYM_VERSION
        self.client.read_u8(self.target, 0xf008, 0)
    }

    fn read_upload_info(&mut self) -> AdsResult<(usize, usize)> {
        // ADSIGRP_SYM_UPLOADINFO2
        let mut upload_info = AdsSymbolUploadInfo::default();
        let mut read_buf = AdsBuffer::with_size(upload_info.size());
        self.client.read(self.target, 0xf00f, 0, read_buf.as_mut_slice())?;
        read_buf.dec_type(&mut upload_info)?;
        Ok((upload_info.symbols_size as usize, upload_info.types_size as usize))
    }

    pub fn read_symbol_list(&mut self) -> AdsResult<Vec<AdsSymbolInfo>> {
        // ADSIGRP_SYM_UPLOAD
        let (size, _) = self.read_upload_info()?;
        let mut read_buf = AdsBuffer::with_size(size);
        self.client.read(self.target, 0xf00b, 0, read_buf.as_mut_slice())?;
        let mut symbols = Vec::new();
        while read_buf.remain() != 0 {
            let info = read_buf.dec_into::<AdsSymbolInfo>()?;
            symbols.push(info);
        }
        Ok(symbols)
    }

    pub fn read_type_list(&mut self) -> AdsResult<Vec<AdsTypeInfo>> {
        // ADSIGRP_SYM_DT_UPLOAD
        let (_, size) = self.read_upload_info()?;
        let mut read_buf = AdsBuffer::with_size(size);
        self.client.read(self.target, 0xf00e, 0, read_buf.as_mut_slice())?;
        let mut types = Vec::new();
        while read_buf.remain() != 0 {
            let info = read_buf.dec_into::<AdsTypeInfo>()?;
            types.push(info);
        }
        Ok(types)
    }

    pub fn read_symbol_info(&mut self, name: &str) -> AdsResult<AdsSymbolInfo> {
        // IdxGrp = ADSIGRP_SYM_INFOBYNAMEEX
        let write_buf = AdsBuffer::from_utf8(name);
        let mut read_buf = AdsBuffer::with_size(1024);
        let readen = self.client.read_write(self.target, 0xf009, 0, write_buf.as_slice(), read_buf.as_mut_slice())?;
        read_buf.resize(readen);
        read_buf.dec_into_type::<AdsSymbolInfo>()
    }

    pub fn read_type_info(&mut self, name: &str) -> AdsResult<AdsTypeInfo> {
        // IdxGrp = ADSIGRP_SYM_DT_INFOBYNAMEEX
        let write_buf = AdsBuffer::from_utf8(name);
        let mut read_buf = AdsBuffer::with_size(2048);
        let readen = self.client.read_write(self.target, 0xf011, 0, write_buf.as_slice(), read_buf.as_mut_slice())?;
        read_buf.resize(readen);
        read_buf.dec_into_type::<AdsTypeInfo>()
    }

    pub fn create_handle(&mut self, name: &str) -> AdsResult<u32> {
        // IdxGrp = ADSIGRP_SYM_HNDBYNAME
        let write_buf = AdsBuffer::from_utf8(name);
        let mut read_buf = AdsBuffer::with_size_of::<u32>();
        self.client.read_write(self.target, 0xf003, 0, write_buf.as_slice(), read_buf.as_mut_slice())?;
        read_buf.dec_into_u32()
    }

    pub fn release_handle(&mut self, handle: u32) -> AdsResult<()> {
        // IdxGrp = ADSIGRP_SYM_RELEASEHND
        if handle == 0 {
            return Ok(());
        }
        let buf = AdsBuffer::from_u32(handle);
        self.client.write(self.target, 0xf006, 0, buf.as_slice())
    }

    pub fn read_by_handle(&mut self, handle: u32, data: &mut [u8]) -> AdsResult<usize> {
        // IdxGrp = ADSIGRP_SYM_VALBYHND
        self.client.read(self.target, 0xf005, handle, data)
    }

    pub fn write_by_handle(&mut self, handle: u32, data: &[u8]) -> AdsResult<()> {
        // IdxGrp = ADSIGRP_SYM_VALBYHND
        self.client.write(self.target, 0xf005, handle, data)
    }

    pub fn read_by_name(&mut self, name: &str, data: &mut [u8]) -> AdsResult<usize> {
        // IdxGrp = ADSIGRP_SYM_VALBYNAME
        let write_buf = AdsBuffer::from_utf8(name);
        self.client.read_write(self.target, 0xf004, 0, write_buf.as_slice(), data)
    }
}

#[allow(non_snake_case)]
pub mod AdsTypeId {
    pub const Void: u32 = 0;
    pub const Int16: u32 = 2;
    pub const Int32: u32 = 3;
    pub const Single: u32 = 4;
    pub const Double: u32 = 5;
    pub const Char: u32 = 16;
    pub const Byte: u32 = 17;
    pub const UInt16: u32 = 18;
    pub const UInt32: u32 = 19;
    pub const Int64: u32 = 20;
    pub const UInt64: u32 = 21;
    pub const String: u32 = 30;
    pub const WString: u32 = 31;
    pub const Bool: u32 = 33; // Bit
    pub const BigType: u32 = 65;
    pub const MaxType: u32 = 67;
}

#[allow(non_snake_case)]
pub mod AdsSymbolFlags {
    pub const None: u32 = 0x00000000;
    pub const Persistent: u32 = 0x00000001;
    pub const BitValue: u32 = 0x00000002;
    pub const ReferenceTo: u32 = 0x00000004;
    pub const TypeGuid: u32 = 0x00000008;
    pub const InterfactPtr: u32 = 0x00000010;
    pub const ReadOnly: u32 = 0x00000020;
    pub const MethodAccess: u32 = 0x00000040;
    pub const MethodRef: u32 = 0x00000080;
    pub const ContextMask: u32 = 0x00000F00;
    pub const Attributes: u32 = 0x00001000;
    pub const Static: u32 = 0x0002000;
    pub const InitOnReset: u32 = 0x00004000;
    pub const ExtendedFlags: u32 = 0x00008000;
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct AdsSymbolInfo {
    pub grp: u32,
    pub off: u32,
    pub size: u32,
    pub type_id: u32,
    pub flags: u32,
    pub name: String,
    pub type_name: String,
    pub comment: String,
    pub type_guid: Option<AdsGuid>,
    pub attr_items: Vec<(String, String)>,
}

impl AdsCodable for AdsSymbolInfo {
    fn size(&self) -> usize {
        1024
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        panic!("AdsSymbolInfo does not support 'encode'!");
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        // AdsSymbolEntry
        let len = buf.dec_into_u32()? - 4;
        let end = buf.pos() + len as usize;
        buf.dec_u32(&mut self.grp)?;
        buf.dec_u32(&mut self.off)?;
        buf.dec_u32(&mut self.size)?;
        buf.dec_u32(&mut self.type_id)?;
        buf.dec_u32(&mut self.flags)?;
        let name_len = buf.dec_into_u16()?;
        let type_len = buf.dec_into_u16()?;
        let comment_len = buf.dec_into_u16()?;
        buf.dec_utf8_len(&mut self.name, 1 + name_len as usize)?;
        buf.dec_utf8_len(&mut self.type_name, 1 + type_len as usize)?;
        buf.dec_utf8_len(&mut self.comment, 1 + comment_len as usize)?;
        if (self.flags & AdsSymbolFlags::TypeGuid) != 0 {
            let mut type_guid = AdsGuid::default();
            buf.dec_type(&mut type_guid)?;
            self.type_guid = Some(type_guid);
        }
        if (self.flags & AdsSymbolFlags::Attributes) != 0 {
            let attr_cnt = buf.dec_into_u16()?;
            for _ in 0..attr_cnt {
                let attr_name_len = buf.dec_into_u8()?;
                let attr_value_len = buf.dec_into_u8()?;
                let attr_name = buf.dec_into_utf8_len(1 + attr_name_len as usize)?;
                let attr_value = buf.dec_into_utf8_len(1 + attr_value_len as usize)?;
                self.attr_items.push((attr_name, attr_value));
            }
        }
        // ADS_UINT32 flags2; // if ADSSYMBOLFLAG_EXTENDED_FLAGS is set
        // AdsRefactorInfo refInfo[]; // if ADSSYMBOLFLAG2_OLDNAMES is set one ore more AdsRefactorInfo
        if buf.pos() < end {
            buf.skip(end - buf.pos());
        }
        Ok(())
    }
}

#[allow(non_snake_case)]
pub mod AdsTypeFlags {
    pub const None: u32 = 0x00000000;
    pub const DataType: u32 = 0x00000001;
    pub const DataItem: u32 = 0x00000002;
    pub const ReferenceTo: u32 = 0x00000004;
    pub const MethodDeref: u32 = 0x00000008;
    pub const Oversample: u32 = 0x00000010;
    pub const BitValues: u32 = 0x00000020;
    pub const PropItem: u32 = 0x00000040;
    pub const TypeGuid: u32 = 0x00000080;
    pub const Persistent: u32 = 0x00000100;
    pub const CopyMask: u32 = 0x00000200;
    pub const TComInterfacePtr: u32 = 0x00000400;
    pub const MethodInfos: u32 = 0x00000800;
    pub const Attributes: u32 = 0x000001000;
    pub const EnumInfos: u32 = 0x00002000;
    pub const Aligned: u32 = 0x00010000;
    pub const Static: u32 = 0x00020000;
    pub const SpLevels: u32 = 0x00040000;
    pub const IgnorePersist: u32 = 0x00080000;
    pub const AnySizeArray: u32 = 0x00100000;
    pub const PersistantType: u32 = 0x00200000;
    pub const InitOnResult: u32 = 0x00400000;
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct AdsTypeInfo {
    pub version: u32,
    pub hash: u32,
    pub hash_base: u32,
    pub size: u32,
    pub offset: u32,
    pub alias: u32,
    pub flags: u32,
    pub name: String,
    pub type_name: String,
    pub comment: String,
    pub array_dim: u16,
    pub sub_cnt: u16,
    pub guid: AdsGuid,
    pub array_items: Vec<(i32, u32)>,
    pub sub_items: Vec<AdsTypeInfo>,
    pub meth_items: Vec<(String, String)>,
    pub attr_items: Vec<(String, String)>,
    pub enum_items: Vec<(String, usize)>,
}

impl AdsCodable for AdsTypeInfo {
    fn size(&self) -> usize {
        2048
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        panic!("AdsTypeInfo does not support 'encode'!");
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        // AdsDatatypeEntry
        let len = buf.dec_into_u32()? - 4;
        let end = buf.pos() + len as usize;
        buf.dec_u32(&mut self.version)?;
        buf.dec_u32(&mut self.hash)?;
        buf.dec_u32(&mut self.hash_base)?;
        buf.dec_u32(&mut self.size)?;
        buf.dec_u32(&mut self.offset)?;
        buf.dec_u32(&mut self.alias)?;
        buf.dec_u32(&mut self.flags)?;
        let name_len = buf.dec_into_u16()?;
        let type_len = buf.dec_into_u16()?;
        let comment_len = buf.dec_into_u16()?;
        buf.dec_u16(&mut self.array_dim)?;
        buf.dec_u16(&mut self.sub_cnt)?;
        buf.dec_utf8_len(&mut self.name, 1 + name_len as usize)?;
        buf.dec_utf8_len(&mut self.type_name, 1 + type_len as usize)?;
        buf.dec_utf8_len(&mut self.comment, 1 + comment_len as usize)?;
        if self.array_dim != 0 {
            for _ in 0..self.array_dim {
                // AdsDatatypeArrayInfo
                let bound = buf.dec_into_i32()?;
                let elements = buf.dec_into_u32()?;
                self.array_items.push((bound, elements));
            }
        }
        if self.sub_cnt != 0 {
            for _ in 0..self.sub_cnt {
                // AdsDatatypeEntry
                let sub_item = buf.dec_into_type::<AdsTypeInfo>()?;
                self.sub_items.push(sub_item);
            }
        }
        if (self.flags & AdsTypeFlags::TypeGuid) != 0 {
            buf.dec_type(&mut self.guid)?;
        }
        // ADS_UINT8 copyMask[]; // "size" bytes containing 0xff or 0x00 - 0x00 means ignore byte (ADSIGRP_SYM_VALBYHND_WITHMASK)
        if (self.flags & AdsTypeFlags::MethodInfos) != 0 {
            let meth_cnt = buf.dec_into_u16()?;
            for _ in 0..meth_cnt {
                // AdsMethodEntry
                let meth_len = buf.dec_into_u32()? - 4;
                let meth_end = buf.pos() + meth_len as usize;
                buf.skip(4); // ADS_UINT32 version; // version of datatype structure
                buf.skip(4); // ADS_UINT32 vTableIndex; // vTable index of this method
                buf.skip(4); // ADS_UINT32 returnSize; // size of datatype ( in bytes )
                buf.skip(4); // ADS_UINT32 returnAlignSize; // size of biggest element for alignment
                buf.skip(4); // ADS_UINT32 reserved; //
                buf.skip(16); // GUID returnTypeGuid;//
                buf.skip(4); // ADS_UINT32 returnDataType;// adsDataType of symbol (if alias)
                buf.skip(4); // ADS_UINT32 flags; //
                let meth_name_len = buf.dec_into_u16()?;
                let meth_type_len = buf.dec_into_u16()?;
                let meth_comment_len = buf.dec_into_u16()?;
                let meth_para_cnt = buf.dec_into_u16()?;
                let meth_name = buf.dec_into_utf8_len(1 + meth_name_len as usize)?;
                let meth_type = buf.dec_into_utf8_len(1 + meth_type_len as usize)?;
                let _meth_comment = buf.dec_into_utf8_len(1 + meth_comment_len as usize)?;
                for _ in 0..meth_para_cnt {
                    // AdsMethodParaEntry
                    let para_len = buf.dec_into_u32()? - 4;
                    let para_end = buf.pos() + para_len as usize;
                    buf.skip(4); // ADS_UINT32 size; // size of datatype ( in bytes )
                    buf.skip(4); // ADS_UINT32 alignSize; // size of biggest element for alignment
                    buf.skip(4); // ADS_UINT32 dataType; // adsDataType of symbol (if alias)
                    buf.skip(4); // ADS_UINT32 flags; //
                    buf.skip(4); // ADS_UINT32 reserved; //
                    buf.skip(16); // GUID typeGuid; //
                    buf.skip(2); // ADS_UINT16 lengthIsPara; // index-1 of corresponding parameter with length info - 0 = no para, 1 = first para...
                    let para_name_len = buf.dec_into_u16()?;
                    let para_type_len = buf.dec_into_u16()?;
                    let para_comment_len = buf.dec_into_u16()?;
                    let _para_name = buf.dec_into_utf8_len(1 + para_name_len as usize)?;
                    let _para_type = buf.dec_into_utf8_len(1 + para_type_len as usize)?;
                    let _para_comment = buf.dec_into_utf8_len(1 + para_comment_len as usize)?;
                    if buf.pos() < para_end {
                        buf.skip(para_end - buf.pos());
                    }
                }
                self.meth_items.push((meth_name, meth_type));
                if buf.pos() < meth_end {
                    buf.skip(meth_end - buf.pos());
                }
            }
        }
        if (self.flags & AdsTypeFlags::Attributes) != 0 {
            let attr_cnt = buf.dec_into_u16()?;
            for _ in 0..attr_cnt {
                let attr_name_len = buf.dec_into_u8()?;
                let attr_value_len = buf.dec_into_u8()?;
                let attr_name = buf.dec_into_utf8_len(1 + attr_name_len as usize)?;
                let attr_value = buf.dec_into_utf8_len(1 + attr_value_len as usize)?;
                self.attr_items.push((attr_name, attr_value));
            }
        }
        if (self.flags & AdsTypeFlags::EnumInfos) != 0 {
            let enum_cnt = buf.dec_into_u16()?;
            for _ in 0..enum_cnt {
                let enum_name_len = buf.dec_into_u8()?;
                let enum_name = buf.dec_into_utf8_len(1 + enum_name_len as usize)?;
                let enum_value: usize;
                match self.size {
                    1 => enum_value = buf.dec_into_u8()? as usize,
                    2 => enum_value = buf.dec_into_u16()? as usize,
                    4 => enum_value = buf.dec_into_u32()? as usize,
                    8 => enum_value = buf.dec_into_u64()? as usize,
                    _ => panic!("Undefined enum size!"),
                }
                self.enum_items.push((enum_name, enum_value));
            }
        }
        // AdsRefactorInfo refInfo[]; // if ADSDATATYPEFLAG_REFACTORINFO is set one ore more AdsRefactorInfo
        // AdsSpLevelEntry access; // if ADSDATATYPEFLAG_SPLEVELS is set read and write access rights
        if buf.pos() < end {
            buf.skip(end - buf.pos());
        }
        Ok(())
    }
}

#[derive(Default)]
struct AdsSymbolUploadInfo {
    symbols_cnt: u32,
    symbols_size: u32,
    types_cnt: u32,
    types_size: u32,
    symbols_dyn_max: u32,
    symbols_dyn_use: u32,
    symbols_dyn_inv: u32,
    code_page: u32,
    target_x64: bool, //: 1
    basetypes_inc: bool, //: 1
                      // ULONG		reserved1				//: 30
                      // ULONG		reseverd2[7]
}

impl AdsCodable for AdsSymbolUploadInfo {
    fn size(&self) -> usize {
        16 * size_of::<u32>()
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        panic!("AdsSymbolUploadInfo does not support 'encode'!");
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.symbols_cnt)?;
        buf.dec_u32(&mut self.symbols_size)?;
        buf.dec_u32(&mut self.types_cnt)?;
        buf.dec_u32(&mut self.types_size)?;
        buf.dec_u32(&mut self.symbols_dyn_max)?;
        buf.dec_u32(&mut self.symbols_dyn_use)?;
        buf.dec_u32(&mut self.symbols_dyn_inv)?;
        buf.dec_u32(&mut self.code_page)?;
        let flags = buf.dec_into_u32()?;
        self.target_x64 = (flags & 0x0001) != 0;
        self.basetypes_inc = (flags & 0x0002) != 0;
        buf.skip(7 * size_of::<u32>());
        Ok(())
    }
}
