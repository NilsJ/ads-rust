// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)] // TODO
#![allow(non_camel_case_types)]
use ads_def::*;

pub struct AdsSystemClient {
    client: AdsClientRef,
    target: AdsAddr,
}

impl AdsSystemClient {
    pub fn new(client: AdsClientRef, target: AdsNetId) -> Self {
        AdsSystemClient { client, target: AdsAddr::new(target, 10000) }
    }

    pub fn get_state(&mut self) -> AdsResult<AdsState> {
        self.client.read_state(self.target).map(|(a, _)| a)
    }

    pub fn set_start(&mut self) -> AdsResult<()> {
        self.client.write_control(self.target, AdsState::ReStart, 0, &[])
    }

    pub fn set_config(&mut self) -> AdsResult<()> {
        self.client.write_control(self.target, AdsState::ReConfig, 0, &[])
    }

    pub fn set_reboot(&mut self) -> AdsResult<()> {
        self.client.write_control(self.target, AdsState::Shutdown, 1, &[])
    }

    pub fn set_reboot_delayed(&mut self, secs: u32) -> AdsResult<()> {
        let buf = AdsBuffer::from_u32(secs);
        self.client.write_control(self.target, AdsState::Shutdown, 1, buf.as_slice())
    }

    pub fn set_shutdown(&mut self) -> AdsResult<()> {
        self.client.write_control(self.target, AdsState::Shutdown, 0, &[])
    }

    pub fn set_shutdown_delayed(&mut self, secs: u32) -> AdsResult<()> {
        let buf = AdsBuffer::from_u32(secs);
        self.client.write_control(self.target, AdsState::Shutdown, 0, buf.as_slice())
    }

    pub fn abort_shutdown(&mut self) -> AdsResult<()> {
        self.client.write_control(self.target, AdsState::PowerGood, 0, &[])
    }

    pub fn product_version(&mut self) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_PRODUCT_VERSION
        let mut buf = AdsBuffer::with_size(16);
        self.client.read(self.target, 160, 0, buf.as_mut_slice())?;
        let v2 = buf.dec_into_u16()?;
        let v1 = buf.dec_into_u16()?;
        let v4 = buf.dec_into_u16()?;
        let v3 = buf.dec_into_u16()?;
        Ok(format!("{}.{}.{}.{}", v1, v2, v3, v4))
    }

    pub fn get_time_utc(&mut self) -> AdsResult<u64> {
        // IdxGrp = SYSTEMSERVICE_TIMESERVICES, IdxOff = TIMESERVICE_DATEANDTIME_GMT
        self.client.read_u64(self.target, 400, 7)
    }

    pub fn set_time_utc(&mut self, time: u64) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_TIMESERVICES, IdxOff = TIMESERVICE_DATEANDTIME_GMT
        self.client.write_u64(self.target, 400, 7, time)
    }

    pub fn get_time_local(&mut self) -> AdsResult<u64> {
        // IdxGrp = SYSTEMSERVICE_TIMESERVICES, IdxOff = TIMESERVICE_DATEANDTIME_100NS
        self.client.read_u64(self.target, 400, 5)
    }

    pub fn set_time_local(&mut self, time: u64) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_TIMESERVICES, IdxOff = TIMESERVICE_DATEANDTIME_100NS
        self.client.write_u64(self.target, 400, 5, time)
    }

    pub fn system_state(&mut self) -> AdsResult<AdsSystemState> {
        // IdxGrp = SYSTEMSERVICE_SYSSERV_STATE
        self.client.read_type(self.target, 0xf0, 0)
    }

    fn find_tag<'a>(source: &'a str, tag: &'a str) -> Option<(String, usize)> {
        let tag0 = format!("<{}>", tag);
        let pos0 = source.find(&tag0)? + tag0.len();

        let tag1 = format!("</{}>", tag);
        let pos1 = source[pos0..].find(&tag1)? + pos0;

        Some((source[pos0..pos1].to_string(), pos1 + tag1.len()))
    }

    pub fn target_info(&mut self) -> AdsResult<AdsSystemTargetInfo> {
        // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_QUERYXMLINFO
        let len = self.client.read_u32(self.target, 700, 1)?;
        let source = self.client.read_utf8(self.target, 700, 1, len as usize)?;
        let mut src0 = source.as_str();
        let mut info = AdsSystemTargetInfo::default();

        if let Some((val, pos)) = Self::find_tag(src0, "TargetType") {
            info.target_type = val;
            src0 = &src0[pos..];
        }
        if let Some((val, pos)) = Self::find_tag(src0, "TargetVersion") {
            let mut src1 = val.as_str();
            if let Some((val, pos)) = Self::find_tag(src1, "Version") {
                info.twincat_version = val;
                src1 = &src1[pos..];
            }
            if let Some((val, pos)) = Self::find_tag(src1, "Revision") {
                info.twincat_revision = val;
                src1 = &src1[pos..];
            }
            if let Some((val, _pos)) = Self::find_tag(src1, "Build") {
                info.twincat_build = val;
            }
            src0 = &src0[pos..];
        }
        if let Some((val, pos)) = Self::find_tag(src0, "TargetFeatures") {
            let mut src1 = val.as_str();
            if let Some((val, pos)) = Self::find_tag(src1, "Level") {
                info.twincat_level = val;
                src1 = &src1[pos..];
            }
            if let Some((val, _pos)) = Self::find_tag(src1, "NetId") {
                info.net_id = val;
            }
            src0 = &src0[pos..];
        }
        if let Some((val, pos)) = Self::find_tag(src0, "Hardware") {
            let mut src1 = val.as_str();
            if let Some((val, pos)) = Self::find_tag(src1, "Model") {
                info.hardware_model = val;
                src1 = &src1[pos..];
            }
            if let Some((val, pos)) = Self::find_tag(src1, "SerialNo") {
                info.hardware_serial = val;
                src1 = &src1[pos..];
            }
            if let Some((val, pos)) = Self::find_tag(src1, "CPUVersion") {
                info.hardware_version = val;
                src1 = &src1[pos..];
            }
            if let Some((val, pos)) = Self::find_tag(src1, "Date") {
                info.hardware_date = val;
                src1 = &src1[pos..];
            }
            if let Some((val, _pos)) = Self::find_tag(src1, "CPUArchitecture") {
                if let Ok(idx) = val.parse::<u32>() {
                    info.hardware_cpu = match idx {
                        0 => "INTELx86",
                        1 => "MIPS",
                        2 => "ALPHA",
                        3 => "PPC",
                        4 => "SHX",
                        5 => "ARM",
                        6 => "IA64",
                        7 => "ALPHA64",
                        8 => "MSIL",
                        9 => "AMDx64",
                        10 => "IA32_ON_WIN64",
                        _ => "",
                    }
                    .to_string();
                }
            }
            src0 = &src0[pos..];
        }
        if let Some((val, _pos)) = Self::find_tag(src0, "OsImage") {
            let mut src1 = val.as_str();
            if let Some((val, pos)) = Self::find_tag(src1, "ImageDevice") {
                info.image_device = val;
                src1 = &src1[pos..];
            }
            if let Some((val, pos)) = Self::find_tag(src1, "ImageVersion") {
                info.image_version = val;
                src1 = &src1[pos..];
            }
            if let Some((val, pos)) = Self::find_tag(src1, "ImageLevel") {
                info.image_level = val;
                src1 = &src1[pos..];
            }
            if let Some((val, pos)) = Self::find_tag(src1, "OsName") {
                info.image_os_name = val;
                src1 = &src1[pos..];
            }
            if let Some((val, _pos)) = Self::find_tag(src1, "OsVersion") {
                info.image_os_version = val;
            }
        }

        Ok(info)
    }

    pub fn target_type(&mut self) -> AdsResult<&str> {
        // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_TARGETTYPE
        let v = self.client.read_u32(self.target, 700, 2)?;
        match v {
            0 => Ok("PC"),
            1 => Ok("CX"),
            2 => Ok("BC"),
            3 => Ok("BX"),
            _ => Ok("?"),
        }
    }

    pub fn target_platform(&mut self) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_PLATFORM
        self.client.read_utf8(self.target, 700, 4, 64)
    }

    pub fn target_project_guid(&mut self) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_PROJECTGUID
        let guid: AdsGuid = self.client.read_type(self.target, 700, 5)?;
        Ok(guid.to_string())
    }

    pub fn target_project_version(&mut self) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_PROJECTVERSIONGUID
        let guid: AdsGuid = self.client.read_type(self.target, 700, 6)?;
        Ok(guid.to_string())
    }

    pub fn target_project_name(&mut self) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_PROJECTNAME
        let len = self.client.read_u32(self.target, 700, 7)?;
        self.client.read_utf8(self.target, 700, 7, len as usize)
    }

    pub fn target_fingerprint(&mut self) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_SELFSIGNEDCERTFP
        self.client.read_utf8(self.target, 700, 9, 129)
    }

    /* NOT SUPPORTED!
    // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_FILEVERSION
    std::string CAdsTargetClient::TargetVersion() const
    {
        auto nLength = _oClient.ReadValue<uint32_t>(_mtargetess, 700, 3);
        if (nLength == 0) { throw std::runtime_error("Invalid length readen!"); }
        return _oClient.ReadString(_mtargetess, 700, 3, nLength);
    }
    */
    /* NOT SUPPORTED!
    // IdxGrp = SYSTEMSERVICE_TARGETINFO, IdxOff = TARGETINFO_QUERYXMLINFOVERB
    std::string CAdsTargetClient::TargetQueryXmlInfo() const
    {
        auto nLength = _oClient.ReadValue<uint32_t>(_mtargetess, 700, 8);
        if (nLength == 0) { throw std::runtime_error("Invalid length readen!"); }
        return _oClient.ReadString(_mtargetess, 700, 8, nLength);
    }
    */
}

#[repr(u8)]
pub enum AdsPlatformIds {
    x86 = 0,
    x64 = 1,
    ARMv7 = 2,
    ARMT2 = 3,
}

#[repr(u8)]
pub enum AdsOsTypeIds {
    XP = 0,
    CE = 1,
    UM = 2,
    TC = 3,
}

#[repr(u16)]
pub enum AdsSystemFlags {
    ConfigModeOnly = 0x01,
    RouterModeOnly = 0x02,
}

#[derive(Default, Debug, Clone, Copy)]
pub struct AdsSystemState {
    pub ads_state: u16,
    pub dev_state: u16,
    pub restarts: u16,
    pub version: AdsVersion,
    pub platform: u8,
    pub os_type: u8,
    pub flags: u16,
    pub timeout: u32,
}

impl AdsCodable for AdsSystemState {
    fn size(&self) -> usize {
        22
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u16(self.ads_state)?;
        buf.enc_u16(self.dev_state)?;
        buf.enc_u16(self.restarts)?;
        buf.enc_type(&self.version)?;
        buf.enc_u8(self.platform)?;
        buf.enc_u8(self.os_type)?;
        buf.enc_u16(self.flags)?;
        buf.enc_u32(self.timeout)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u16(&mut self.ads_state)?;
        buf.dec_u16(&mut self.dev_state)?;
        buf.dec_u16(&mut self.restarts)?;
        buf.dec_type(&mut self.version)?;
        buf.dec_u8(&mut self.platform)?;
        buf.dec_u8(&mut self.os_type)?;
        buf.dec_u16(&mut self.flags)?;
        buf.dec_u32(&mut self.timeout)
    }
}

#[derive(Default, Debug, Clone)]
pub struct AdsSystemTargetInfo {
    pub target_type: String,
    pub hardware_model: String,
    pub hardware_serial: String,
    pub hardware_version: String,
    pub hardware_date: String,
    pub hardware_cpu: String,
    pub image_device: String,
    pub image_version: String,
    pub image_level: String,
    pub image_os_name: String,
    pub image_os_version: String,
    pub twincat_version: String,
    pub twincat_revision: String,
    pub twincat_build: String,
    pub twincat_level: String,
    pub net_id: String,
}
