// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
#![allow(dead_code)] // TODO
use ads_def::*;
use std::fs::File;
use std::io::{Read, Write};
use std::str;

pub struct AdsFileClient {
    client: AdsClientRef,
    target: AdsAddr,
}

impl AdsFileClient {
    pub fn new(client: AdsClientRef, target: AdsNetId) -> Self {
        AdsFileClient { client, target: AdsAddr::new(target, 10000) }
    }

    const fn mode_idx_off(mode: u32, path: u32) -> u32 {
        ((path) << 16) + (mode)
    }

    pub fn open(&mut self, path: &str, mode: u32) -> AdsResult<u32> {
        // IdxGrp = SYSTEMSERVICE_FOPEN
        let write_buf = AdsBuffer::from_utf8(path);
        let mut read_buf = AdsBuffer::with_size_of::<u32>();
        self.client.read_write(self.target, 120, Self::mode_idx_off(mode, AdsFilePathGeneric), write_buf.as_slice(), read_buf.as_mut_slice())?;
        read_buf.dec_into_u32()
    }

    pub fn close(&mut self, handle: u32) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_FCLOSE
        self.client.read_write(self.target, 121, handle, &[], &mut [])?;
        Ok(()) // ignore
    }

    pub fn read(&mut self, handle: u32, data: &mut [u8]) -> AdsResult<usize> {
        // IdxGrp = SYSTEMSERVICE_FREAD
        self.client.read_write(self.target, 122, handle, &[], data)
    }

    pub fn read_utf8(&mut self, handle: u32, len: usize) -> AdsResult<String> {
        // IdxGrp = SYSTEMSERVICE_FREAD
        let mut buf = AdsBuffer::with_size(len);
        self.client.read_write(self.target, 122, handle, &[], buf.as_mut_slice())?;
        buf.dec_into_utf8()
    }

    pub fn read_utf8_all(&mut self, path: &str) -> AdsResult<String> {
        const read_size: usize = 32768;
        let mut read_val = String::new();
        let mut read_data = [0; read_size];
        let handle = self.open(path, AdsFileModeRead)?;
        while let Ok(n) = self.read(handle, &mut read_data) {
            if n != 0 {
                if let Ok(s) = str::from_utf8(&read_data[..n]) {
                    read_val.push_str(s);
                } else {
                    break;
                }
            }
            if n < read_size {
                break;
            }
        }
        self.close(handle)?;
        Ok(read_val)
    }

    pub fn write(&mut self, handle: u32, data: &[u8]) -> AdsResult<usize> {
        // IdxGrp = SYSTEMSERVICE_FWRITE
        let mut read_buf = AdsBuffer::with_size_of::<u32>();
        self.client.read_write(self.target, 123, handle, data, read_buf.as_mut_slice())?;
        Ok(read_buf.dec_into_u32()? as usize)
    }

    pub fn write_utf8(&mut self, handle: u32, val: &str) -> AdsResult<usize> {
        // IdxGrp = SYSTEMSERVICE_FWRITE
        let write_buf = AdsBuffer::from_utf8(val);
        let mut read_buf = AdsBuffer::with_size_of::<u32>();
        self.client.read_write(self.target, 123, handle, write_buf.as_slice(), read_buf.as_mut_slice())?;
        Ok(read_buf.dec_into_u32()? as usize)
    }

    pub fn write_utf8_all(&mut self, path: &str, val: &str) -> AdsResult<usize> {
        const write_size: usize = 32768;
        let mut write_pos = 0;
        let handle = self.open(path, AdsFileModeWrite)?;
        while write_pos < val.len() {
            let mut write_data = &val[write_pos..];
            if write_data.len() > write_size {
                write_data = &write_data[..write_size];
            }
            if let Ok(n) = self.write(handle, write_data.as_bytes()) {
                write_pos += n;
            } else {
                break;
            }
        }
        self.close(handle)?;
        Ok(write_pos)
    }

    pub fn flush(&mut self, handle: u32) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_FFLUSH
        self.client.read_write(self.target, 112, handle, &[], &mut [])?;
        Ok(())
    }

    pub fn truncate(&mut self, handle: u32, pos: u32) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_FTRUNCATE
        let write_buf = AdsBuffer::from_u32(pos);
        self.client.read_write(self.target, 114, handle, write_buf.as_slice(), &mut [])?;
        Ok(())
    }

    pub fn seek(&mut self, handle: u32, pos: u32) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_FSEEK
        let write_buf = AdsBuffer::from_u32(pos);
        self.client.read_write(self.target, 124, handle, write_buf.as_slice(), &mut [])?;
        Ok(())
    }

    pub fn tell(&mut self, handle: u32) -> AdsResult<u32> {
        // IdxGrp = SYSTEMSERVICE_FTELL
        let mut read_buf = AdsBuffer::with_size_of::<u32>();
        self.client.read_write(self.target, 125, handle, &[], read_buf.as_mut_slice())?;
        read_buf.dec_into_u32()
    }

    pub fn remove(&mut self, path: &str) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_FDELETE
        let write_buf = AdsBuffer::from_utf8(path);
        self.client.read_write(self.target, 131, AdsFilePathGeneric, write_buf.as_slice(), &mut [])?;
        Ok(())
    }

    pub fn rename(&mut self, origin: &str, destination: &str) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_FRENAME
        let mut write_buf = AdsBuffer::new();
        write_buf.enc_utf8(origin)?;
        write_buf.enc_utf8(destination)?;
        self.client.read_write(self.target, 132, AdsFilePathGeneric, write_buf.as_slice(), &mut [])?;
        Ok(())
    }

    pub fn copy(&mut self, origin: &str, destination: &str) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_FRENAME
        let mut write_buf = AdsBuffer::new();
        write_buf.enc_utf8(origin)?;
        write_buf.enc_utf8(destination)?;
        self.client.read_write(self.target, 136, AdsFilePathGeneric, write_buf.as_slice(), &mut [])?;
        Ok(())
    }

    pub fn status(&mut self, path: &str) -> AdsResult<AdsFileStatus> {
        // IdxGrp = SYSTEMSERVICE_FGETSTATUS
        let write_buf = AdsBuffer::from_utf8(path);
        let mut read_buf = AdsBuffer::with_size(36);
        self.client.read_write(self.target, 134, AdsFilePathGeneric, write_buf.as_slice(), read_buf.as_mut_slice())?;
        read_buf.dec_into_type::<AdsFileStatus>()
    }

    pub fn hash(&mut self, path: &str) -> AdsResult<[u8; 16]> {
        // IdxGrp = SYSTEMSERVICE_FGETMD5HASH
        let write_buf = AdsBuffer::from_utf8(path);
        let mut read_buf = [0; 16];
        self.client.read_write(self.target, 137, AdsFilePathGeneric, write_buf.as_slice(), &mut read_buf)?;
        Ok(read_buf)
    }

    pub fn find(&mut self, path: &str) -> AdsResult<AdsFileEntry> {
        // IdxGrp = SYSTEMSERVICE_FFILEFIND
        let write_buf = AdsBuffer::from_utf8(path);
        let mut read_buf = AdsBuffer::with_size(324);
        self.client.read_write(self.target, 133, AdsFilePathGeneric, write_buf.as_slice(), read_buf.as_mut_slice())?;
        read_buf.dec_into_type::<AdsFileEntry>()
    }

    pub fn find_next(&mut self, handle: u32) -> AdsResult<AdsFileEntry> {
        // IdxGrp = SYSTEMSERVICE_FFILEFIND
        let mut read_buf = AdsBuffer::with_size(324);
        self.client.read_write(self.target, 133, handle, &[], read_buf.as_mut_slice())?;
        read_buf.dec_into_type::<AdsFileEntry>()
    }

    pub fn find_close(&mut self, handle: u32) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_CLOSEHANDLE
        self.client.write(self.target, 111, handle, &[])?;
        Ok(()) // ignore
    }

    pub fn find_all(&mut self, path: &str) -> AdsResult<Vec<AdsFileEntry>> {
        let mut entries = Vec::new();

        let entry = self.find(path)?;
        let mut handle = entry.handle;
        entries.push(entry);

        while let Ok(entry) = self.find_next(handle) {
            handle = entry.handle;
            entries.push(entry);
        }

        self.find_close(handle)?;
        Ok(entries)
    }

    pub fn create_dir(&mut self, path: &str) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_MKDIR
        let write_buf = AdsBuffer::from_utf8(path);
        self.client.read_write(self.target, 138, AdsFilePathGeneric, write_buf.as_slice(), &mut [])?;
        Ok(())
    }

    pub fn remove_dir(&mut self, path: &str) -> AdsResult<()> {
        // IdxGrp = SYSTEMSERVICE_RMDIR
        let write_buf = AdsBuffer::from_utf8(path);
        self.client.read_write(self.target, 139, AdsFilePathGeneric, write_buf.as_slice(), &mut [])?;
        Ok(())
    }

    pub fn get(&mut self, remote_path: &str, local_path: &str) -> AdsResult<usize> {
        const read_size: usize = 32768;
        let mut read_pos: usize = 0;
        let mut read_data = [0; read_size];
        let mut local = File::create(local_path)?;
        let handle = self.open(remote_path, AdsFileModeRead)?;
        while let Ok(n) = self.read(handle, &mut read_data) {
            if n != 0 {
                local.write_all(&read_data[..n])?;
                read_pos += n;
            }
            if n < read_size {
                break;
            }
        }
        self.close(handle)?;
        local.flush()?;
        Ok(read_pos)
    }

    pub fn put(&mut self, local_path: &str, remote_path: &str) -> AdsResult<usize> {
        const write_size: usize = 32768;
        let mut write_pos = 0;
        let mut write_data = [0; write_size];
        let mut local = File::open(local_path)?;
        let handle = self.open(remote_path, AdsFileModeWrite)?;
        while let Ok(n) = local.read(&mut write_data) {
            if n != 0 {
                self.write(handle, &write_data[..n])?;
                write_pos += n;
            }
            if n < write_size {
                break;
            }
        }
        self.close(handle)?;
        Ok(write_pos)
    }
}

#[allow(non_snake_case)]
pub mod AdsFileMode {
    pub const Read: u32 = 0x01;
    pub const Write: u32 = 0x02;
    pub const Append: u32 = 0x04;
    pub const Plus: u32 = 0x08;
    pub const Binary: u32 = 0x10;
    pub const Text: u32 = 0x20;
    pub const EnsureDirectory: u32 = 0x40;
    pub const EnableDirectory: u32 = 0x80;
    pub const Overwrite: u32 = 0x100;
    pub const OverwriteAndRename: u32 = 0x200;
}

pub const AdsFileModeRead: u32 = AdsFileMode::Read | AdsFileMode::Binary | AdsFileMode::EnsureDirectory;
pub const AdsFileModeWrite: u32 = AdsFileMode::Write | AdsFileMode::Plus | AdsFileMode::Binary | AdsFileMode::EnsureDirectory;
pub const AdsFileModeAppend: u32 = AdsFileMode::Append | AdsFileMode::Plus | AdsFileMode::Binary | AdsFileMode::EnsureDirectory;

#[allow(non_snake_case)]
pub mod AdsFilePath {
    pub const Generic: u32 = 1;
    pub const BootProject: u32 = 2;
    pub const BootData: u32 = 3;
    pub const BootPath: u32 = 4;
}

pub const AdsFilePathGeneric: u32 = AdsFilePath::Generic;

#[allow(non_snake_case)]
pub mod AdsFileAttr {
    pub const ReadOnly: u32 = 0x01;
    pub const Hidden: u32 = 0x02;
    pub const System: u32 = 0x04;
    pub const Directory: u32 = 0x10;
    pub const Archive: u32 = 0x20;
    pub const Device: u32 = 0x40;
    pub const Normal: u32 = 0x80;
    pub const Temporary: u32 = 0x100;
    pub const SparseFile: u32 = 0x200;
    pub const ReparsePoint: u32 = 0x400;
    pub const Compressed: u32 = 0x800;
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct AdsFileStatus {
    pub size: u64,
    pub creation_time: u64,
    pub modified_time: u64,
    pub access_time: u64,
    pub attributes: u32,
}

impl AdsCodable for AdsFileStatus {
    fn size(&self) -> usize {
        36
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u64(self.size)?;
        buf.enc_u64(self.creation_time)?;
        buf.enc_u64(self.modified_time)?;
        buf.enc_u64(self.access_time)?;
        buf.enc_u32(self.attributes)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u64(&mut self.size)?;
        buf.dec_u64(&mut self.creation_time)?;
        buf.dec_u64(&mut self.modified_time)?;
        buf.dec_u64(&mut self.access_time)?;
        buf.dec_u32(&mut self.attributes)
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct AdsFileEntry {
    pub handle: u32,
    pub attributes: u32,
    pub creation_time: u64,
    pub access_time: u64,
    pub modified_time: u64,
    pub size: u64,
    pub name: String,
    pub alternate_name: String,
}

impl AdsCodable for AdsFileEntry {
    fn size(&self) -> usize {
        324
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.handle)?;
        buf.enc_u32(self.attributes)?;
        buf.enc_u64(self.creation_time)?;
        buf.enc_u64(self.access_time)?;
        buf.enc_u64(self.modified_time)?;
        buf.enc_u32(((self.size & 0xffffffff00000000) >> 32) as u32)?;
        buf.enc_u32((self.size & 0x00000000ffffffff) as u32)?;
        buf.skip(8);
        buf.enc_utf8_len(&self.name, 260)?;
        buf.enc_utf8_len(&self.alternate_name, 14)?;
        buf.skip(2);
        Ok(())
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.handle)?;
        buf.dec_u32(&mut self.attributes)?;
        buf.dec_u64(&mut self.creation_time)?;
        buf.dec_u64(&mut self.access_time)?;
        buf.dec_u64(&mut self.modified_time)?;
        self.size = ((buf.dec_into_u32()? as u64) << 32) + buf.dec_into_u32()? as u64;
        buf.skip(8);
        buf.dec_utf8_len(&mut self.name, 260)?;
        buf.dec_utf8_len(&mut self.alternate_name, 14)?;
        buf.skip(2);
        Ok(())
    }
}
