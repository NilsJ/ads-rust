// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
// See: https://infosys.beckhoff.com/content/1033/devicemanager/index.html
#![allow(non_upper_case_globals)]
#![allow(dead_code)] // TODO
use crate::sum::{AdsSumReadClient, AdsSumResponse, AdsSumResponseExt};
use ads_def::*;
use log::debug;
use std::collections::HashMap;
use std::mem::size_of;
use std::vec::Vec;
use std::{thread, time::Duration};

#[repr(u32)]
#[derive(Copy, Clone, PartialEq)]
pub enum AdsMdpArea {
    General = 0x01,
    Config = 0x08,
    Service = 0x0B,
    Device = 0x0F,
}

impl From<u32> for AdsMdpArea {
    fn from(v: u32) -> Self {
        match v {
            0x01 => AdsMdpArea::General,
            0x08 => AdsMdpArea::Config,
            0x0B => AdsMdpArea::Service,
            0x0F => AdsMdpArea::Device,
            _ => panic!("Invalid AdsMdpAreaId!"),
        }
    }
}

#[repr(u32)]
#[derive(Copy, Clone, PartialEq)]
pub enum AdsMdpType {
    Access = 0x01,
    NIC = 0x02,
    Time = 0x03,
    User = 0x04,
    RAS = 0x05, // deprecated
    FTP = 0x06,
    SMB = 0x07,
    TwinCAT = 0x08,
    DataStore = 0x09, // deprecated
    Software = 0x0A,
    CPU = 0x0B,
    RAM = 0x0C,
    Log = 0x0D,        // deprecated
    FirewallCE = 0x0E, // WinCE/WEC
    Firewall = 0x0F,   // Firewall
    FSO = 0x10,
    Registry = 0x11,
    PLC = 0x12,
    Display = 0x13,
    EWF = 0x14,
    FBWF = 0x15,
    RegFilter = 0x16,    // deprecated
    SiliconDrive = 0x17, // deprecated
    OS = 0x18,
    RAID = 0x19,
    CX9Flash = 0x1A, // deprecated
    Fan = 0x1B,
    Mainboard = 0x1C,
    Disk = 0x1D,
    UPS = 0x1E,
    SMART = 0x1F,
    MassStorage = 0x20,
    UWF = 0x21,
    Misc = 0x100,
}

impl From<u32> for AdsMdpType {
    fn from(v: u32) -> Self {
        match v {
            0x01 => AdsMdpType::Access,
            0x02 => AdsMdpType::NIC,
            0x03 => AdsMdpType::Time,
            0x04 => AdsMdpType::User,
            0x05 => AdsMdpType::RAS,
            0x06 => AdsMdpType::FTP,
            0x07 => AdsMdpType::SMB,
            0x08 => AdsMdpType::TwinCAT,
            0x09 => AdsMdpType::DataStore,
            0x0A => AdsMdpType::Software,
            0x0B => AdsMdpType::CPU,
            0x0C => AdsMdpType::RAM,
            0x0D => AdsMdpType::Log,
            0x0E => AdsMdpType::FirewallCE,
            0x0F => AdsMdpType::Firewall,
            0x10 => AdsMdpType::FSO,
            0x11 => AdsMdpType::Registry,
            0x12 => AdsMdpType::PLC,
            0x13 => AdsMdpType::Display,
            0x14 => AdsMdpType::EWF,
            0x15 => AdsMdpType::FBWF,
            0x16 => AdsMdpType::RegFilter,
            0x17 => AdsMdpType::SiliconDrive,
            0x18 => AdsMdpType::OS,
            0x19 => AdsMdpType::RAID,
            0x1A => AdsMdpType::CX9Flash,
            0x1B => AdsMdpType::Fan,
            0x1C => AdsMdpType::Mainboard,
            0x1D => AdsMdpType::Disk,
            0x1E => AdsMdpType::UPS,
            0x1F => AdsMdpType::SMART,
            0x20 => AdsMdpType::MassStorage,
            0x21 => AdsMdpType::UWF,
            0x100 => AdsMdpType::Misc,
            _ => panic!("Invalid AdsMdpType!"),
        }
    }
}

impl ToString for AdsMdpType {
    fn to_string(&self) -> String {
        let r = match self {
            AdsMdpType::Access => "Access",
            AdsMdpType::NIC => "NIC",
            AdsMdpType::Time => "Time",
            AdsMdpType::User => "User",
            AdsMdpType::RAS => "RAS (deprecated)",
            AdsMdpType::FTP => "FTP",
            AdsMdpType::SMB => "SMB",
            AdsMdpType::TwinCAT => "TwinCAT",
            AdsMdpType::DataStore => "DataStore (deprecated)",
            AdsMdpType::Software => "Software",
            AdsMdpType::CPU => "CPU",
            AdsMdpType::RAM => "RAM",
            AdsMdpType::Log => "Log (deprecated)",
            AdsMdpType::FirewallCE => "Firewall (WinCE/WEC)",
            AdsMdpType::Firewall => "Firewall",
            AdsMdpType::FSO => "FSO",
            AdsMdpType::Registry => "Registry",
            AdsMdpType::PLC => "PLC",
            AdsMdpType::Display => "Display",
            AdsMdpType::EWF => "EWF",
            AdsMdpType::FBWF => "FBWF",
            AdsMdpType::RegFilter => "RegFilter (deprecated)",
            AdsMdpType::SiliconDrive => "SiliconDrive (deprecated)",
            AdsMdpType::OS => "OS",
            AdsMdpType::RAID => "RAID",
            AdsMdpType::Fan => "Fan",
            AdsMdpType::Mainboard => "Mainboard",
            AdsMdpType::Disk => "Disk",
            AdsMdpType::UPS => "UPS",
            AdsMdpType::SMART => "SMART",
            AdsMdpType::MassStorage => "MassStorage",
            AdsMdpType::UWF => "UWF",
            AdsMdpType::Misc => "Misc",
            _ => "",
        };
        if r.is_empty() {
            self.to_string()
        } else {
            r.to_string()
        }
    }
}

const AdsMdpIdxGrp: u32 = 0xf302; // ADSIGRP_CANOPEN_SDO
const AdsMdpIdxSum: u32 = 0xf304; // ADSIGRP_CANOPEN_SDO_SUMUP_READ

const fn mdp_idx_off(area: AdsMdpArea, module: u32, table: u32, subidx: u32) -> u32 {
    ((area as u32) & 0x0f) << 28 | (module & 0xff) << 20 | (table & 0x0f) << 16 | (subidx & 0xff)
}

pub struct AdsMdpModuleList {
    map: HashMap<u32, AdsMdpType>,
}

impl AdsMdpModuleList {
    pub fn ids(&self) -> Vec<u32> {
        self.map.keys().copied().collect::<Vec<u32>>()
    }

    pub fn get_type(&self, id: &u32) -> AdsMdpType {
        self.map[id]
    }

    pub fn get_mod(&self, id: AdsMdpType) -> Option<u32> {
        self.map.iter().find_map(|(key, &val)| if val == id { Some(*key) } else { None })
    }

    pub fn get_mods(&self, id: AdsMdpType) -> Vec<u32> {
        let mut vec: Vec<u32> = self.map.iter().filter_map(|(key, val)| if *val == id { Some(*key) } else { None }).collect();
        vec.sort_unstable();
        vec
    }
}

pub struct AdsMdpSumReadClient {
    client: AdsSumReadClient,
}

impl AdsMdpSumReadClient {
    pub fn new(client: AdsClientRef, target: AdsAddr) -> Self {
        AdsMdpSumReadClient {
            client: AdsSumReadClient::with_grp(client, target, AdsMdpIdxSum),
        }
    }

    pub fn add(&mut self, module: u32, table: u32, subidx: u32, len: usize) -> usize {
        self.client.add(AdsMdpIdxGrp, mdp_idx_off(AdsMdpArea::Config, module, table, subidx), len as u32)
    }

    pub fn add_bool(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<bool>())
    }

    pub fn add_u8(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<u8>())
    }

    pub fn add_i16(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<i16>())
    }

    pub fn add_u16(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<u16>())
    }

    pub fn add_i32(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<i32>())
    }

    pub fn add_u32(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<u32>())
    }

    pub fn add_u64(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<u64>())
    }

    pub fn add_utf8(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, 256)
    }

    pub fn add_size_of<T>(&mut self, module: u32, table: u32, subidx: u32) -> usize {
        self.add(module, table, subidx, size_of::<T>())
    }

    pub fn add_area(&mut self, area: AdsMdpArea, module: u32, table: u32, subidx: u32, len: usize) -> usize {
        self.client.add(AdsMdpIdxGrp, mdp_idx_off(area, module, table, subidx), len as u32)
    }

    pub fn execute(&mut self) -> AdsResult<()> {
        self.client.execute()
    }
}

impl AdsSumResponse for AdsMdpSumReadClient {
    fn cnt_res(&self) -> usize {
        self.client.cnt_res()
    }
    fn chk_res(&self) -> AdsResult<()> {
        self.client.chk_res()
    }
    fn get_res(&self, idx: usize) -> AdsResult<()> {
        self.client.get_res(idx)
    }
    fn get_res_buf(&self, idx: usize, size: usize) -> AdsResult<AdsBuffer> {
        self.client.get_res_buf(idx, size)
    }
}

impl AdsSumResponseExt for AdsMdpSumReadClient {}

pub struct AdsMdpClient {
    client: AdsClientRef,
    target: AdsAddr,
}

impl AdsMdpClient {
    pub fn new(client: AdsClientRef, target: AdsNetId) -> Self {
        AdsMdpClient { client, target: AdsAddr::new(target, 10000) }
    }

    pub fn read(&mut self, area: AdsMdpArea, module: u32, table: u32, subidx: u32, data: &mut [u8]) -> AdsResult<usize> {
        self.client.read(self.target, AdsMdpIdxGrp, mdp_idx_off(area, module, table, subidx), data)
    }

    pub fn read_u8(&mut self, area: AdsMdpArea, module: u32, table: u32, subidx: u32) -> AdsResult<u8> {
        self.client.read_u8(self.target, AdsMdpIdxGrp, mdp_idx_off(area, module, table, subidx))
    }

    pub fn read_u16(&mut self, area: AdsMdpArea, module: u32, table: u32, subidx: u32) -> AdsResult<u16> {
        self.client.read_u16(self.target, AdsMdpIdxGrp, mdp_idx_off(area, module, table, subidx))
    }

    pub fn read_u32(&mut self, area: AdsMdpArea, module: u32, table: u32, subidx: u32) -> AdsResult<u32> {
        self.client.read_u32(self.target, AdsMdpIdxGrp, mdp_idx_off(area, module, table, subidx))
    }

    pub fn read_utf8(&mut self, area: AdsMdpArea, module: u32, table: u32, subidx: u32) -> AdsResult<String> {
        self.client.read_utf8(self.target, AdsMdpIdxGrp, mdp_idx_off(area, module, table, subidx), 256)
    }

    pub fn write(&mut self, area: AdsMdpArea, module: u32, table: u32, subidx: u32, data: &[u8]) -> AdsResult<()> {
        self.client.write(self.target, AdsMdpIdxGrp, mdp_idx_off(area, module, table, subidx), data)
    }

    // --------------------------------------------------
    // general area

    pub fn read_device_name(&mut self) -> AdsResult<String> {
        self.read_utf8(AdsMdpArea::General, 0, 8, 0)
    }

    pub fn read_hardware_version(&mut self) -> AdsResult<String> {
        self.read_utf8(AdsMdpArea::General, 0, 9, 0)
    }

    pub fn read_image_version(&mut self) -> AdsResult<String> {
        self.read_utf8(AdsMdpArea::General, 0, 10, 0)
    }

    // --------------------------------------------------
    // device area

    pub fn read_module_list(&mut self) -> AdsResult<AdsMdpModuleList> {
        let mut map = HashMap::new();

        let len = self.read_u16(AdsMdpArea::Device, 2, 0, 0)? as u32;
        if len == 0 {
            return Ok(AdsMdpModuleList { map });
        }

        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        for i in 1..=len {
            read.add_area(AdsMdpArea::Device, 2, 0, i, size_of::<u32>());
        }
        read.execute()?;
        read.chk_res()?;
        for i in 0..len {
            let module = read.get_res_u32(i as usize)?;
            let mod_id = module & 0x0000ffff;
            let mod_type = (module & 0xffff0000) >> 16;
            map.insert(mod_id, AdsMdpType::from(mod_type));
        }
        Ok(AdsMdpModuleList { map })
    }

    pub fn read_module_names(&mut self, modules: &[u32]) -> AdsResult<HashMap<u32, String>> {
        let mut map = HashMap::new();
        if modules.is_empty() {
            return Ok(map);
        }

        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        for module in modules {
            read.add_area(AdsMdpArea::Device, 6, 0, module + 1, 256);
        }
        read.execute()?;
        read.chk_res()?;
        for (i, module) in modules.iter().enumerate() {
            let name = read.get_res_utf8(i)?;
            map.insert(*module, name);
        }
        Ok(map)
    }

    pub fn read_module_name(&mut self, module: u32) -> AdsResult<String> {
        self.read_utf8(AdsMdpArea::Device, 6, 0, module + 1)
    }

    pub fn read_serial(&mut self) -> AdsResult<String> {
        self.read_utf8(AdsMdpArea::Device, 0x9f, 0, 0)
    }

    // ------------------------------------------------------------
    // Access

    // ------------------------------------------------------------
    // NIC

    pub fn read_nic(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        read.add_utf8(module, 0, 3);
        read.add_utf8(module, 1, 1); // MAC address
        read.add_utf8(module, 1, 2); // IPv4 address
        read.add_utf8(module, 1, 3); // IPv4 subnet mask
        read.add_bool(module, 1, 4); // DHCP enabled
        read.add_utf8(module, 1, 5); // IPv4 default gateway
        read.add_utf8(module, 1, 6); // IPv4 dns server
        read.add_utf8(module, 1, 7); // virtual device name
        read.add_utf8(module, 1, 8); // IPv4 dns server active

        read.execute()?;

        if let Ok(v) = read.get_res_utf8(0) {
            println!("NIC.Name={}", v);
        }
        if let Ok(v) = read.get_res_utf8(1) {
            println!("NIC.MAC={}", v);
        }
        if let Ok(v) = read.get_res_utf8(2) {
            println!("NIC.Address={}", v);
        }
        if let Ok(v) = read.get_res_utf8(3) {
            println!("NIC.Subnet={}", v);
        }
        if let Ok(v) = read.get_res_bool(4) {
            println!("NIC.DHCP={}", v);
        }
        if let Ok(v) = read.get_res_utf8(5) {
            println!("NIC.Gateway={}", v);
        }
        if let Ok(v) = read.get_res_utf8(6) {
            println!("NIC.DnsServer={}", v);
        }
        if let Ok(v) = read.get_res_utf8(7) {
            println!("NIC.VirtualDeviceName={}", v);
        }
        if let Ok(v) = read.get_res_utf8(8) {
            println!("NIC.DnsServerActive={}", v);
        }
        Ok(())
    }

    pub fn write_nic(&mut self, module: u32, address: &str, subnet: &str, gateway: &str) -> AdsResult<()> {
        let mut buf = AdsBuffer::with_capacity(256);
        let address_len = address.len() + 1;
        let subnet_len = subnet.len() + 1;
        let gateway_len = gateway.len() + 1;
        let len = 3 * size_of::<u32>() + address_len + subnet_len + gateway_len;

        buf.enc_u32(len as u32)?;
        buf.enc_u32(address_len as u32)?;
        buf.enc_u32(subnet_len as u32)?;
        buf.enc_u32(gateway_len as u32)?;
        buf.enc_utf8(address)?;
        buf.enc_utf8(subnet)?;
        buf.enc_utf8(gateway)?;

        self.write(AdsMdpArea::Service, module, 0, 1, buf.as_slice())?;

        let mut status: u16 = 255;
        while status >= 100 {
            thread::sleep(Duration::from_millis(100));
            if let Ok(s) = self.read_u16(AdsMdpArea::Service, module, 0, 2) {
                status = s;
            }
        }
        if status != 0 {
            debug!("AdsMdpClient.write_nic: status={}", status);
            return Err(AdsError::from(0x0001));
        }
        Ok(())
    }

    // ------------------------------------------------------------
    // Time

    pub fn read_time(&mut self, module: u32) -> AdsResult<()> {
        if let Ok(server) = self.read_utf8(AdsMdpArea::Config, module, 1, 1) {
            // NTP server by name or IP address
            println!("Server={}", server);
        }
        if let Ok(refresh) = self.read_u32(AdsMdpArea::Config, module, 1, 2) {
            // NTP refresh time in seconds
            println!("Refresh={}s", refresh);
        }
        //let time_unix = self.read_u32(AdsMdpArea::Config, module, 1, 3)?; // Unix Time, seconds since 1970-01-01
        if let Ok(time) = self.read_utf8(AdsMdpArea::Config, module, 1, 4) {
            // Textual time presentation (ISO 8601)
            println!("Time={}", time);
        }
        //let timezone = self.read_u32(AdsMdpArea::Config, module, 1, 5)?; // Timezone index (from list in table 2)
        if let Ok(timezone_offset) = self.read_u32(AdsMdpArea::Config, module, 1, 6) {
            // Timezone offset in seconds relative to UTC
            println!("Offset={}", timezone_offset);
        }
        Ok(())
    }

    // ------------------------------------------------------------
    // User

    pub fn read_user(&mut self, module: u32) -> AdsResult<()> {
        let len = self.read_u16(AdsMdpArea::Config, module, 1, 0)? as u32;

        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        for i in 1..=len {
            read.add_utf8(module, 1, i);
            read.add_utf8(module, 3, i);
        }

        read.execute()?;
        for i in 0..len as usize {
            let name = read.get_res_utf8(i * 2).unwrap_or_default();
            let groups = read.get_res_utf8(i * 2 + 1).unwrap_or_default();
            println!("Users.Name='{}', Groups='{}'", name, groups);
        }

        Ok(())
    }

    // ------------------------------------------------------------
    // SMB

    // ------------------------------------------------------------
    // TwinCAT

    pub fn read_twincat(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        read.add_utf8(module, 0, 3);
        read.add_u16(module, 1, 1);
        read.add_u16(module, 1, 2);
        read.add_u16(module, 1, 3);
        read.add_u16(module, 1, 12);
        read.add_utf8(module, 1, 4);
        read.add_u16(module, 1, 6);
        read.add_utf8(module, 1, 11);

        read.execute()?;
        read.chk_res()?;

        println!("TwinCAT.Name={}", read.get_res_utf8(0)?);
        println!("TwinCAT.Version.Major={}", read.get_res_u16(1)?);
        println!("TwinCAT.Version.Minor={}", read.get_res_u16(2)?);
        println!("TwinCAT.Version.Build={}", read.get_res_u16(3)?);
        println!("TwinCAT.Version.Revision={}", read.get_res_u16(4)?);

        println!("TwinCAT.System.NetID={}", read.get_res_utf8(5)?);
        println!("TwinCAT.System.Status={}", read.get_res_u16(6)?);
        println!("TwinCAT.System.SystemID={}", read.get_res_utf8(7)?);

        Ok(())
    }

    // ------------------------------------------------------------
    // Unknown

    // ------------------------------------------------------------
    // Software

    pub fn read_software(&mut self, module: u32) -> AdsResult<()> {
        let len = self.read_u16(AdsMdpArea::Config, module, 1, 0)? as u32;

        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        for i in 1..=len {
            read.add_utf8(module, 1, i);
            read.add_utf8(module, 2, i);
            read.add_utf8(module, 3, i);
            read.add_utf8(module, 4, i);
        }

        read.execute()?;
        for i in 0..len as usize {
            let name = read.get_res_utf8(i * 4).unwrap_or_default();
            let company = read.get_res_utf8(i * 4 + 1).unwrap_or_default();
            let date = read.get_res_utf8(i * 4 + 2).unwrap_or_default();
            let version = read.get_res_utf8(i * 4 + 3).unwrap_or_default();
            println!("Software.Name='{}', Version='{}', Company='{}', Date='{}'", name, version, company, date);
        }

        Ok(())
    }

    // ------------------------------------------------------------
    // CPU

    pub fn read_cpu(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        let idx_freq = read.add_u32(module, 1, 1);
        let idx_usage = read.add_u16(module, 1, 2);
        let idx_temp = read.add_u16(module, 1, 3);

        read.execute()?;

        println!("CPU.Usage[%]={}", read.get_res_u16(idx_usage).unwrap_or_default());
        println!("CPU.Frequency[Hz]={}", read.get_res_u32(idx_freq).unwrap_or_default());
        println!("CPU.Temperature[°C]={}", read.get_res_u16(idx_temp).unwrap_or_default());

        Ok(())
    }

    // ------------------------------------------------------------
    // RAM

    pub fn read_ram(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        read.add_u64(module, 1, 6);
        read.add_u64(module, 1, 7);

        read.execute()?;

        println!("RAM.Allocated[MB]={}", read.get_res_u64(0).unwrap_or_default() / (1024 * 1024));
        println!("RAM.Available[MB]={}", read.get_res_u64(1).unwrap_or_default() / (1024 * 1024));

        Ok(())
    }

    // ------------------------------------------------------------
    // FSO

    // ------------------------------------------------------------
    // PLC

    // ------------------------------------------------------------
    // Display

    // ------------------------------------------------------------
    // EWF

    // ------------------------------------------------------------
    // FBWF

    // ------------------------------------------------------------
    // OS

    pub fn read_os(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        read.add_utf8(module, 0, 3);
        read.add_u32(module, 1, 1);
        read.add_u32(module, 1, 2);
        read.add_u32(module, 1, 3);
        read.add_utf8(module, 1, 4);

        read.execute()?;

        println!("OS.Name={}", read.get_res_utf8(0).unwrap_or_default());
        println!("OS.Major={}", read.get_res_u32(1).unwrap_or_default());
        println!("OS.Minor={}", read.get_res_u32(2).unwrap_or_default());
        println!("OS.Build={}", read.get_res_u32(3).unwrap_or_default());
        println!("OS.Version={}", read.get_res_utf8(4).unwrap_or_default());

        Ok(())
    }

    // ------------------------------------------------------------
    // Fan

    pub fn read_fan(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        read.add_utf8(module, 0, 3);
        read.add_u16(module, 1, 1);

        read.execute()?;

        if let Ok(v) = read.get_res_utf8(0) {
            println!("FAN.Name={}", v);
        }
        if let Ok(v) = read.get_res_u16(1) {
            println!("Fan.Speed[r/m]={}", v);
        }
        Ok(())
    }

    // ------------------------------------------------------------
    // Mainboard

    pub fn read_mainboard(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        read.add_utf8(module, 1, 1);
        read.add_utf8(module, 1, 2);
        read.add_utf8(module, 1, 3);
        read.add_u32(module, 1, 4);
        read.add_u32(module, 1, 5);
        read.add_i32(module, 1, 6);
        read.add_i32(module, 1, 7);
        read.add_i32(module, 1, 8);
        read.add_i32(module, 1, 9);
        read.add_i16(module, 1, 10);
        read.add_u8(module, 2, 1);
        read.add_u8(module, 2, 2);
        read.add_u8(module, 2, 3);

        read.execute()?;

        println!("Mainboard.Type={}", read.get_res_utf8(0).unwrap_or_default());
        println!("Mainboard.Serial={}", read.get_res_utf8(1).unwrap_or_default());
        println!("Mainboard.Date={}", read.get_res_utf8(2).unwrap_or_default());
        println!("Mainboard.BootCount={}", read.get_res_u32(3).unwrap_or_default());
        println!("Mainboard.Operation[m]={}", read.get_res_u32(4).unwrap_or_default());
        println!("Mainboard.MinTemp[°C]={}", read.get_res_i32(5).unwrap_or_default());
        println!("Mainboard.MaxTemp[°C]={}", read.get_res_i32(6).unwrap_or_default());
        println!("Mainboard.MinVolt[mV]={}", read.get_res_i32(7).unwrap_or_default());
        println!("Mainboard.MaxVolt[mV]={}", read.get_res_i32(8).unwrap_or_default());
        println!("Mainboard.Temp[°C]={}", read.get_res_i16(9).unwrap_or_default());
        println!("Mainboard.Revision={}", read.get_res_u8(10).unwrap_or_default());
        println!("Mainboard.BiosMajor={}", read.get_res_u8(11).unwrap_or_default());
        println!("Mainboard.BiosMinor={}", read.get_res_u8(12).unwrap_or_default());

        Ok(())
    }

    // ------------------------------------------------------------
    // Disk

    pub fn read_disk(&mut self, module: u32) -> AdsResult<()> {
        let len = self.read_u16(AdsMdpArea::Config, module, 1, 0)? as u32;

        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        for i in 1..=len {
            read.add_utf8(module, 1, i);
            read.add_utf8(module, 2, i);
            read.add_utf8(module, 3, i);
            read.add_u32(module, 4, i);
            read.add_u64(module, 5, i);
            read.add_u64(module, 6, i);
        }

        read.execute()?;

        for i in 0..len as usize {
            println!("Disk.Letter={}", read.get_res_utf8(i * 4).unwrap_or_default());
            println!("Disk.VolumeLabel={}", read.get_res_utf8(i * 4 + 1).unwrap_or_default());
            println!("Disk.FileSystem={}", read.get_res_utf8(i * 4 + 2).unwrap_or_default());
            println!("Disk.Type={}", read.get_res_u32(i * 4 + 3).unwrap_or_default());
            println!("Disk.TotalSize[MB]={}", read.get_res_u64(i * 4 + 4).unwrap_or_default() / (1024 * 1024));
            println!("Disk.FreeSize[MB]={}", read.get_res_u64(i * 4 + 5).unwrap_or_default() / (1024 * 1024));
        }

        Ok(())
    }

    // ------------------------------------------------------------
    // UPS

    // ------------------------------------------------------------
    // SMART

    pub fn read_smart(&mut self, module: u32) -> AdsResult<()> {
        let mut read = AdsMdpSumReadClient::new(self.client.clone(), self.target);
        read.add_u32(module, 1, 1);
        read.add_utf8(module, 1, 2);
        read.add_utf8(module, 1, 3);
        read.add_u32(module, 1, 4);
        read.add_u64(module, 1, 5);
        read.add_u32(module, 1, 6);
        read.add_u64(module, 1, 7);
        read.add_u64(module, 1, 8);

        read.execute()?;

        println!("SMART.Index={}", read.get_res_u32(0).unwrap_or_default());
        println!("SMART.Caption={}", read.get_res_utf8(1).unwrap_or_default());
        println!("SMART.Partitions={}", read.get_res_utf8(2).unwrap_or_default());
        println!("SMART.PartitionCount={}", read.get_res_u32(3).unwrap_or_default());
        println!("SMART.TotalCylinders={}", read.get_res_u64(4).unwrap_or_default());
        println!("SMART.TotalHeads={}", read.get_res_u32(5).unwrap_or_default());
        println!("SMART.TotalSectors={}", read.get_res_u64(6).unwrap_or_default());
        println!("SMART.TotalTracks={}", read.get_res_u64(7).unwrap_or_default());

        Ok(())
    }

    // ------------------------------------------------------------
    // MassStorage

    // ------------------------------------------------------------
    // Misc
}
