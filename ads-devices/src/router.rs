// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_def::*;
use log::debug;
use std::mem::size_of;

#[repr(u32)]
pub enum AdsPortTypeIds {
    Router = 0x00,
    R3Port = 0x01,
    R0Ctrl = 0x02,
    R0Task = 0x03,
    R0Io = 0x04,
    Tp = 0x05,
    R3PortItf = 0x06,
    R0CtrlItf = 0x07,
    R0TaskItf = 0x08,
    R0IoItf = 0x09,
    TpItf = 0x10,
    Max = 0xFF,
}

#[repr(u32)]
pub enum AdsPortAttr {
    Undef = 0x0000,
    ADS = 0x0001,
    Server = 0x0002,
    Client = 0x0004,
    Symbols = 0x0008,
    Sync = 0x0010,
    R3FileObj = 0x8000000,
}

pub struct AdsRouterClient {
    client: AdsClientRef,
    target: AdsAddr,
}

impl AdsRouterClient {
    pub fn new(client: AdsClientRef, target: AdsNetId) -> Self {
        AdsRouterClient { client, target: AdsAddr::new(target, 1) }
    }

    pub fn get_state(&mut self) -> AdsResult<AdsState> {
        self.client.read_state(self.target).map(|(a, _)| a)
    }

    pub fn read_info(&mut self) -> AdsResult<AdsRouterInfo> {
        // IdxGrp = ROUTERADSGRP_SYSDATA, IdxOff = ROUTERADSOFFS_SYSDATA_STATUSINFO
        self.client.read_type::<AdsRouterInfo>(self.target, 1, 1)
    }

    pub fn read_ports(&mut self) -> AdsResult<Vec<AdsPortEntry>> {
        // IdxGrp = ROUTERADSGRP_PORTDATA, IdxOff = ROUTERADSOFFS_PORTDATA_ALLPORTS
        let mut entry = AdsPortEntry::default();
        let mut buf = AdsBuffer::with_size(100 * entry.size());
        let len = self.client.read(self.target, 2, 1, buf.as_mut_slice())?;

        if (len % entry.size()) != 0 {
            debug!("AdsRouterClient.read_ports: len={}", len);
            return Err(AdsError::from(0x0706));
        }
        let cnt = len / entry.size();
        let mut entries = Vec::new();
        for _ in 0..cnt {
            buf.dec_type(&mut entry)?;
            entries.push(entry.clone());
        }
        entries.sort_unstable_by(|a, b| a.port.cmp(&b.port));
        Ok(entries)
    }

    // IdxGrp = ROUTERADSGRP_REMOTES, IdxOff = ROUTERADSOFFS_REMOTES_ALL TODO
}

#[derive(Default, Debug, Clone, Copy)]
pub struct AdsRouterInfo {
    pub memory_max: u32,
    pub memory_avl: u32,
    pub ports: u32,
    pub drivers: u32,
    pub routers: u32,
    pub debug: i32,
    pub mailbox_size: u32,
    pub mailbox_used: u32,
}

impl AdsCodable for AdsRouterInfo {
    fn size(&self) -> usize {
        8 * size_of::<u32>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.memory_max)?;
        buf.enc_u32(self.memory_avl)?;
        buf.enc_u32(self.ports)?;
        buf.enc_u32(self.drivers)?;
        buf.enc_u32(self.routers)?;
        buf.enc_i32(self.debug)?;
        buf.enc_u32(self.mailbox_size)?;
        buf.enc_u32(self.mailbox_used)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.memory_max)?;
        buf.dec_u32(&mut self.memory_avl)?;
        buf.dec_u32(&mut self.ports)?;
        buf.dec_u32(&mut self.drivers)?;
        buf.dec_u32(&mut self.routers)?;
        buf.dec_i32(&mut self.debug)?;
        buf.dec_u32(&mut self.mailbox_size)?;
        buf.dec_u32(&mut self.mailbox_used)
    }
}

#[derive(Default, Debug, Clone)]
pub struct AdsPortEntry {
    pub fixed: u32,
    pub port: u16,
    pub queue: u16,
    pub port_type: u32,
    pub port_attr: u32,
    pub name: String,
}

impl AdsCodable for AdsPortEntry {
    fn size(&self) -> usize {
        size_of::<u32>() + size_of::<u16>() + size_of::<u16>() + size_of::<u32>() + size_of::<u32>() + 32
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.fixed)?;
        buf.enc_u16(self.port)?;
        buf.enc_u16(self.queue)?;
        buf.enc_u32(self.port_type)?;
        buf.enc_u32(self.port_attr)?;
        buf.enc_utf8_len(&self.name, 32)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.fixed)?;
        buf.dec_u16(&mut self.port)?;
        buf.dec_u16(&mut self.queue)?;
        buf.dec_u32(&mut self.port_type)?;
        buf.dec_u32(&mut self.port_attr)?;
        buf.dec_utf8_len(&mut self.name, 32)
    }
}
