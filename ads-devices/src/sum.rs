// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)] // TODO
use ads_def::commands::{AdsReadRequest, AdsReadResponse};
use ads_def::commands::{AdsReadWriteRequest, AdsReadWriteResponse};
use ads_def::commands::{AdsWriteRequest, AdsWriteResponse};
use ads_def::*;
use log::debug;
use std::mem::size_of;

pub trait AdsSumResponse {
    fn cnt_res(&self) -> usize;
    fn chk_res(&self) -> AdsResult<()>;
    fn get_res(&self, idx: usize) -> AdsResult<()>;
    fn get_res_buf(&self, idx: usize, size: usize) -> AdsResult<AdsBuffer>;
}

pub struct AdsSumReadClient {
    client: AdsClientRef,
    target: AdsAddr,
    grp: u32,
    vec_req: Vec<AdsReadRequest>,
    vec_res: Vec<AdsReadResponse>,
}

impl AdsSumResponse for AdsSumReadClient {
    fn cnt_res(&self) -> usize {
        self.vec_res.len()
    }
    fn chk_res(&self) -> AdsResult<()> {
        for res in self.vec_res.iter() {
            if res.result != 0 {
                debug!("AdsSumReadClient.chk_res: {:?}", &res);
                return Err(AdsError::from(res.result));
            }
        }
        Ok(())
    }
    fn get_res(&self, idx: usize) -> AdsResult<()> {
        let res = self.get(idx)?;
        if res.result != 0 {
            debug!("AdsSumReadClient.get_res: {:?}", &res);
            return Err(AdsError::from(res.result));
        }
        Ok(())
    }
    fn get_res_buf(&self, idx: usize, size: usize) -> AdsResult<AdsBuffer> {
        let res = self.get(idx)?;
        if res.result != 0 {
            debug!("AdsSumReadClient.get_res_buf: {:?}", &res);
            return Err(AdsError::from(res.result));
        }
        if res.data.len() > size {
            debug!("AdsSumReadClient.get_res_buf: {:?}", &res);
            return Err(AdsError::from(0x0705));
        }
        Ok(AdsBuffer::from_slice(&res.data))
    }
}

impl AdsSumReadClient {
    pub fn new(client: AdsClientRef, target: AdsAddr) -> Self {
        // IdxGrp = ADSIGRP_SUMUP_READ (0xf080)
        // IdxGrp = ADSIGRP_SUMUP_READEX2 (0xf084)
        Self::with_grp(client, target, 0xf084)
    }

    pub fn with_grp(client: AdsClientRef, target: AdsAddr, grp: u32) -> Self {
        AdsSumReadClient {
            client,
            target,
            grp,
            vec_req: Vec::new(),
            vec_res: Vec::new(),
        }
    }

    pub fn reserve(&mut self, cnt: usize) {
        self.vec_req.reserve(cnt);
        self.vec_res.reserve(cnt);
    }

    pub fn clear(&mut self) {
        self.vec_req.clear();
        self.vec_res.clear();
    }

    pub fn add(&mut self, grp: u32, off: u32, len: u32) -> usize {
        let idx = self.vec_req.len();
        self.vec_req.push(AdsReadRequest { grp, off, len });
        idx
    }

    pub fn get(&self, idx: usize) -> AdsResult<&AdsReadResponse> {
        self.vec_res.get(idx).ok_or(AdsError::from(0x0721))
    }

    pub fn execute(&mut self) -> AdsResult<()> {
        let cnt = self.vec_req.len();
        if cnt == 0 {
            return Ok(());
        }
        if cnt > 500 {
            return Err(AdsError::from(0x070A));
        }

        let mut read_len = cnt * 2 * size_of::<u32>();
        let write_len = cnt * 3 * size_of::<u32>();
        let mut write_buf = AdsBuffer::with_capacity(write_len);
        for req in self.vec_req.iter() {
            write_buf.enc_u32(req.grp)?;
            write_buf.enc_u32(req.off)?;
            write_buf.enc_u32(req.len)?;
            read_len += req.len as usize;
        }
        let mut read_buf = AdsBuffer::with_size(read_len);

        self.client.read_write(self.target, self.grp, cnt as u32, write_buf.as_slice(), read_buf.as_mut_slice())?;

        self.vec_res.clear();
        self.vec_res.reserve(cnt);
        for _ in 0..cnt {
            let result = read_buf.dec_into_u32()?;
            let len = read_buf.dec_into_u32()? as usize;
            self.vec_res.push(AdsReadResponse { result, data: vec![0; len] });
        }
        for i in 0..cnt {
            read_buf.dec_slice(self.vec_res[i].data.as_mut_slice())?;
        }

        Ok(())
    }
}

pub struct AdsSumWriteClient {
    client: AdsClientRef,
    target: AdsAddr,
    grp: u32,
    vec_req: Vec<AdsWriteRequest>,
    vec_res: Vec<AdsWriteResponse>,
}

impl AdsSumResponse for AdsSumWriteClient {
    fn cnt_res(&self) -> usize {
        self.vec_res.len()
    }
    fn chk_res(&self) -> AdsResult<()> {
        for res in self.vec_res.iter() {
            if res.result != 0 {
                debug!("AdsSumWriteClient.chk_res: {:?}", &res);
                return Err(AdsError::from(res.result));
            }
        }
        Ok(())
    }
    fn get_res(&self, idx: usize) -> AdsResult<()> {
        let res = self.get(idx)?;
        if res.result != 0 {
            debug!("AdsSumWriteClient.get_res: {:?}", &res);
            return Err(AdsError::from(res.result));
        }
        Ok(())
    }
    fn get_res_buf(&self, _: usize, _: usize) -> AdsResult<AdsBuffer> {
        debug!("AdsSumWriteClient.get_res_buf");
        Err(AdsError::from(0x0701))
    }
}

impl AdsSumWriteClient {
    pub fn new(client: AdsClientRef, target: AdsAddr) -> Self {
        // IdxGrp = ADSIGRP_SUMUP_WRITE
        Self::with_grp(client, target, 0xf081)
    }

    pub fn with_grp(client: AdsClientRef, target: AdsAddr, grp: u32) -> Self {
        AdsSumWriteClient {
            client,
            target,
            grp,
            vec_req: Vec::new(),
            vec_res: Vec::new(),
        }
    }

    pub fn reserve(&mut self, cnt: usize) {
        self.vec_req.reserve(cnt);
        self.vec_res.reserve(cnt);
    }

    pub fn clear(&mut self) {
        self.vec_req.clear();
        self.vec_res.clear();
    }

    pub fn add(&mut self, grp: u32, off: u32, data: &[u8]) -> usize {
        let idx = self.vec_req.len();
        self.vec_req.push(AdsWriteRequest { grp, off, data: data.to_vec() });
        idx
    }

    pub fn get(&self, idx: usize) -> AdsResult<&AdsWriteResponse> {
        self.vec_res.get(idx).ok_or(AdsError::from(0x0721))
    }

    pub fn execute(&mut self) -> AdsResult<()> {
        let cnt = self.vec_req.len();
        if cnt == 0 {
            return Ok(());
        }
        if cnt > 500 {
            return Err(AdsError::from(0x070A));
        }

        let read_len = cnt * size_of::<u32>();
        let mut write_len = cnt * 3 * size_of::<u32>();
        for req in self.vec_req.iter() {
            write_len += req.data.len();
        }
        let mut write_buf = AdsBuffer::with_capacity(write_len);
        for req in self.vec_req.iter() {
            write_buf.enc_u32(req.grp)?;
            write_buf.enc_u32(req.off)?;
            write_buf.enc_u32(req.data.len() as u32)?;
        }
        for req in self.vec_req.iter() {
            write_buf.enc_slice(req.data.as_slice())?;
        }
        let mut read_buf = AdsBuffer::with_size(read_len);

        self.client.read_write(self.target, self.grp, cnt as u32, write_buf.as_slice(), read_buf.as_mut_slice())?;

        self.vec_res.clear();
        self.vec_res.reserve(cnt);
        for _ in 0..cnt {
            self.vec_res.push(AdsWriteResponse { result: read_buf.dec_into_u32()? });
        }

        Ok(())
    }
}

pub struct AdsSumReadWriteClient {
    client: AdsClientRef,
    target: AdsAddr,
    grp: u32,
    vec_req: Vec<AdsReadWriteRequest>,
    vec_res: Vec<AdsReadWriteResponse>,
}

impl AdsSumResponse for AdsSumReadWriteClient {
    fn cnt_res(&self) -> usize {
        self.vec_res.len()
    }
    fn chk_res(&self) -> AdsResult<()> {
        for res in self.vec_res.iter() {
            if res.result != 0 {
                debug!("AdsSumReadWriteClient.chk_res: {:?}", &res);
                return Err(AdsError::from(res.result));
            }
        }
        Ok(())
    }
    fn get_res(&self, idx: usize) -> AdsResult<()> {
        let res = self.get(idx)?;
        if res.result != 0 {
            debug!("AdsSumReadWriteClient.get_res: {:?}", &res);
            return Err(AdsError::from(res.result));
        }
        Ok(())
    }
    fn get_res_buf(&self, idx: usize, size: usize) -> AdsResult<AdsBuffer> {
        let res = self.get(idx)?;
        if res.result != 0 {
            debug!("AdsSumReadWriteClient.get_res_buf: {:?}", &res);
            return Err(AdsError::from(res.result));
        }
        if res.read_data.len() > size {
            debug!("AdsSumReadWriteClient.get_res_buf: {:?}", &res);
            return Err(AdsError::from(0x0705));
        }
        Ok(AdsBuffer::from_slice(&res.read_data))
    }
}

impl AdsSumReadWriteClient {
    pub fn new(client: AdsClientRef, target: AdsAddr) -> Self {
        // IdxGrp = ADSIGRP_SUMUP_READWRITE
        Self::with_grp(client, target, 0xf082)
    }

    pub fn with_grp(client: AdsClientRef, target: AdsAddr, grp: u32) -> Self {
        AdsSumReadWriteClient {
            client,
            target,
            grp,
            vec_req: Vec::new(),
            vec_res: Vec::new(),
        }
    }

    pub fn reserve(&mut self, cnt: usize) {
        self.vec_req.reserve(cnt);
        self.vec_res.reserve(cnt);
    }

    pub fn clear(&mut self) {
        self.vec_req.clear();
        self.vec_res.clear();
    }

    pub fn add(&mut self, grp: u32, off: u32, read_len: usize, write_data: &[u8]) -> usize {
        let idx = self.vec_req.len();
        self.vec_req.push(AdsReadWriteRequest {
            grp,
            off,
            read_len: read_len as u32,
            write_data: write_data.to_vec(),
        });
        idx
    }

    pub fn get(&self, idx: usize) -> AdsResult<&AdsReadWriteResponse> {
        self.vec_res.get(idx).ok_or(AdsError::from(0x0721))
    }

    pub fn execute(&mut self) -> AdsResult<()> {
        let cnt = self.vec_req.len();
        if cnt == 0 {
            return Ok(());
        }
        if cnt > 500 {
            return Err(AdsError::from(0x070A));
        }

        let mut read_len = cnt * 2 * size_of::<u32>();
        let mut write_len = cnt * 4 * size_of::<u32>();
        for req in self.vec_req.iter() {
            write_len += req.write_data.len();
        }
        let mut write_buf = AdsBuffer::with_capacity(write_len);
        for req in self.vec_req.iter() {
            write_buf.enc_u32(req.grp)?;
            write_buf.enc_u32(req.off)?;
            write_buf.enc_u32(req.read_len)?;
            write_buf.enc_u32(req.write_data.len() as u32)?;
            read_len += req.read_len as usize;
        }
        for req in self.vec_req.iter() {
            write_buf.enc_slice(req.write_data.as_slice())?;
        }
        let mut read_buf = AdsBuffer::with_size(read_len);

        self.client.read_write(self.target, self.grp, cnt as u32, write_buf.as_slice(), read_buf.as_mut_slice())?;

        self.vec_res.clear();
        self.vec_res.reserve(cnt);
        for _ in 0..cnt {
            let result = read_buf.dec_into_u32()?;
            let len = read_buf.dec_into_u32()? as usize;
            self.vec_res.push(AdsReadWriteResponse { result, read_data: vec![0; len] });
        }
        for i in 0..self.vec_res.len() {
            read_buf.dec_slice(self.vec_res[i].read_data.as_mut_slice())?;
        }

        Ok(())
    }
}

pub trait AdsSumResponseExt: AdsSumResponse {
    fn get_res_bool(&self, idx: usize) -> AdsResult<bool> {
        self.get_res_buf(idx, size_of::<bool>())?.dec_into_bool()
    }

    fn get_res_u8(&self, idx: usize) -> AdsResult<u8> {
        self.get_res_buf(idx, size_of::<u8>())?.dec_into_u8()
    }

    fn get_res_i16(&self, idx: usize) -> AdsResult<i16> {
        self.get_res_buf(idx, size_of::<i16>())?.dec_into_i16()
    }

    fn get_res_u16(&self, idx: usize) -> AdsResult<u16> {
        self.get_res_buf(idx, size_of::<u16>())?.dec_into_u16()
    }

    fn get_res_i32(&self, idx: usize) -> AdsResult<i32> {
        self.get_res_buf(idx, size_of::<i32>())?.dec_into_i32()
    }

    fn get_res_u32(&self, idx: usize) -> AdsResult<u32> {
        self.get_res_buf(idx, size_of::<u32>())?.dec_into_u32()
    }

    fn get_res_u64(&self, idx: usize) -> AdsResult<u64> {
        self.get_res_buf(idx, size_of::<u64>())?.dec_into_u64()
    }

    fn get_res_utf8(&self, idx: usize) -> AdsResult<String> {
        let mut buf = self.get_res_buf(idx, 256)?;
        buf.dec_into_utf8_len(buf.len())
    }
}

impl AdsSumResponseExt for AdsSumReadClient {}
//impl AdsSumResponseExt for AdsSumWriteClient {}
impl AdsSumResponseExt for AdsSumReadWriteClient {}
