// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_def::*;

#[repr(u32)]
pub enum AdsRegRoot {
    LocalMachine = 200, // HKEY_LOCAL_MACHINE
    CurrentUser = 201,  // HKEY_CURRENT_USER
    ClassesRoot = 202,  // HKEY_CLASSES_ROOT
}

#[repr(u32)]
pub enum AdsRegType {
    None = 0,                  // REG_NONE
    String = 1,                // REG_SZ
    ExpandString = 2,          // REG_EXPAND_SZ
    Binary = 3,                // REG_BINARY
    Dword = 4,                 // REG_DWORD
    DwordBigEndiang = 5,       // REG_DWORD_BIG_ENDIAN
    Link = 6,                  // REG_LINK
    MultiString = 7,           // REG_MULTI_SZ
    ResourceList = 8,          // REG_RESOURCE_LIST
    ResourceDescriptor = 9,    // REG_FULL_RESOURCE_DESCRIPTOR
    ResourceRequirements = 10, // REG_RESOURCE_REQUIREMENTS_LIST
    Qword = 11,                // REG_QWORD
}

pub struct AdsRegistryClient {
    client: AdsClientRef,
    target: AdsAddr,
}

impl AdsRegistryClient {
    pub fn new(client: AdsClientRef, target: AdsNetId) -> Self {
        AdsRegistryClient { client, target: AdsAddr::new(target, 10000) }
    }

    fn unify_key(key: &str) -> String {
        key.replace("/", "\\")
    }

    pub fn get_state(&mut self) -> AdsResult<AdsState> {
        self.client.read_state(self.target).map(|(a, _)| a)
    }

    pub fn set_value(&mut self, key_name: &str, value_name: &str, value_type: AdsRegType, value_data: &[u8]) -> AdsResult<()> {
        let mut buf = AdsBuffer::with_capacity(key_name.len() + value_name.len() + value_data.len() + 6);
        {
            buf.enc_utf8(Self::unify_key(key_name).as_str())?;
            buf.enc_utf8(value_name)?;
            buf.enc_u32(value_type as u32)?;
            buf.enc_slice(value_data)?;
        }
        // IdxGrp = SYSTEMSERVICE_REG_*
        self.client.write(self.target, AdsRegRoot::LocalMachine as u32, 0, buf.as_slice())
    }

    pub fn set_dword(&mut self, key: &str, value: &str, data: u32) -> AdsResult<()> {
        let buf = AdsBuffer::from_u32(data);
        self.set_value(key, value, AdsRegType::Dword, buf.as_slice())
    }

    pub fn query_value(&mut self, key_name: &str, value_name: &str, value_data: &mut [u8]) -> AdsResult<usize> {
        let mut buf = AdsBuffer::with_capacity(key_name.len() + value_name.len() + 2);
        {
            buf.enc_utf8(Self::unify_key(key_name).as_str())?;
            buf.enc_utf8(value_name)?;
        }
        // IdxGrp = SYSTEMSERVICE_REG_*
        let result = self.client.read_write(self.target, AdsRegRoot::LocalMachine as u32, 0, buf.as_slice(), value_data);
        if let Err(err) = result {
            match err.as_code() {
                1 => Err(AdsError::from(0x070C)), // registry key not found
                2 => Err(AdsError::from(0x0710)), // registry value not found
                _ => result,
            }
        } else {
            result
        }
    }

    pub fn query_dword(&mut self, key: &str, value: &str) -> AdsResult<u32> {
        let mut buf = AdsBuffer::with_size_of::<u32>();
        self.query_value(key, value, buf.as_mut_slice())?;
        buf.dec_into_u32()
    }

    fn enum_entries(&mut self, key: &str, off: u32) -> AdsResult<Vec<String>> {
        let mut idx = off;
        let mut entries = Vec::<String>::new();
        let mut read_buf = AdsBuffer::with_size(64);
        let write_buf = AdsBuffer::from_utf8(Self::unify_key(key).as_str());
        loop {
            read_buf.reset();
            // IdxGrp = SYSTEMSERVICE_REG_*, IdxOff = SYSTEMSERVICE_REG_ENUMVALUE_*
            let result = self.client.read_write(self.target, AdsRegRoot::LocalMachine as u32, idx, write_buf.as_slice(), read_buf.as_mut_slice());
            if let Err(err) = result {
                return Err(match err.as_code() {
                    0x70C | 0x745 => {
                        if !entries.is_empty() {
                            return Ok(entries);
                        }
                        err
                    }
                    1 => AdsError::from(0x070C), // registry key not found
                    2 => AdsError::from(0x0710), // registry value not found
                    _ => err,
                });
            }
            entries.push(read_buf.dec_into_utf8()?);
            idx += 1;
        }
    }

    pub fn enum_keys(&mut self, key: &str) -> AdsResult<Vec<String>> {
        self.enum_entries(key, 0)
    }

    pub fn enum_values(&mut self, key: &str) -> AdsResult<Vec<String>> {
        self.enum_entries(key, 0x80000000)
    }
}
