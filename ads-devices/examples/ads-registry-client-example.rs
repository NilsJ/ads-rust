// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_client::*;
use ads_devices::*;
use std::error::Error;
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let (client, addr) = AdsClient::connect()?;
    let mut client = AdsRegistryClient::new(client, addr);

    const REGKEY_SYSTEM: &str = "Software/Beckhoff/TwinCAT3/System";
    const REGVAL_LOCKEDMEMSIZE: &str = "LockedMemSize";

    println!("# State");
    let ads_state = client.get_state()?;
    println!("AdsState: {}", ads_state);

    println!("# Query");
    let mem_size_org = client.query_dword(REGKEY_SYSTEM, REGVAL_LOCKEDMEMSIZE)?;
    println!("LockedMemSize: {:#X} ({})", mem_size_org, mem_size_org);

    println!("# Set");
    client.set_dword(REGKEY_SYSTEM, REGVAL_LOCKEDMEMSIZE, 0x12340000)?;
    let mem_size = client.query_dword(REGKEY_SYSTEM, REGVAL_LOCKEDMEMSIZE)?;
    println!("LockedMemSize: {:#X} ({})", mem_size, mem_size);
    client.set_dword(REGKEY_SYSTEM, REGVAL_LOCKEDMEMSIZE, mem_size_org)?;
    let mem_size = client.query_dword(REGKEY_SYSTEM, REGVAL_LOCKEDMEMSIZE)?;
    println!("LockedMemSize: {:#X} ({})", mem_size, mem_size);

    println!("# Enum");
    let key = "Software/Beckhoff".to_owned();
    fn print_enum_keys_values(client: &mut AdsRegistryClient, key: &str, prefix: &str) {
        if let Ok(keys) = client.enum_keys(&key) {
            for k in keys {
                let key = format!("{}/{}", key, k);
                println!("{}{}/", prefix, k);
                print_enum_keys_values(client, &key, format!("{}  ", prefix).as_str());
            }
        }
        if let Ok(vals) = client.enum_values(&key) {
            for v in vals {
                println!("{}{}", prefix, v);
            }
        }
    }
    print_enum_keys_values(&mut client, &key, "");
    Ok(())
}
