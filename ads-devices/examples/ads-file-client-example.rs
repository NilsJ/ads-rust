// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
use ads_client::*;
use ads_devices::*;
use std::error::Error;
use std::fs;
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let (client, addr) = AdsClient::connect()?;
    let mut client = AdsFileClient::new(client, addr);

    #[cfg(target_family = "windows")]
    let file_local = "C:\\Temp\\test.txt";
    #[cfg(target_family = "unix")]
    let file_local = "/tmp/test.txt";

    let file_root = "/var/tmp/ads-file-client-example";
    let file_name = "test.txt";
    let file_name1 = "test1.bak";
    let file_path = format!("{}/{}", file_root, file_name);
    let file_dest = format!("{}/{}", file_root, file_name1);
    let file_hash = [80, 93, 62, 247, 187, 215, 251, 40, 253, 32, 254, 226, 124, 62, 110, 65];

    const file_size: u64 = 40960;
    let mut file_data = String::new();
    for i in 0..file_size {
        let c = b'0' + (i % 10) as u8;
        file_data.push(c as char);
    }

    // create directory and write file content
    client.create_dir(file_root)?;

    //let handle = client.open(&file_path, AdsFileModeWrite)?;
    //client.write_utf8(handle, "Test234")?;
    //client.close(handle)?;
    client.write_utf8_all(&file_path, &file_data)?;

    // check file status and file hash
    let status = client.status(&file_path)?;
    assert_eq!(file_size, status.size);
    let hash = client.hash(&file_path)?;
    assert_eq!(file_hash, hash.as_slice());

    // download file from remote to local
    client.get(&file_path, file_local)?;
    let local_data = fs::read_to_string(file_local)?;
    assert_eq!(file_data.len(), local_data.len());
    assert_eq!(file_data, local_data);

    // copy file on remote and check file entries
    client.copy(&file_path, &file_dest)?;

    let entries = client.find_all(&format!("{}/*", file_root))?;
    assert!(entries.len() >= 2);
    assert_eq!(file_size, entries.iter().find(|&f| f.name == file_name).unwrap().size);
    assert_eq!(file_size, entries.iter().find(|&f| f.name == file_name1).unwrap().size);

    // remove origin file and move copy to origin on remote
    client.remove(&file_path)?;
    client.rename(&file_dest, &file_path)?;

    // check origin file status and file hash
    let status = client.status(&file_path)?;
    assert_eq!(file_size, status.size);
    let hash = client.hash(&file_path)?;
    assert_eq!(file_hash, hash.as_slice());

    // upload file from local to remote and check it
    client.put(file_local, &file_dest)?;
    let hash = client.hash(&file_dest)?;
    assert_eq!(file_hash, hash.as_slice());

    fs::remove_file(file_local)?;

    // read file content from remote
    //let handle = client.open(&file_path, AdsFileModeRead)?;
    //let readen = client.read_utf8(handle, 1024)?;
    //client.close(handle)?;
    let read_data = client.read_utf8_all(&file_path)?;
    assert_eq!(file_data.len(), read_data.len());
    assert_eq!(file_data, read_data);

    // remote files and directory on remote
    client.remove(&file_path)?;
    client.remove(&file_dest)?;
    client.remove_dir(file_root)?;

    assert!(client.find(file_root).is_err());

    println!("Ok");

    Ok(())
}
