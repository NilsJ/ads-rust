// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_client::*;
use ads_devices::*;
use std::error::Error;
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let (client, addr) = AdsClient::connect()?;
    let mut client = AdsMdpClient::new(client, addr);

    // general area
    println!("DeviceName: {}", client.read_device_name()?);
    println!("HardwareVersion: {}", client.read_hardware_version()?);
    println!("ImageVersion: {}", client.read_image_version()?);

    // device area
    println!("Serial: {}", client.read_serial()?);

    let module_list = client.read_module_list()?;
    let mut module_ids = module_list.ids();
    module_ids.sort_unstable();

    assert!(module_ids.len() > 10);

    println!("# Module List");
    for id in &module_ids {
        println!("  ID={}, Type={}", id, module_list.get_type(id).to_string());
    }

    //let module_names = client.read_module_names(module_ids.as_slice())?;
    //println!("# Module Names");
    //for id in &module_ids {
    //    println!("  ID={}, Name={}", id, module_names[id]);
    //}

    // Access

    println!("# NIC");
    for id in module_list.get_mods(AdsMdpType::NIC) {
        let name = client.read_module_name(id).unwrap_or_default();
        println!("# NIC({}): {}", id, name);
        client.read_nic(id)?;
    }

    println!("# Time");
    if let Some(id) = module_list.get_mod(AdsMdpType::Time) {
        client.read_time(id)?;
    } else {
        println!("? Time module not found!");
    }

    println!("# User");
    if let Some(id) = module_list.get_mod(AdsMdpType::User) {
        client.read_user(id)?;
    } else {
        println!("? User module not found!");
    }

    // SMB

    println!("# TwinCAT");
    if let Some(id) = module_list.get_mod(AdsMdpType::TwinCAT) {
        client.read_twincat(id)?;
    } else {
        println!("? TwinCAT module not found!");
    }

    // Unknown

    println!("# Software");
    if let Some(id) = module_list.get_mod(AdsMdpType::Software) {
        client.read_software(id)?;
    } else {
        println!("? Software module not found!");
    }

    println!("# CPU");
    if let Some(id) = module_list.get_mod(AdsMdpType::CPU) {
        client.read_cpu(id)?;
    } else {
        println!("? CPU module not found!");
    }

    println!("# RAM");
    if let Some(id) = module_list.get_mod(AdsMdpType::RAM) {
        client.read_ram(id)?;
    } else {
        println!("? RAM module not found!");
    }

    // FSO

    // PLC

    // Display

    // EWF

    // FBWF

    println!("# OS");
    if let Some(id) = module_list.get_mod(AdsMdpType::OS) {
        client.read_os(id)?;
    } else {
        println!("? OS module not found!");
    }

    println!("# Fan");
    if let Some(id) = module_list.get_mod(AdsMdpType::Fan) {
        client.read_fan(id)?;
    } else {
        println!("? Fan module not found!");
    }

    println!("# Mainboard");
    if let Some(id) = module_list.get_mod(AdsMdpType::Mainboard) {
        client.read_mainboard(id)?;
    } else {
        println!("? Mainboard module not found!");
    }

    println!("# Disk");
    if let Some(id) = module_list.get_mod(AdsMdpType::Disk) {
        client.read_disk(id)?;
    } else {
        println!("? Disk module not found!");
    }

    // UPS

    println!("# SMART");
    if let Some(id) = module_list.get_mod(AdsMdpType::SMART) {
        client.read_smart(id)?;
    } else {
        println!("? SMART module not found!");
    }

    // MassStorage

    // Misc

    Ok(())
}
