// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_client::*;
use ads_devices::*;
use std::error::Error;
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let (client, addr) = AdsClient::connect()?;
    let mut client = AdsRouterClient::new(client, addr);

    println!("# State");
    let ads_state = client.get_state()?;
    println!("AdsState: {}", ads_state);

    println!("# Info");
    let info = client.read_info()?;
    println!("Memory.Maximum: {}", info.memory_max);
    println!("Memory.Available: {}", info.memory_avl);
    println!("Ports: {}", info.ports);
    println!("Drivers: {}", info.drivers);
    println!("Routers: {}", info.routers);
    println!("Debug: {}", info.debug);
    println!("Mailbox.Size: {}", info.mailbox_size);
    println!("Mailbox.Used: {}", info.mailbox_used);

    Ok(())
}
