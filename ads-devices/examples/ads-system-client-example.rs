// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_client::*;
use ads_devices::*;
use std::error::Error;
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let (client, addr) = AdsClient::connect()?;
    let mut client = AdsSystemClient::new(client, addr);

    println!("# State");
    let ads_state = client.get_state()?;
    println!("AdsState: {}", ads_state);

    println!("# Time");
    let time = client.get_time_utc()?;
    println!("UTC: {}", time);
    let time = client.get_time_local()?;
    println!("Local: {}", time);

    println!("# Product Version");
    let version = client.product_version()?;
    println!("Version: {}", version);

    println!("# Target Info");
    let info = client.target_info()?;
    println!("Target.Type: {}", info.target_type);
    println!("Hardware.Model: {}", info.hardware_model);
    println!("Hardware.Serial: {}", info.hardware_serial);
    println!("Hardware.Version: {}", info.hardware_version);
    println!("Hardware.Date: {}", info.hardware_date);
    println!("Hardware.CPU: {}", info.hardware_cpu);
    println!("Image.Device: {}", info.image_device);
    println!("Image.Version: {}", info.image_version);
    println!("Image.Level: {}", info.image_level);
    println!("Image.OS.Name: {}", info.image_os_name);
    println!("Image.OS.Version: {}", info.image_os_version);
    println!("TwinCAT.Version: {}", info.twincat_version);
    println!("TwinCAT.Revision: {}", info.twincat_revision);
    println!("TwinCAT.Build: {}", info.twincat_build);
    println!("TwinCAT.Level: {}", info.twincat_level);
    println!("NetID: {}", info.net_id);

    Ok(())
}
