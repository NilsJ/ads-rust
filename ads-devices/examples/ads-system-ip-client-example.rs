// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_client::*;
use ads_devices::*;
use std::error::Error;
use std::net::Ipv4Addr;
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let (client, addr) = AdsClient::connect()?;
    let mut client = AdsSystemIpClient::new(client, addr);

    println!("# State");
    let ads_state = client.get_state()?;
    println!("AdsState: {}", ads_state);

    println!("# Host");
    let host_name = client.read_host_name()?;
    println!("Hostname: {}", host_name);
    let host_addr = client.read_host_addr()?;
    println!("Address: {}", host_addr);
    let host_netid = client.read_host_netid()?;
    println!("NetId: {}", host_netid);

    println!("# Adapter");
    let adapter_best = client.read_adapter_best(&Ipv4Addr::new(208, 67, 220, 220))?;
    println!("Best: {}", adapter_best);
    let adapter_list = client.read_adapter_list()?;
    for info in adapter_list {
        println!("  {}: {}", info.name, info.description);
        println!("    IP: {}", info.address);
        println!("    Mask: {}", info.subnetmask);
        println!("    Gateway: {}", info.gateway);
    }

    Ok(())
}
