// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
use ads_client::*;
use ads_devices::*;
use std::error::Error;
use stderrlog::*;

fn print_symbol_info(info: &AdsSymbolInfo) {
    println!("Name: {}", info.name);
    println!("Type: {}", info.type_name);
    println!("TypeId: {}", info.type_id);
    println!("Size: {}", info.size);
    println!("Flags: {:#x}", info.flags);
    println!("Comment: '{}'", info.comment);
    if !info.attr_items.is_empty() {
        println!("Attributes:");
        for e in &info.attr_items {
            println!(" {}: {}", e.0, e.1);
        }
    }
}

fn print_type_info(info: &AdsTypeInfo) {
    println!("Name: {}", info.name);
    println!("Type: {}", info.type_name);
    println!("Size: {}", info.size);
    println!("Flags: {:#x}", info.flags);
    println!("Comment: '{}'", info.comment);
    if !info.array_items.is_empty() {
        println!("ArrayItems:");
        for i in &info.array_items {
            println!(" {}: {}", i.0, i.1);
        }
    }
    if !info.sub_items.is_empty() {
        println!("SubItems:");
        for i in &info.sub_items {
            println!(" {}: {}", i.name, i.type_name);
        }
    }
    if !info.meth_items.is_empty() {
        println!("MethodItems:");
        for i in &info.meth_items {
            println!(" {}: {}", i.0, i.1);
        }
    }
    if !info.attr_items.is_empty() {
        println!("Attributes:");
        for e in &info.attr_items {
            println!(" {}: {}", e.0, e.1);
        }
    }
    if !info.enum_items.is_empty() {
        println!("Enumeration:");
        for e in &info.enum_items {
            println!(" {}: {}", e.0, e.1);
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let (client, addr) = AdsClient::connect()?;
    let mut client = AdsSymbolClient::new(client, AdsAddr::new(addr, 851));

    println!("# State");
    let ads_state = client.get_state()?;
    println!("AdsState: {}", ads_state);

    println!("# Symbols");
    let version = client.read_version()?;
    println!("Version: {}", version);

    println!("# Handle");
    let handle = client.create_handle("MAIN.fTest")?;
    let write_buf = AdsBuffer::from_f64(65.432);
    client.write_by_handle(handle, write_buf.as_slice())?;
    let mut read_buf = AdsBuffer::with_size_of::<f64>();
    client.read_by_handle(handle, read_buf.as_mut_slice())?;
    let value = read_buf.dec_into_f64()?;
    println!("MAIN.fTest: {}", value);
    client.release_handle(handle)?;

    println!("# Symbol Info");
    let info = client.read_symbol_info("MAIN.aTest")?;
    print_symbol_info(&info);

    println!("# Type Info");
    let info = client.read_type_info(&info.type_name)?;
    print_type_info(&info);

    println!("# Symbol Info");
    let info = client.read_symbol_info("MAIN.eTest")?;
    print_symbol_info(&info);

    println!("# Type Info");
    let info = client.read_type_info(&info.type_name)?;
    print_type_info(&info);

    println!("# Symbol Info");
    let info = client.read_symbol_info("MAIN.stTest")?;
    print_symbol_info(&info);

    println!("# Type Info");
    let info = client.read_type_info(&info.type_name)?;
    print_type_info(&info);

    println!("# Symbol Info");
    let info = client.read_symbol_info("MAIN.fbTest")?;
    print_symbol_info(&info);

    println!("# Type Info");
    let info = client.read_type_info(&info.type_name)?;
    print_type_info(&info);

    println!();
    println!("# Symbols List");
    let list = client.read_symbol_list()?;
    for info in list {
        println!("  {}: {}", info.name, info.type_name);
    }

    println!();
    println!("# Types List");
    let list = client.read_type_list()?;
    for info in list {
        println!("  {}: {}", info.name, info.type_name);
        if !info.sub_items.is_empty() {
            for info in info.sub_items {
                println!("    {}: {}", info.name, info.type_name);
            }
        }
    }
    Ok(())
}
