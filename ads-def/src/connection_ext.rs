// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use crate::{
    error::{AdsResult, AdsError},
    addr::AdsAddr,
    port::AdsPort,
    types::AdsState,
};
use std::time::{Instant, Duration};

pub trait AdsPortExt : AdsPort {
    fn reconnect(&mut self, target: AdsAddr, timeout: u64) -> AdsResult<AdsState> {
        let timeout = Instant::now() + Duration::from_millis(timeout);
        let mut sleep = 100;
        while timeout >= Instant::now() {
            if self.connect().is_ok() {
                if let Ok((state, _)) = self.read_state(target) {
                    return Ok(state);
                }
            }
            std::thread::sleep(std::time::Duration::from_millis(sleep));
            sleep += 100;
        }
        Err(AdsError::Internal("Timeout!"))
    }
}
