// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use crate::{
    addr::AdsAddr,
    client::AdsClient,
    coding::{AdsBuffer, AdsCodable},
    error::AdsResult,
    types::AdsState,
};

pub trait AdsClientExt: AdsClient {
    fn write_state(&mut self, target: AdsAddr, state: AdsState) -> AdsResult<()> {
        self.write_control(target, state, 0, &[])
    }

    fn read_type<T: AdsCodable>(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<T> {
        let mut buf = AdsBuffer::with_size_of::<T>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into::<T>()
    }

    fn read_i8(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<i8> {
        let mut buf = AdsBuffer::with_size_of::<i8>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_i8()
    }

    fn read_i16(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<i16> {
        let mut buf = AdsBuffer::with_size_of::<i16>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_i16()
    }

    fn read_i32(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<i32> {
        let mut buf = AdsBuffer::with_size_of::<i32>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_i32()
    }

    fn read_i64(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<i64> {
        let mut buf = AdsBuffer::with_size_of::<i64>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_i64()
    }

    fn read_u8(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<u8> {
        let mut buf = AdsBuffer::with_size_of::<u8>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_u8()
    }

    fn read_u16(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<u16> {
        let mut buf = AdsBuffer::with_size_of::<u16>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_u16()
    }

    fn read_u32(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<u32> {
        let mut buf = AdsBuffer::with_size_of::<u32>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_u32()
    }

    fn read_u64(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<u64> {
        let mut buf = AdsBuffer::with_size_of::<u64>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_u64()
    }

    fn read_f32(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<f32> {
        let mut buf = AdsBuffer::with_size_of::<f32>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_f32()
    }

    fn read_f64(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<f64> {
        let mut buf = AdsBuffer::with_size_of::<f64>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_f64()
    }

    fn read_utf8(&mut self, target: AdsAddr, grp: u32, off: u32, len: usize) -> AdsResult<String> {
        let mut buf = AdsBuffer::with_size(len);
        let readen = self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_utf8_len(readen)
    }

    fn read_bool(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<bool> {
        let mut buf = AdsBuffer::with_size_of::<bool>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_bool()
    }

    fn read_char(&mut self, target: AdsAddr, grp: u32, off: u32) -> AdsResult<char> {
        let mut buf = AdsBuffer::with_size_of::<char>();
        self.read(target, grp, off, buf.as_mut_slice())?;
        buf.dec_into_char()
    }

    fn write_type<T: AdsCodable>(&mut self, target: AdsAddr, grp: u32, off: u32, val: &T) -> AdsResult<()> {
        let buf = AdsBuffer::from_type(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_i8(&mut self, target: AdsAddr, grp: u32, off: u32, val: i8) -> AdsResult<()> {
        let buf = AdsBuffer::from_i8(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_i16(&mut self, target: AdsAddr, grp: u32, off: u32, val: i16) -> AdsResult<()> {
        let buf = AdsBuffer::from_i16(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_i32(&mut self, target: AdsAddr, grp: u32, off: u32, val: i32) -> AdsResult<()> {
        let buf = AdsBuffer::from_i32(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_i64(&mut self, target: AdsAddr, grp: u32, off: u32, val: i64) -> AdsResult<()> {
        let buf = AdsBuffer::from_i64(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_u8(&mut self, target: AdsAddr, grp: u32, off: u32, val: u8) -> AdsResult<()> {
        let buf = AdsBuffer::from_u8(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_u16(&mut self, target: AdsAddr, grp: u32, off: u32, val: u16) -> AdsResult<()> {
        let buf = AdsBuffer::from_u16(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_u32(&mut self, target: AdsAddr, grp: u32, off: u32, val: u32) -> AdsResult<()> {
        let buf = AdsBuffer::from_u32(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_u64(&mut self, target: AdsAddr, grp: u32, off: u32, val: u64) -> AdsResult<()> {
        let buf = AdsBuffer::from_u64(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_f32(&mut self, target: AdsAddr, grp: u32, off: u32, val: f32) -> AdsResult<()> {
        let buf = AdsBuffer::from_f32(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_f64(&mut self, target: AdsAddr, grp: u32, off: u32, val: f64) -> AdsResult<()> {
        let buf = AdsBuffer::from_f64(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_utf8(&mut self, target: AdsAddr, grp: u32, off: u32, val: &str) -> AdsResult<()> {
        let buf = AdsBuffer::from_utf8(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_bool(&mut self, target: AdsAddr, grp: u32, off: u32, val: bool) -> AdsResult<()> {
        let buf = AdsBuffer::from_bool(val);
        self.write(target, grp, off, buf.as_slice())
    }

    fn write_char(&mut self, target: AdsAddr, grp: u32, off: u32, val: char) -> AdsResult<()> {
        let buf = AdsBuffer::from_char(val);
        self.write(target, grp, off, buf.as_slice())
    }
}

impl<T: AdsClient> AdsClientExt for T {}
