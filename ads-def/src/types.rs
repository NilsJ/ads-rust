// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(clippy::from_over_into)]
use crate::coding::{AdsBuffer, AdsCodable};
use crate::error::AdsResult;
use std::fmt::{Display, Error, Formatter};

pub type DevState = u16;
pub type AdsName = String;

#[repr(u16)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum AdsState {
    Invalid,
    ReStart, // Reset
    Run,
    Stop,
    PowerGood,
    Shutdown,
    Config,
    ReConfig,
    // Invalid = 0,
    // Idle = 1,
    // Reset = 2,
    // Init = 3,
    // Start = 4,
    // Run = 5,
    // Stop = 6,
    // SaveCfg = 7,
    // LoadCfg = 8,
    // PowerFailure = 9,
    // PowerGood = 10,
    // Error = 11,
    // Shutdown = 12,
    // Suspend = 13,
    // Resume = 14,
    // Config = 15,
    // Reconfig = 16,
    // Stopping = 17,
    // Incompatible = 18,
    // Exception = 19,
}

impl From<u16> for AdsState {
    fn from(v: u16) -> Self {
        match v {
            0 => AdsState::Invalid,
            2 => AdsState::ReStart,
            5 => AdsState::Run,
            6 => AdsState::Stop,
            10 => AdsState::PowerGood,
            12 => AdsState::Shutdown,
            15 => AdsState::Config,
            16 => AdsState::ReConfig,
            _ => AdsState::Invalid,
        }
    }
}

impl Into<u16> for AdsState {
    fn into(self) -> u16 {
        match self {
            AdsState::Invalid => 0,
            AdsState::ReStart => 2,
            AdsState::Run => 5,
            AdsState::Stop => 6,
            AdsState::PowerGood => 10,
            AdsState::Shutdown => 12,
            AdsState::Config => 15,
            AdsState::ReConfig => 16,
        }
    }
}

impl Display for AdsState {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            AdsState::Invalid => write!(f, "Invalid"),
            AdsState::ReStart => write!(f, "ReStart"),
            AdsState::Run => write!(f, "Run"),
            AdsState::Stop => write!(f, "Stop"),
            AdsState::PowerGood => write!(f, "PowerGood"),
            AdsState::Shutdown => write!(f, "Shutdown"),
            AdsState::Config => write!(f, "Config"),
            AdsState::ReConfig => write!(f, "ReConfig"),
        }
    }
}

#[repr(C)]
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct AdsVersion {
    version: u8,
    revision: u8,
    build: u16,
}

impl AdsCodable for AdsVersion {
    fn size(&self) -> usize {
        4
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u8(self.version)?;
        buf.enc_u8(self.revision)?;
        buf.enc_u16(self.build)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u8(&mut self.version)?;
        buf.dec_u8(&mut self.revision)?;
        buf.dec_u16(&mut self.build)
    }
}

impl Display for AdsVersion {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}.{}.{}", self.version, self.revision, self.build)
    }
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct AdsGuid {
    data1: u32,
    data2: u16,
    data3: u16,
    data4: [u8; 8],
}

impl AdsCodable for AdsGuid {
    fn size(&self) -> usize {
        16
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.data1)?;
        buf.enc_u16(self.data2)?;
        buf.enc_u16(self.data3)?;
        buf.enc_slice(&self.data4)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.data1)?;
        buf.dec_u16(&mut self.data2)?;
        buf.dec_u16(&mut self.data3)?;
        buf.dec_slice(&mut self.data4)
    }
}

impl Display for AdsGuid {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(
            f,
            "{:08X}-{:04X}-{:04X}-{:02X}{:02X}-{:02X}{:02X}{:02X}{:02X}{:02X}{:02X}",
            self.data1, self.data2, self.data3, self.data4[0], self.data4[1], self.data4[2], self.data4[3], self.data4[4], self.data4[5], self.data4[6], self.data4[7]
        )
    }
}
