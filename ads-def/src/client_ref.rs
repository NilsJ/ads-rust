// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
// TODO optimize borrow, add borrow and deref traits?
use crate::{
    addr::AdsAddr,
    client::AdsClient,
    error::AdsResult,
    types::{AdsName, AdsState, AdsVersion, DevState},
};
use std::{
    cell::{Ref, RefCell, RefMut},
    clone::Clone,
    ops::Deref,
    rc::Rc,
};

pub struct AdsClientRef {
    cell: Rc<RefCell<dyn AdsClient>>,
}

impl AdsClientRef {
    pub fn new<T: AdsClient + 'static>(client: T) -> Self {
        AdsClientRef { cell: Rc::new(RefCell::new(client)) }
    }
    fn borrow(&self) -> Ref<dyn AdsClient> {
        self.cell.deref().borrow()
    }
    fn borrow_mut(&mut self) -> RefMut<dyn AdsClient> {
        self.cell.deref().borrow_mut()
    }
}

impl Clone for AdsClientRef {
    fn clone(&self) -> Self {
        AdsClientRef { cell: self.cell.clone() }
    }
}

impl AdsClient for AdsClientRef {
    fn get_local(&self) -> AdsResult<AdsAddr> {
        self.borrow().get_local()
    }

    fn read_info(&mut self, target: AdsAddr) -> AdsResult<(AdsName, AdsVersion)> {
        self.borrow_mut().read_info(target)
    }

    fn read_state(&mut self, target: AdsAddr) -> AdsResult<(AdsState, DevState)> {
        self.borrow_mut().read_state(target)
    }

    fn write_control(&mut self, target: AdsAddr, ads_state: AdsState, dev_state: DevState, data: &[u8]) -> AdsResult<()> {
        self.borrow_mut().write_control(target, ads_state, dev_state, data)
    }

    fn read(&mut self, target: AdsAddr, grp: u32, off: u32, data: &mut [u8]) -> AdsResult<usize> {
        self.borrow_mut().read(target, grp, off, data)
    }

    fn write(&mut self, target: AdsAddr, grp: u32, off: u32, data: &[u8]) -> AdsResult<()> {
        self.borrow_mut().write(target, grp, off, data)
    }

    fn read_write(&mut self, target: AdsAddr, grp: u32, off: u32, write_data: &[u8], read_data: &mut [u8]) -> AdsResult<usize> {
        self.borrow_mut().read_write(target, grp, off, write_data, read_data)
    }
}
