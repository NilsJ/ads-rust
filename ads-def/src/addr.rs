// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use crate::coding::{AdsBuffer, AdsCodable};
use crate::error::{AdsError, AdsResult};
use log::debug;
use std::fmt::{self, Display};
use std::net::Ipv4Addr;
use std::str::FromStr;

pub type AdsPort = u16;

#[repr(C)]
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, Hash)]
pub struct AdsNetId([u8; 6]);

impl AdsNetId {
    pub fn new(n1: u8, n2: u8, n3: u8, n4: u8, n5: u8, n6: u8) -> Self {
        Self([n1, n2, n3, n4, n5, n6])
    }

    #[inline]
    pub fn byte(&self, idx: usize) -> u8 {
        self.0[idx]
    }

    #[inline]
    pub fn bytes(&self) -> &[u8] {
        &self.0
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.0[0..4] == [0u8; 4]
    }

    #[inline]
    pub fn is_loopback(&self) -> bool {
        self.0[0..4] == [127, 0, 0, 1]
    }

    pub fn from_addr(a: &[u8; 4]) -> Self {
        Self([a[0], a[1], a[2], a[3], 1, 1])
    }

    pub fn from_ipv4(addr: &Ipv4Addr) -> Self {
        let a = addr.octets();
        Self([a[0], a[1], a[2], a[3], 1, 1])
    }

    pub fn from_slice(s: &[u8]) -> AdsResult<Self> {
        if s.len() != 6 {
            debug!("AdsNetId::from_slice: {:?}", &s);
            return Err(AdsError::from(0x000E));
        }
        let mut b = [0u8; 6];
        b.copy_from_slice(s);
        Ok(Self(b))
    }
}

impl Default for AdsNetId {
    fn default() -> Self {
        Self([0, 0, 0, 0, 1, 1])
    }
}

impl Display for AdsNetId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}.{}.{}.{}.{}.{}", self.0[0], self.0[1], self.0[2], self.0[3], self.0[4], self.0[5])
    }
}

impl From<[u8; 6]> for AdsNetId {
    fn from(b: [u8; 6]) -> Self {
        Self(b)
    }
}

impl From<&str> for AdsNetId {
    fn from(s: &str) -> Self {
        AdsNetId::from_str(s).unwrap_or_default()
    }
}

impl FromStr for AdsNetId {
    type Err = AdsError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split('.').collect();
        if parts.len() != 6 {
            debug!("AdsNetId::from_str: {}", s);
            return Err(AdsError::from(0x000E));
        }
        let mut bytes = [0; 6];
        for (i, p) in parts.into_iter().enumerate() {
            match p.parse::<u8>() {
                Ok(v) => bytes[i] = v,
                Err(_) => {
                    debug!("AdsNetId::from_str: {}", s);
                    return Err(AdsError::from(0x000F));
                }
            }
        }
        Ok(Self(bytes))
    }
}

impl AdsCodable for AdsNetId {
    fn size(&self) -> usize {
        6
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_slice(&self.0)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_slice(&mut self.0)
    }
}

#[repr(C)]
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Hash)]
pub struct AdsAddr {
    pub id: AdsNetId,
    pub port: AdsPort,
}

impl AdsAddr {
    pub fn new(id: AdsNetId, port: AdsPort) -> Self {
        Self { id, port }
    }

    pub fn from_id(id: AdsNetId) -> Self {
        Self { id, port: 10000 }
    }

    pub fn from_port(port: AdsPort) -> Self {
        Self { id: AdsNetId::default(), port }
    }

    pub fn from_slice(s: &[u8]) -> Self {
        Self {
            id: AdsNetId::from_slice(&s[0..6]).unwrap(),
            port: u16::from_le_bytes(s[6..8].try_into().unwrap()),
        }
    }
}

impl Default for AdsAddr {
    fn default() -> Self {
        Self { id: AdsNetId::default(), port: 10000 }
    }
}

impl Display for AdsAddr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.id, self.port)
    }
}

impl From<&str> for AdsAddr {
    fn from(s: &str) -> Self {
        AdsAddr::from_str(s).unwrap_or_default()
    }
}

impl FromStr for AdsAddr {
    type Err = AdsError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.contains('.') {
            if s.contains(':') {
                let parts: Vec<&str> = s.split(':').collect();
                if parts.len() != 2 {
                    debug!("AdsAddr.from_str: {}", s);
                    return Err(AdsError::from(0x000E));
                }
                Ok(Self {
                    id: AdsNetId::from_str(parts[0])?,
                    port: u16::from_str(parts[1]).map_err(|_| AdsError::from(0x0018))?,
                })
            } else {
                Ok(Self { id: AdsNetId::from_str(s)?, port: 10000 })
            }
        } else {
            Ok(Self {
                id: AdsNetId::default(),
                port: u16::from_str(s).map_err(|_| AdsError::from(0x0018))?,
            })
        }
    }
}

impl AdsCodable for AdsAddr {
    fn size(&self) -> usize {
        8
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_type(&self.id)?;
        buf.enc_u16(self.port)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_type(&mut self.id)?;
        buf.dec_u16(&mut self.port)
    }
}
