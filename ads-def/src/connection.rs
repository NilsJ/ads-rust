// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use crate::commands::*;
use crate::{
    addr::AdsAddr,
    client::AdsClient,
    error::{AdsError, AdsResult},
    types::{AdsName, AdsState, AdsVersion, DevState},
};
use log::debug;

pub trait AdsConnection {
    fn local_addr(&self) -> AdsResult<AdsAddr>;

    fn connect(&mut self) -> AdsResult<()>;
    fn disconnect(&mut self);

    fn send_recv(&mut self, target: AdsAddr, req: AdsReq) -> AdsResult<AdsRes>;
}

impl<T: AdsConnection> AdsClient for T {
    fn get_local(&self) -> AdsResult<AdsAddr> {
        self.local_addr()
    }

    fn read_info(&mut self, target: AdsAddr) -> AdsResult<(AdsName, AdsVersion)> {
        let req = AdsReq::ReadInfo(AdsReadInfoRequest::default());
        let res = self.send_recv(target, req)?;

        if let AdsRes::ReadInfo(r) = res {
            if r.result != 0 {
                debug!("AdsConnection.read_info: {:?}", &r);
                return Err(AdsError::from(r.result));
            }
            Ok((r.name, r.version))
        } else {
            panic!("Invalid response!");
        }
    }

    fn read_state(&mut self, target: AdsAddr) -> AdsResult<(AdsState, DevState)> {
        let req = AdsReq::ReadState(AdsReadStateRequest::default());
        let res = self.send_recv(target, req)?;

        if let AdsRes::ReadState(r) = res {
            if r.result != 0 {
                debug!("AdsConnection.read_state: {:?}", &r);
                return Err(AdsError::from(r.result));
            }
            Ok((AdsState::from(r.ads_state), r.dev_state))
        } else {
            panic!("Invalid response!");
        }
    }

    fn write_control(&mut self, target: AdsAddr, ads_state: AdsState, dev_state: DevState, data: &[u8]) -> AdsResult<()> {
        let req = AdsReq::WriteControl(AdsWriteControlRequest {
            ads_state: ads_state.into(),
            dev_state,
            data: data.to_vec(),
        });
        let res = self.send_recv(target, req)?;

        if let AdsRes::WriteControl(r) = res {
            if r.result != 0 {
                debug!("AdsConnection.write_control: {:?}", &r);
                return Err(AdsError::from(r.result));
            }
            Ok(())
        } else {
            panic!("Invalid response!");
        }
    }

    fn read(&mut self, target: AdsAddr, grp: u32, off: u32, data: &mut [u8]) -> AdsResult<usize> {
        let req = AdsReq::Read(AdsReadRequest { grp, off, len: data.len() as u32 });
        let res = self.send_recv(target, req)?;

        if let AdsRes::Read(r) = res {
            if r.result != 0 {
                debug!("AdsConnection.read: {:?}", &r);
                return Err(AdsError::from(r.result));
            }
            if r.data.len() > data.len() {
                debug!("AdsConnection.read: {:?}", &r);
                return Err(AdsError::from(0x000E));
            }
            if r.data.len() < data.len() {
                data[..r.data.len()].copy_from_slice(&r.data);
            } else {
                data.copy_from_slice(&r.data);
            }
            Ok(r.data.len())
        } else {
            panic!("Invalid response!");
        }
    }

    fn write(&mut self, target: AdsAddr, grp: u32, off: u32, data: &[u8]) -> AdsResult<()> {
        let req = AdsReq::Write(AdsWriteRequest { grp, off, data: data.to_vec() });
        let res = self.send_recv(target, req)?;

        if let AdsRes::Write(r) = res {
            if r.result != 0 {
                debug!("AdsConnection.write: {:?}", &r);
                return Err(AdsError::from(r.result));
            }
            Ok(())
        } else {
            panic!("Invalid response!");
        }
    }

    fn read_write(&mut self, target: AdsAddr, grp: u32, off: u32, write_data: &[u8], read_data: &mut [u8]) -> AdsResult<usize> {
        let req = AdsReq::ReadWrite(AdsReadWriteRequest {
            grp,
            off,
            read_len: read_data.len() as u32,
            write_data: write_data.to_vec(),
        });
        let res = self.send_recv(target, req)?;

        if let AdsRes::ReadWrite(r) = res {
            if r.result != 0 {
                debug!("AdsConnection.read_write: {:?}", &r);
                return Err(AdsError::from(r.result));
            }
            if r.read_data.len() > read_data.len() {
                debug!("AdsConnection.read_write: {:?}", &r);
                return Err(AdsError::from(0x000E));
            }
            if r.read_data.len() < read_data.len() {
                read_data[..r.read_data.len()].copy_from_slice(&r.read_data);
            } else {
                read_data.copy_from_slice(&r.read_data);
            }
            Ok(r.read_data.len())
        } else {
            panic!("Invalid response!");
        }
    }
}
