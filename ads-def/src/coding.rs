// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(unused_macros)]
#![allow(dead_code)] // TODO
use crate::error::{AdsError, AdsResult};
use log::debug;
use std::mem::size_of;

#[derive(Default, Debug)]
pub struct AdsBuffer {
    pos: usize,
    buf: Vec<u8>,
}

pub trait AdsCodable: Default {
    fn size(&self) -> usize;
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()>;
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()>;
}

macro_rules! enc_prim {
    ($self:ident, $val:expr, $type:tt) => {{
        let b = $type::to_le_bytes($val);
        $self.buf.extend_from_slice(&b);
        Ok(())
    }};
}

macro_rules! dec_prim {
    ($self:ident, $type:tt) => {{
        const N: usize = size_of::<$type>();
        let mut b = [0; N];
        b.copy_from_slice($self.get(N)?);
        AdsResult::Ok($type::from_le_bytes(b))
    }};
}

macro_rules! dec_slice {
    ($slice:expr, $type:tt) => {{
        const N: usize = size_of::<$type>();
        let mut b = [0; N];
        b.copy_from_slice($slice);
        $type::from_le_bytes(b)
    }};
}

impl AdsBuffer {
    pub fn new() -> Self {
        Self { pos: 0, buf: Vec::new() }
    }

    pub fn with_size(size: usize) -> Self {
        Self::from_vec(vec![0; size])
    }

    pub fn with_size_of<T>() -> Self {
        Self::from_vec(vec![0; size_of::<T>()])
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self::from_vec(Vec::with_capacity(capacity))
    }

    pub fn with_capacity_of<T>() -> Self {
        Self::from_vec(Vec::with_capacity(size_of::<T>()))
    }

    pub fn from_vec(v: Vec<u8>) -> Self {
        Self { pos: 0, buf: v }
    }

    pub fn from_slice(s: &[u8]) -> Self {
        Self { pos: 0, buf: Vec::from(s) }
    }

    pub fn from_i8(val: i8) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_i16(val: i16) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_i32(val: i32) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_i64(val: i64) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_u8(val: u8) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_u16(val: u16) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_u32(val: u32) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_u64(val: u64) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_f32(val: f32) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_f64(val: f64) -> Self {
        Self::from_slice(&val.to_le_bytes())
    }

    pub fn from_bool(val: bool) -> Self {
        Self::from_u8(val as u8)
    }

    pub fn from_char(val: char) -> Self {
        Self::from_u8(val as u8)
    }

    pub fn from_utf8(val: &str) -> Self {
        Self::from_utf8_len(val, val.len() + 1)
    }

    pub fn from_utf8_len(val: &str, len: usize) -> Self {
        let b = val.as_bytes();
        let mut v = vec![0; len];
        v[0..b.len()].copy_from_slice(b);
        Self::from_vec(v)
    }

    pub fn from_type<T: AdsCodable>(val: &T) -> Self {
        let mut s = Self::with_capacity(val.size());
        val.encode(&mut s).unwrap();
        Self::from_vec(s.to_vec())
    }

    pub fn pos(&self) -> usize {
        self.pos
    }

    pub fn len(&self) -> usize {
        self.buf.len()
    }

    pub fn remain(&self) -> usize {
        self.buf.len() - self.pos
    }

    pub fn reset(&mut self) {
        self.pos = 0;
    }

    pub fn resize(&mut self, len: usize) {
        self.reset();
        self.buf.resize(len, 0);
    }

    pub fn reserve(&mut self, len: usize) {
        self.buf.reserve(len)
    }

    pub fn skip(&mut self, size: usize) {
        self.pos += size;
    }

    pub fn is_empty(&self) -> bool {
        self.buf.is_empty()
    }

    pub fn to_vec(&self) -> Vec<u8> {
        self.buf.clone()
    }

    pub fn as_vec(&self) -> &Vec<u8> {
        &self.buf
    }

    pub fn as_mut_vec(&mut self) -> &mut Vec<u8> {
        &mut self.buf
    }

    pub fn as_slice(&self) -> &[u8] {
        self.buf.as_slice()
    }

    pub fn as_mut_slice(&mut self) -> &mut [u8] {
        self.buf.as_mut_slice()
    }

    pub fn enc_i8(&mut self, val: i8) -> AdsResult<()> {
        enc_prim!(self, val, i8)
    }

    pub fn enc_i16(&mut self, val: i16) -> AdsResult<()> {
        enc_prim!(self, val, i16)
    }

    pub fn enc_i32(&mut self, val: i32) -> AdsResult<()> {
        enc_prim!(self, val, i32)
    }

    pub fn enc_i64(&mut self, val: i64) -> AdsResult<()> {
        enc_prim!(self, val, i64)
    }

    pub fn enc_u8(&mut self, val: u8) -> AdsResult<()> {
        enc_prim!(self, val, u8)
    }

    pub fn enc_u16(&mut self, val: u16) -> AdsResult<()> {
        enc_prim!(self, val, u16)
    }

    pub fn enc_u32(&mut self, val: u32) -> AdsResult<()> {
        enc_prim!(self, val, u32)
    }

    pub fn enc_u64(&mut self, val: u64) -> AdsResult<()> {
        enc_prim!(self, val, u64)
    }

    pub fn enc_f32(&mut self, val: f32) -> AdsResult<()> {
        enc_prim!(self, val, f32)
    }

    pub fn enc_f64(&mut self, val: f64) -> AdsResult<()> {
        enc_prim!(self, val, f64)
    }

    pub fn enc_bool(&mut self, val: bool) -> AdsResult<()> {
        self.buf.push(val as u8);
        Ok(())
    }

    pub fn enc_char(&mut self, val: char) -> AdsResult<()> {
        self.buf.push(val as u8);
        Ok(())
    }

    pub fn enc_slice(&mut self, val: &[u8]) -> AdsResult<()> {
        self.buf.extend_from_slice(val);
        Ok(())
    }

    pub fn enc_utf8(&mut self, val: &str) -> AdsResult<()> {
        self.enc_utf8_len(val, val.len() + 1)
    }

    pub fn enc_utf8_len(&mut self, val: &str, len: usize) -> AdsResult<()> {
        let b = val.as_bytes();
        self.buf.extend_from_slice(b);
        for _ in b.len()..len {
            self.buf.push(0);
        }
        Ok(())
    }

    pub fn enc_type<T: AdsCodable>(&mut self, val: &T) -> AdsResult<()> {
        T::encode(val, self)
    }

    fn get(&mut self, len: usize) -> AdsResult<&[u8]> {
        let pos = self.pos;
        let end = pos + len;
        if end > self.buf.len() {
            debug!("AdsBuffer.get: {:?}", &self);
            return Err(AdsError::from(0x0019));
        }
        self.pos += len;
        Ok(&self.buf[pos..end])
    }

    pub fn dec_i8(&mut self, val: &mut i8) -> AdsResult<()> {
        *val = dec_prim!(self, i8)?;
        Ok(())
    }

    pub fn dec_i16(&mut self, val: &mut i16) -> AdsResult<()> {
        *val = dec_prim!(self, i16)?;
        Ok(())
    }

    pub fn dec_i32(&mut self, val: &mut i32) -> AdsResult<()> {
        *val = dec_prim!(self, i32)?;
        Ok(())
    }

    pub fn dec_i64(&mut self, val: &mut i64) -> AdsResult<()> {
        *val = dec_prim!(self, i64)?;
        Ok(())
    }

    pub fn dec_u8(&mut self, val: &mut u8) -> AdsResult<()> {
        *val = dec_prim!(self, u8)?;
        Ok(())
    }

    pub fn dec_u16(&mut self, val: &mut u16) -> AdsResult<()> {
        *val = dec_prim!(self, u16)?;
        Ok(())
    }

    pub fn dec_u32(&mut self, val: &mut u32) -> AdsResult<()> {
        *val = dec_prim!(self, u32)?;
        Ok(())
    }

    pub fn dec_u64(&mut self, val: &mut u64) -> AdsResult<()> {
        *val = dec_prim!(self, u64)?;
        Ok(())
    }

    pub fn dec_f32(&mut self, val: &mut f32) -> AdsResult<()> {
        *val = dec_prim!(self, f32)?;
        Ok(())
    }

    pub fn dec_f64(&mut self, val: &mut f64) -> AdsResult<()> {
        *val = dec_prim!(self, f64)?;
        Ok(())
    }

    pub fn dec_bool(&mut self, val: &mut bool) -> AdsResult<()> {
        *val = self.get(1)?[0] != 0;
        Ok(())
    }

    pub fn dec_char(&mut self, val: &mut char) -> AdsResult<()> {
        *val = self.get(0)?[0] as char;
        Ok(())
    }

    pub fn dec_utf8(&mut self, val: &mut String) -> AdsResult<()> {
        self.dec_utf8_len(val, val.len() + 1)
    }

    pub fn dec_utf8_len(&mut self, val: &mut String, size: usize) -> AdsResult<()> {
        let b = self.get(size)?;
        let mut v = std::str::from_utf8(b)?;
        if let Some(n) = v.find(char::from(0)) {
            v = &v[..n];
        }
        *val = v.to_string();
        Ok(())
    }

    pub fn dec_slice(&mut self, val: &mut [u8]) -> AdsResult<()> {
        val.copy_from_slice(self.get(val.len())?);
        Ok(())
    }

    pub fn dec_type<T: AdsCodable>(&mut self, val: &mut T) -> AdsResult<()> {
        T::decode(val, self)
    }

    pub fn dec_into_i8(&mut self) -> AdsResult<i8> {
        dec_prim!(self, i8)
    }

    pub fn dec_into_i16(&mut self) -> AdsResult<i16> {
        dec_prim!(self, i16)
    }

    pub fn dec_into_i32(&mut self) -> AdsResult<i32> {
        dec_prim!(self, i32)
    }

    pub fn dec_into_i64(&mut self) -> AdsResult<i64> {
        dec_prim!(self, i64)
    }

    pub fn dec_into_u8(&mut self) -> AdsResult<u8> {
        dec_prim!(self, u8)
    }

    pub fn dec_into_u16(&mut self) -> AdsResult<u16> {
        dec_prim!(self, u16)
    }

    pub fn dec_into_u32(&mut self) -> AdsResult<u32> {
        dec_prim!(self, u32)
    }

    pub fn dec_into_u64(&mut self) -> AdsResult<u64> {
        dec_prim!(self, u64)
    }

    pub fn dec_into_f32(&mut self) -> AdsResult<f32> {
        dec_prim!(self, f32)
    }

    pub fn dec_into_f64(&mut self) -> AdsResult<f64> {
        dec_prim!(self, f64)
    }

    pub fn dec_into_bool(&mut self) -> AdsResult<bool> {
        Ok(self.get(1)?[0] != 0)
    }

    pub fn dec_into_char(&mut self) -> AdsResult<char> {
        Ok(self.get(1)?[0] as char)
    }

    pub fn dec_into_utf8(&mut self) -> AdsResult<String> {
        self.dec_into_utf8_len(self.buf.len())
    }

    pub fn dec_into_utf8_len(&mut self, size: usize) -> AdsResult<String> {
        let b = self.get(size)?;
        let mut v = std::str::from_utf8(b)?;
        if let Some(n) = v.find(char::from(0)) {
            v = &v[..n];
        }
        Ok(v.to_string())
    }

    pub fn dec_into_slice(&mut self, size: usize) -> AdsResult<&[u8]> {
        self.get(size)
    }

    pub fn dec_into_type<T: AdsCodable>(&mut self) -> AdsResult<T> {
        let mut val = T::default();
        T::decode(&mut val, self)?;
        Ok(val)
    }

    pub fn dec_into<T: AdsCodable>(&mut self) -> AdsResult<T> {
        let mut val = T::default();
        T::decode(&mut val, self)?;
        Ok(val)
    }
}
