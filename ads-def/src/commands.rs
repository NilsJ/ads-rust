// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)] // TODO
use crate::{
    coding::{AdsBuffer, AdsCodable},
    error::{AdsError, AdsResult},
    header::AdsCommandId,
    types::AdsVersion,
};
use log::debug;
use std::mem::size_of;

pub enum AdsReq {
    Read(AdsReadRequest),
    Write(AdsWriteRequest),
    ReadWrite(AdsReadWriteRequest),
    ReadState(AdsReadStateRequest),
    WriteControl(AdsWriteControlRequest),
    ReadInfo(AdsReadInfoRequest),
}

pub enum AdsRes {
    Read(AdsReadResponse),
    Write(AdsWriteResponse),
    ReadWrite(AdsReadWriteResponse),
    ReadState(AdsReadStateResponse),
    WriteControl(AdsWriteControlResponse),
    ReadInfo(AdsReadInfoResponse),
}

pub trait AdsRequest: AdsCodable {
    fn command(&self) -> u16;
}

pub trait AdsResponse: AdsCodable {
    fn command(&self) -> u16;
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadInfoRequest {}

impl AdsRequest for AdsReadInfoRequest {
    fn command(&self) -> u16 {
        AdsCommandId::ReadInfo as u16
    }
}

impl AdsCodable for AdsReadInfoRequest {
    fn size(&self) -> usize {
        0
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        Ok(())
    }
    fn decode(&mut self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadInfoResponse {
    pub result: u32,
    pub version: AdsVersion,
    pub name: String,
}

impl AdsResponse for AdsReadInfoResponse {
    fn command(&self) -> u16 {
        AdsCommandId::ReadInfo as u16
    }
}

impl AdsCodable for AdsReadInfoResponse {
    fn size(&self) -> usize {
        24
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.result)?;
        buf.enc_type(&self.version)?;
        buf.enc_utf8_len(&self.name, 16)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.result)?;
        if self.result != 0 {
            debug!("AdsReadInfoResponse.decode: {}", AdsError::from(self.result));
            return Err(AdsError::from(self.result));
        }
        buf.dec_type(&mut self.version)?;
        buf.dec_utf8_len(&mut self.name, 16)
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadRequest {
    pub grp: u32,
    pub off: u32,
    pub len: u32,
}

impl AdsRequest for AdsReadRequest {
    fn command(&self) -> u16 {
        AdsCommandId::Read as u16
    }
}

impl AdsCodable for AdsReadRequest {
    fn size(&self) -> usize {
        3 * size_of::<u32>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.grp)?;
        buf.enc_u32(self.off)?;
        buf.enc_u32(self.len)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.grp)?;
        buf.dec_u32(&mut self.off)?;
        buf.dec_u32(&mut self.len)
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadResponse {
    pub result: u32,
    pub data: Vec<u8>,
}

impl AdsResponse for AdsReadResponse {
    fn command(&self) -> u16 {
        AdsCommandId::Read as u16
    }
}

impl AdsCodable for AdsReadResponse {
    fn size(&self) -> usize {
        8 + self.data.len()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.result)?;
        buf.enc_slice(&self.data)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.result)?;
        if self.result != 0 {
            debug!("AdsReadResponse.decode: {}", AdsError::from(self.result));
            return Err(AdsError::from(self.result));
        }
        let len = buf.dec_into_u32()? as usize;
        if len != 0 {
            self.data = buf.dec_into_slice(len)?.to_vec();
        }
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsWriteRequest {
    pub grp: u32,
    pub off: u32,
    pub data: Vec<u8>,
}

impl AdsRequest for AdsWriteRequest {
    fn command(&self) -> u16 {
        AdsCommandId::Write as u16
    }
}

impl AdsCodable for AdsWriteRequest {
    fn size(&self) -> usize {
        3 * size_of::<u32>() + self.data.len()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.grp)?;
        buf.enc_u32(self.off)?;
        if self.data.is_empty() {
            buf.enc_u32(0)?;
        } else {
            buf.enc_u32(self.data.len() as u32)?;
            buf.enc_slice(self.data.as_slice())?;
        }
        Ok(())
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.grp)?;
        buf.dec_u32(&mut self.off)?;
        let len = buf.dec_into_u32()? as usize;
        self.data.clear();
        if len != 0 {
            self.data = buf.dec_into_slice(len)?.to_vec();
        }
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsWriteResponse {
    pub result: u32,
}

impl AdsResponse for AdsWriteResponse {
    fn command(&self) -> u16 {
        AdsCommandId::Write as u16
    }
}

impl AdsCodable for AdsWriteResponse {
    fn size(&self) -> usize {
        4
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.result)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.result)?;
        if self.result != 0 {
            debug!("AdsWriteResponse.decode: {}", AdsError::from(self.result));
            return Err(AdsError::from(self.result));
        }
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadWriteRequest {
    pub grp: u32,
    pub off: u32,
    pub read_len: u32,
    pub write_data: Vec<u8>,
}

impl AdsRequest for AdsReadWriteRequest {
    fn command(&self) -> u16 {
        AdsCommandId::ReadWrite as u16
    }
}

impl AdsCodable for AdsReadWriteRequest {
    fn size(&self) -> usize {
        4 * size_of::<u32>() + self.write_data.len()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.grp)?;
        buf.enc_u32(self.off)?;
        buf.enc_u32(self.read_len)?;
        if self.write_data.is_empty() {
            buf.enc_u32(0)?;
        } else {
            buf.enc_u32(self.write_data.len() as u32)?;
            buf.enc_slice(self.write_data.as_slice())?;
        }
        Ok(())
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.grp)?;
        buf.dec_u32(&mut self.off)?;
        buf.dec_u32(&mut self.read_len)?;
        let write_len = buf.dec_into_u32()? as usize;
        self.write_data.clear();
        if write_len != 0 {
            self.write_data = buf.dec_into_slice(write_len)?.to_vec();
        }
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadWriteResponse {
    pub result: u32,
    pub read_data: Vec<u8>,
}

impl AdsResponse for AdsReadWriteResponse {
    fn command(&self) -> u16 {
        AdsCommandId::ReadWrite as u16
    }
}

impl AdsCodable for AdsReadWriteResponse {
    fn size(&self) -> usize {
        2 * size_of::<u32>() + self.read_data.len()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.result)?;
        if self.read_data.is_empty() {
            buf.enc_u32(0)?;
        } else {
            buf.enc_u32(self.read_data.len() as u32)?;
            buf.enc_slice(self.read_data.as_slice())?;
        }
        Ok(())
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.result)?;
        if self.result != 0 {
            debug!("AdsReadWriteResponse.decode: {}", AdsError::from(self.result));
            return Err(AdsError::from(self.result));
        }
        let read_len = buf.dec_into_u32()? as usize;
        self.read_data.clear();
        if read_len != 0 {
            self.read_data = buf.dec_into_slice(read_len)?.to_vec();
        }
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadStateRequest {}

impl AdsRequest for AdsReadStateRequest {
    fn command(&self) -> u16 {
        AdsCommandId::ReadState as u16
    }
}

impl AdsCodable for AdsReadStateRequest {
    fn size(&self) -> usize {
        0
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        Ok(())
    }
    fn decode(&mut self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsReadStateResponse {
    pub result: u32,
    pub ads_state: u16,
    pub dev_state: u16,
}

impl AdsResponse for AdsReadStateResponse {
    fn command(&self) -> u16 {
        AdsCommandId::ReadState as u16
    }
}

impl AdsCodable for AdsReadStateResponse {
    fn size(&self) -> usize {
        8
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.result)?;
        buf.enc_u16(self.ads_state)?;
        buf.enc_u16(self.dev_state)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.result)?;
        if self.result != 0 {
            debug!("AdsReadStateResponse.decode: {}", AdsError::from(self.result));
            return Err(AdsError::from(self.result));
        }
        buf.dec_u16(&mut self.ads_state)?;
        buf.dec_u16(&mut self.dev_state)
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsWriteControlRequest {
    pub ads_state: u16,
    pub dev_state: u16,
    pub data: Vec<u8>,
}

impl AdsRequest for AdsWriteControlRequest {
    fn command(&self) -> u16 {
        AdsCommandId::WriteControl as u16
    }
}

impl AdsCodable for AdsWriteControlRequest {
    fn size(&self) -> usize {
        2 * size_of::<u16>() + size_of::<u32>() + self.data.len()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u16(self.ads_state)?;
        buf.enc_u16(self.dev_state)?;
        if self.data.is_empty() {
            buf.enc_u32(0)?;
        } else {
            buf.enc_u32(self.data.len() as u32)?;
            buf.enc_slice(self.data.as_slice())?;
        }
        Ok(())
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u16(&mut self.ads_state)?;
        buf.dec_u16(&mut self.dev_state)?;
        let len = buf.dec_into_u32()? as usize;
        self.data.clear();
        if len != 0 {
            self.data = buf.dec_into_slice(len)?.to_vec();
        }
        Ok(())
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct AdsWriteControlResponse {
    pub result: u32,
}

impl AdsResponse for AdsWriteControlResponse {
    fn command(&self) -> u16 {
        AdsCommandId::WriteControl as u16
    }
}

impl AdsCodable for AdsWriteControlResponse {
    fn size(&self) -> usize {
        size_of::<u32>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.result)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.result)?;
        if self.result != 0 {
            debug!("AdsWriteControlResponse.decode: {}", AdsError::from(self.result));
            return Err(AdsError::from(self.result));
        }
        Ok(())
    }
}
