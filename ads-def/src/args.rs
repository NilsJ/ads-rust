// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)]
#![allow(non_upper_case_globals)]
use std::{collections::HashMap, env::args, net::Ipv4Addr, str::FromStr};

use crate::AdsNetId;

pub struct AdsArgs {
    cmds: Vec<String>,
    opts: Vec<String>,
    args: HashMap<String, Option<String>>,
}

impl AdsArgs {
    pub fn default() -> Self {
        Self::new(args().collect())
    }

    pub fn new(args: Vec<String>) -> Self {
        let mut cmd_vec = Vec::<String>::new();
        let mut opt_vec = Vec::<String>::new();
        let mut arg_map = HashMap::<String, Option<String>>::new();

        const opt_prefix: &str = "-";
        const arg_prefix: &str = "--";

        let mut idx = 0;
        while idx < args.len() {
            let arg = args[idx].to_lowercase();
            if arg.starts_with(&opt_prefix) {
                if arg.starts_with(&arg_prefix) {
                    if let Some(arg) = arg.strip_prefix(&arg_prefix) {
                        if arg.contains('=') {
                            let (arg, val) = arg.split_once('=').unwrap();
                            arg_map.insert(arg.to_string(), Some(val.to_string()));
                        } else {
                            let arg = arg.to_string();
                            let val = args[idx + 1].to_lowercase();
                            if !val.starts_with(&opt_prefix) {
                                arg_map.insert(arg, Some(val));
                                idx += 1;
                            } else {
                                arg_map.insert(arg, None);
                            }
                        }
                    }
                } else if let Some(arg) = arg.strip_prefix(&opt_prefix) {
                    for o in arg.chars() {
                        let os = o.to_string();
                        if o.is_alphanumeric() && !opt_vec.contains(&os) {
                            opt_vec.push(os);
                        }
                    }
                }
            } else {
                cmd_vec.push(arg.to_lowercase());
            }
            idx += 1;
        }

        AdsArgs { cmds: cmd_vec, opts: opt_vec, args: arg_map }
    }

    pub fn take_command(&mut self) -> Option<String> {
        let first = self.cmds.first()?.to_owned();
        self.cmds.remove(0);
        Some(first)
    }

    pub fn has_opt(&self, opt: &str) -> bool {
        self.opts.contains(&opt.to_lowercase())
    }

    pub fn has_arg(&self, arg: &str) -> bool {
        self.args.contains_key(&arg.to_lowercase())
    }

    pub fn get_arg(&self, arg: &str) -> Option<&String> {
        let o = self.args.get(&arg.to_lowercase());
        match o {
            Some(v) => v.as_ref(),
            None => None,
        }
    }

    pub fn get_target_ip(&self) -> Option<Ipv4Addr> {
        let mut arg = self.args.get("target-ip");
        if arg.is_none() {
            arg = self.args.get("target");
        }
        match arg? {
            Some(v) => Ipv4Addr::from_str(v).ok(),
            None => None,
        }
    }

    pub fn get_target_ads(&self) -> Option<AdsNetId> {
        let mut arg = self.args.get("target-ads");
        if arg.is_none() {
            arg = self.args.get("target-ams");
        }
        match arg? {
            Some(v) => AdsNetId::from_str(v).ok(),
            None => None,
        }
    }
}
