// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use crate::addr::AdsAddr;
use crate::error::AdsResult;
use crate::types::{AdsName, AdsState, AdsVersion, DevState};

pub trait AdsClient {
    fn get_local(&self) -> AdsResult<AdsAddr>;
    fn read_info(&mut self, target: AdsAddr) -> AdsResult<(AdsName, AdsVersion)>;
    fn read_state(&mut self, target: AdsAddr) -> AdsResult<(AdsState, DevState)>;
    fn write_control(&mut self, target: AdsAddr, ads_state: AdsState, dev_state: DevState, data: &[u8]) -> AdsResult<()>;
    fn read(&mut self, target: AdsAddr, grp: u32, off: u32, data: &mut [u8]) -> AdsResult<usize>;
    fn write(&mut self, target: AdsAddr, grp: u32, off: u32, data: &[u8]) -> AdsResult<()>;
    fn read_write(&mut self, target: AdsAddr, grp: u32, off: u32, write_data: &[u8], read_data: &mut [u8]) -> AdsResult<usize>;
}
