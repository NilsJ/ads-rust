// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#[macro_use]
mod coding;

mod addr;
mod args;
mod client;
mod client_ext;
mod client_ref;
mod connection;
mod error;
mod types;
//mod connection_ext;

pub mod commands;
pub mod header;

pub use crate::{
    addr::{AdsAddr, AdsNetId, AdsPort},
    args::AdsArgs,
    client::AdsClient,
    client_ext::AdsClientExt,
    client_ref::AdsClientRef,
    coding::{AdsBuffer, AdsCodable},
    connection::AdsConnection,
    error::{AdsError, AdsResult},
    //connection_ext::AdsConnectionExt,
    types::{AdsGuid, AdsName, AdsState, AdsVersion, DevState},
};
