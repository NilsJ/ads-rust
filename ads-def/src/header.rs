// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
#![allow(dead_code)] // TODO
use crate::{
    addr::AdsAddr,
    coding::{AdsBuffer, AdsCodable},
    error::AdsResult,
};
use std::mem::size_of;
use std::sync::atomic::{AtomicU32, Ordering};

#[repr(u16)]
pub enum AdsCommandId {
    Invalid = 0,
    ReadInfo = 1,
    Read = 2,
    Write = 3,
    ReadState = 4,
    WriteControl = 5,
    AddNotify = 6,
    DelNotify = 7,
    Notify = 8,
    ReadWrite = 9,
}

#[repr(u16)]
pub enum AdsStateFlag {
    Request = 0x4, // Command + Request
    Response = 0x5, // Command + Response
                   // Request = 0x0000,
                   // Response = 0x0001,
                   // NoReturn = 0x0002,
                   // Command = 0x0004,
                   // RequestCommand = 0x0004,
                   // ResponseCommand = 0x0005,
                   // SystemCommand = 0x0008,
                   // HighPriority = 0x0010,
                   // Timestamp = 0x0020,
                   // UdpCommand = 0x0040,
                   // InitCommand = 0x0080,
                   // Broadcast = 0x8000,
}

#[derive(Default, Debug)]
pub struct AdsHeader {
    pub target: AdsAddr,
    pub source: AdsAddr,
    pub command: u16,
    pub state: u16,
    pub length: u32,
    pub error: u32,
    pub invoke: u32,
}

impl AdsHeader {
    fn new_invoke() -> u32 {
        static mut invoke: AtomicU32 = AtomicU32::new(1);
        unsafe { invoke.fetch_add(1, Ordering::Relaxed) }
    }

    pub fn new_req(target: AdsAddr, source: AdsAddr, command: u16, length: u32) -> Self {
        AdsHeader {
            target,
            source,
            command,
            state: AdsStateFlag::Request as u16,
            length,
            error: 0,
            invoke: Self::new_invoke(),
        }
    }

    pub fn from_req(req: AdsHeader, length: u32) -> Self {
        AdsHeader {
            target: req.source,
            source: req.target,
            command: req.command,
            state: AdsStateFlag::Response as u16,
            length,
            error: 0,
            invoke: req.invoke,
        }
    }
}

impl AdsCodable for AdsHeader {
    fn size(&self) -> usize {
        32
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_type(&self.target)?;
        buf.enc_type(&self.source)?;
        buf.enc_u16(self.command)?;
        buf.enc_u16(self.state)?;
        buf.enc_u32(self.length)?;
        buf.enc_u32(self.error)?;
        buf.enc_u32(self.invoke)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_type(&mut self.target)?;
        buf.dec_type(&mut self.source)?;
        buf.dec_u16(&mut self.command)?;
        buf.dec_u16(&mut self.state)?;
        buf.dec_u32(&mut self.length)?;
        buf.dec_u32(&mut self.error)?;
        buf.dec_u32(&mut self.invoke)
    }
}

pub struct AdsHeaderRef<'a> {
    data: &'a [u8],
}

impl AdsHeaderRef<'_> {
    pub fn from_slice(data: &[u8]) -> AdsHeaderRef {
        if data.len() < 32 {
            panic!("Invalid length of slice for AdsHeaderRef!")
        }
        AdsHeaderRef { data }
    }

    pub fn target(&self) -> AdsAddr {
        AdsAddr::from_slice(&self.data[0..8])
    }

    pub fn source(&self) -> AdsAddr {
        AdsAddr::from_slice(&self.data[8..16])
    }

    pub fn command(&self) -> u16 {
        dec_slice!(&self.data[16..18], u16)
    }

    pub fn state(&self) -> u16 {
        dec_slice!(&self.data[18..20], u16)
    }

    pub fn length(&self) -> u32 {
        dec_slice!(&self.data[20..24], u32)
    }

    pub fn error(&self) -> u32 {
        dec_slice!(&self.data[24..28], u32)
    }

    pub fn invoke(&self) -> u32 {
        dec_slice!(&self.data[28..32], u32)
    }
}
