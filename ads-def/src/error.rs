// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use log::debug;
use std::error::Error;
use std::fmt::{self, Debug, Display};
use std::result::Result;

#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Copy, Hash)]
pub struct AdsError {
    code: u32,
}

impl Error for AdsError {}

impl Display for AdsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ADS Error: {} ({:#x})", error_into_message(self.code), self.code)
    }
}

impl Debug for AdsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ADS Error: {} ({:#x})", error_into_message(self.code), self.code)
    }
}

impl AdsError {
    pub fn as_code(&self) -> u32 {
        self.code
    }

    pub fn from_io(e: std::io::Error, o: u32) -> Self {
        if let Some(code) = e.raw_os_error() {
            if code >= 0x2000 {
                return AdsError { code: code as u32 };
            }
        }
        AdsError { code: o }
    }
}

impl From<u32> for AdsError {
    fn from(e: u32) -> Self {
        AdsError { code: e }
    }
}

impl From<std::io::Error> for AdsError {
    fn from(e: std::io::Error) -> Self {
        debug!("AdsError.from(IoError): {} (Kind: {})", e, e.kind());
        Self::from_io(e, 0x000C)
    }
}

impl From<std::str::Utf8Error> for AdsError {
    fn from(e: std::str::Utf8Error) -> Self {
        debug!("AdsError.from(Utf8Error): {}", e);
        AdsError { code: 0x0001 }
    }
}

pub type AdsResult<T> = Result<T, AdsError>;

fn error_into_message(e: u32) -> &'static str {
    match e {
        0x0000 => "No error",
        0x0001 => "Internal error",
        0x0002 => "No real-time",
        0x0003 => "Allocation locked memory",
        0x0004 => "Mailbox full",
        0x0005 => "Wrong receive HMSG",
        0x0006 => "Target port not found",
        0x0007 => "Target machine not found",
        0x0008 => "Unknown command ID",
        0x0009 => "Invalid task ID",
        0x000A => "No IO",
        0x000B => "Unknown AMS command",
        0x000C => "Win32 error",
        0x000D => "Port not connected",
        0x000E => "Invalid AMS length",
        0x000F => "Invalid AMS NetID",
        0x0010 => "Low install level",
        0x0011 => "No debugging available",
        0x0012 => "Port disabled",
        0x0013 => "Port already connected",
        0x0014 => "AMS Sync Win32 error",
        0x0015 => "AMS Sync timeout",
        0x0016 => "AMS Sync error",
        0x0017 => "AMS Sync no index map",
        0x0018 => "Invalid AMS port",
        0x0019 => "No memory",
        0x001A => "TCP send error",
        0x001B => "Host unreachable",
        0x001C => "Invalid AMS fragment",
        0x001D => "TLS send error",
        0x001E => "TLS access denied",
        0x0500 => "Router: no locked memory",
        0x0501 => "Router: resize memory",
        0x0502 => "Router: mailbox full",
        0x0503 => "Router: debug mailbox full",
        0x0504 => "Router: unknown port type",
        0x0505 => "Router: not initialized",
        0x0506 => "Router: port already in use",
        0x0507 => "Router: port not registered",
        0x0508 => "Router: no more queues",
        0x0509 => "Router: invalid port",
        0x050A => "Router: not activated",
        0x050B => "Router: fragment mailbox full",
        0x050C => "Router: fragment timeout",
        0x050D => "Router: port removed",
        0x0700 => "Device: error",
        0x0701 => "Device: service not supported",
        0x0702 => "Device: invalid index group",
        0x0703 => "Device: invalid index offset",
        0x0704 => "Device: invalid access",
        0x0705 => "Device: invalid size",
        0x0706 => "Device: invalid data",
        0x0707 => "Device: not ready",
        0x0708 => "Device: busy",
        0x0709 => "Device: invalid context",
        0x070A => "Device: no memory",
        0x070B => "Device: invalid parameter",
        0x070C => "Device: not found",
        0x070D => "Device: syntax error",
        0x070E => "Device: incompatible",
        0x070F => "Device: already exists",
        0x0710 => "Device: symbol not found",
        0x0711 => "Device: symbol version invalid",
        0x0712 => "Device: invalid state",
        0x0713 => "Device: trans mode not supported",
        0x0714 => "Device: notification handle is invalid",
        0x0715 => "Device: notification client not registered",
        0x0716 => "Device: no more handles",
        0x0717 => "Device: notification size too large",
        0x0718 => "Device: not initialized",
        0x0719 => "Device: timeout",
        0x071A => "Device: query interface failed",
        0x071B => "Device: wrong interface requested",
        0x071C => "Device: ClassID invalid",
        0x071D => "Device: ObjectID invalid",
        0x071E => "Device: pending",
        0x071F => "Device: aborted",
        0x0720 => "Device: warning",
        0x0721 => "Device: invalid array index",
        0x0722 => "Device: symbol not active",
        0x0723 => "Device: access denied",
        0x0724 => "Device: license missing",
        0x0725 => "Device: license expired",
        0x0726 => "Device: license exceeded",
        0x0727 => "Device: license invalid",
        0x0728 => "Device: invalid SystemID",
        0x0729 => "Device: license not limited in time",
        0x072A => "Device: license time in the future",
        0x072B => "Device: license time period too long",
        0x072C => "Device: exception at system startup",
        0x072D => "Device: license file read twice",
        0x072E => "Device: license invalid signature",
        0x072F => "Device: license invalid certificate",
        0x0730 => "Device: license invalid OEM",
        0x0731 => "Device: license invalid SystemID",
        0x0732 => "Device: license demo prohibited",
        0x0733 => "Device: invalid FunctionID",
        0x0734 => "Device: out of range",
        0x0735 => "Device: invalid alignment",
        0x0736 => "Device: lincense invalid platform level",
        0x0737 => "Device: Context forward to passive level",
        0x0738 => "Device: Content forward to dispatch level",
        0x0739 => "Device: Context forward to real-time",
        0x0740 => "Client: error",
        0x0741 => "Client: invalid parameter",
        0x0742 => "Client: polling list is empty",
        0x0743 => "Client: connection already in use",
        0x0744 => "Client: InvokeID already in use",
        0x0745 => "Client: timeout elapsed",
        0x0746 => "Client: Win32 error",
        0x0747 => "Client: invalid client timeout",
        0x0748 => "Client: port not opened",
        0x0749 => "Client: no AMS address",
        0x0750 => "Client: internal error in ADS sync",
        0x0751 => "Client: hash table overflow",
        0x0752 => "Client: hash table key not found",
        0x0753 => "Client: no more symbols in cache",
        0x0754 => "Client: invalid response received",
        0x0755 => "Client: sync port is locked",
        0x1000 => "Runtime: internal error",
        0x1001 => "Runtime: timer value not valid",
        0x1002 => "Runtime: task pointer invalid",
        0x1003 => "Runtime: stack pointer invalid",
        0x1004 => "Runtime: task priority already assigned",
        0x1005 => "Runtime: no free TCB",
        0x1006 => "Runtime: no free semaphores",
        0x1007 => "Runtime: no free queues",
        0x100D => "Runtime: external sync interrupt already applied",
        0x100E => "Runtime: no external sync interrupt applied",
        0x100F => "Runtime: external sync interrupt application failed",
        0x1010 => "Runtime: call in wrong context",
        0x1017 => "Runtime: Intel VT-x not supported",
        0x1018 => "Runtime: Intel VT-x not enabled in BIOS",
        0x1019 => "Runtime: missing function in Intel VT-x",
        0x101A => "Runtime: activation of Intel VT-x failed",
        0x274C => "Socket: host unreachable",
        0x274D => "Socket: connection timeout",
        0x2751 => "Socket: connection refused",
        _ => "Unknown",
    }
}
