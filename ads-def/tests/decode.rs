// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
// https://docs.rs/binary-reader/0.3.0/src/binary_reader/lib.rs.html
use ads_def::*;
use byteorder::{WriteBytesExt, LE};

#[test]
fn decode_u16() {
    let mut data = Vec::new();
    data.write_u16::<LE>(1234).unwrap();
    data.write_u16::<LE>(9876).unwrap();

    let mut buf = AdsBuffer::from_vec(data);
    assert_eq!(1234, buf.dec_into_u16().unwrap());
    assert_eq!(9876, buf.dec_into_u16().unwrap());
}

#[test]
fn decode_u32() {
    let mut data = Vec::new();
    data.write_u32::<LE>(1234).unwrap();
    data.write_u32::<LE>(9876).unwrap();

    let mut buf = AdsBuffer::from_vec(data);
    assert_eq!(1234, buf.dec_into_u32().unwrap());
    assert_eq!(9876, buf.dec_into_u32().unwrap());
}

#[test]
fn decode_f32() {
    let mut data = Vec::new();
    data.write_f32::<LE>(654.321).unwrap();
    data.write_f32::<LE>(456.789).unwrap();

    let mut buf = AdsBuffer::from_vec(data);
    assert!(654.321 == buf.dec_into_f32().unwrap(), "f32#1");
    assert!(456.789 == buf.dec_into_f32().unwrap(), "f32#2");
}

#[test]
fn decode_bool() {
    let mut data = Vec::new();
    data.write_u8(true as u8).unwrap();
    data.write_u8(false as u8).unwrap();
    data.write_u8(true as u8).unwrap();
    data.write_u8(true as u8).unwrap();

    let mut buf = AdsBuffer::from_vec(data);
    assert!(buf.dec_into_bool().unwrap());
    assert!(!buf.dec_into_bool().unwrap());
    assert!(buf.dec_into_bool().unwrap());
    assert!(buf.dec_into_bool().unwrap());
}

#[test]
fn decode_char() {
    let mut data = Vec::new();
    data.write_u8(b'W').unwrap();
    data.write_u8(b'e').unwrap();
    data.write_u8(b'l').unwrap();
    data.write_u8(b't').unwrap();

    let mut buf = AdsBuffer::from_vec(data);
    assert_eq!('W', buf.dec_into_char().unwrap());
    assert_eq!('e', buf.dec_into_char().unwrap());
    assert_eq!('l', buf.dec_into_char().unwrap());
    assert_eq!('t', buf.dec_into_char().unwrap());
}

#[test]
fn decode_mix() {
    let mut data = Vec::new();
    data.write_i8(1).unwrap();
    data.write_i16::<LE>(12).unwrap();
    data.write_i32::<LE>(123).unwrap();
    data.write_i64::<LE>(1234).unwrap();
    data.write_u8(9).unwrap();
    data.write_u16::<LE>(98).unwrap();
    data.write_u32::<LE>(987).unwrap();
    data.write_u64::<LE>(9876).unwrap();
    data.write_f32::<LE>(12.34).unwrap();
    data.write_f64::<LE>(123.456).unwrap();

    let mut buf = AdsBuffer::from_vec(data);
    assert_eq!(1, buf.dec_into_i8().unwrap());
    assert_eq!(12, buf.dec_into_i16().unwrap());
    assert_eq!(123, buf.dec_into_i32().unwrap());
    assert_eq!(1234, buf.dec_into_i64().unwrap());
    assert_eq!(9, buf.dec_into_u8().unwrap());
    assert_eq!(98, buf.dec_into_u16().unwrap());
    assert_eq!(987, buf.dec_into_u32().unwrap());
    assert_eq!(9876, buf.dec_into_u64().unwrap());
    assert!(12.34f32 == buf.dec_into_f32().unwrap(), "f32");
    assert!(123.456f64 == buf.dec_into_f64().unwrap(), "f64");
}
