// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_def::{AdsAddr, AdsNetId};
use std::str::FromStr;

#[test]
fn net_id_new() {
    let id = AdsNetId::new(1, 2, 3, 4, 5, 6);
    assert_eq!([1, 2, 3, 4, 5, 6], id.bytes());
}

#[test]
fn net_id_default() {
    let id = AdsNetId::default();
    assert_eq!([0, 0, 0, 0, 1, 1], id.bytes());
}

#[test]
fn net_id_byte() {
    let id = AdsNetId::new(1, 2, 3, 4, 5, 6);
    assert_eq!(1, id.byte(0));
    assert_eq!(2, id.byte(1));
    assert_eq!(3, id.byte(2));
    assert_eq!(4, id.byte(3));
    assert_eq!(5, id.byte(4));
    assert_eq!(6, id.byte(5));
}

#[test]
fn net_id_is_empty() {
    assert!(AdsNetId::new(0, 0, 0, 0, 0, 0).is_empty());
    assert!(AdsNetId::new(0, 0, 0, 0, 1, 1).is_empty());
    assert!(!AdsNetId::new(1, 2, 3, 4, 5, 6).is_empty());
}

#[test]
fn net_id_is_loopback() {
    assert!(AdsNetId::new(127, 0, 0, 1, 0, 0).is_loopback());
    assert!(AdsNetId::new(127, 0, 0, 1, 1, 1).is_loopback());
    assert!(!AdsNetId::new(1, 2, 3, 4, 5, 6).is_loopback());
}

#[test]
fn net_id_from_str() {
    assert_eq!(AdsNetId::new(1, 2, 3, 4, 5, 6), AdsNetId::from_str("1.2.3.4.5.6").unwrap());
    assert_eq!(AdsNetId::new(1, 2, 3, 4, 5, 6), "1.2.3.4.5.6".parse().unwrap());
    assert_eq!(AdsNetId::new(1, 2, 3, 4, 5, 6), "1.2.3.4.5.6".to_string().parse().unwrap());
}

#[test]
fn net_id_bytes() {
    assert_eq!([1, 2, 3, 4, 5, 6], AdsNetId::new(1, 2, 3, 4, 5, 6).bytes());
}

#[test]
fn addr_from_str() {
    assert_eq!(AdsAddr::new(AdsNetId::new(6, 5, 4, 3, 2, 1), 851), AdsAddr::from_str("6.5.4.3.2.1:851").unwrap());
    assert_eq!(AdsAddr::new(AdsNetId::new(6, 5, 4, 3, 2, 1), 10000), AdsAddr::from_str("6.5.4.3.2.1").unwrap());
    assert_eq!(AdsAddr::new(AdsNetId::default(), 851), AdsAddr::from_str("851").unwrap());

    assert!(AdsAddr::from_str("").is_err());
    assert!(AdsAddr::from_str(":").is_err());
    assert!(AdsAddr::from_str(":654").is_err());
    assert!(AdsAddr::from_str("99999").is_err());
    assert!(AdsAddr::from_str(".").is_err());
    assert!(AdsAddr::from_str("5.4.3.2.1:0").is_err());
    assert!(AdsAddr::from_str("7.6.5.4.3.2.1:851").is_err());
    assert!(AdsAddr::from_str("6.5.4.3.2.1:99999").is_err());
}
