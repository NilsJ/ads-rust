// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_def::{AdsArgs, AdsNetId};
use std::net::Ipv4Addr;

macro_rules! vec_of_strings {
    ($($x:expr),*) => (vec![$($x.to_string()),*]);
}

#[test]
fn args_basic() {
    let args = vec_of_strings!("test", "-o", "-pt", "--key1", "value1", "--key2", "value2");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert!(args.has_opt("o"));
    assert!(args.has_opt("p"));
    assert!(args.has_opt("t"));
    assert!(args.has_arg("key1"));
    assert!(args.has_arg("key2"));
    assert_eq!("value1", args.get_arg("key1").unwrap());
    assert_eq!("value2", args.get_arg("key2").unwrap());
}

#[test]
fn args_command_multi() {
    let args = vec_of_strings!("test", "-o", "one", "--key1", "value1", "two", "-pt", "--key2", "value2");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert_eq!("one", args.take_command().unwrap());
    assert_eq!("two", args.take_command().unwrap());
    assert!(args.has_opt("o"));
    assert!(args.has_opt("p"));
    assert!(args.has_opt("t"));
    assert!(args.has_arg("key1"));
    assert!(args.has_arg("key2"));
    assert_eq!("value1", args.get_arg("key1").unwrap());
    assert_eq!("value2", args.get_arg("key2").unwrap());
}

#[test]
fn args_argument_novalue() {
    let args = vec_of_strings!("test", "-bla", "--keyA", "--key1", "value1", "--keyB", "--key2", "value2");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert!(args.has_opt("b"));
    assert!(args.has_opt("l"));
    assert!(args.has_opt("a"));
    assert!(args.has_arg("key1"));
    assert!(args.has_arg("key2"));
    assert_eq!("value1", args.get_arg("key1").unwrap());
    assert_eq!("value2", args.get_arg("key2").unwrap());
    assert!(args.has_arg("keyA"));
    assert!(args.has_arg("keyB"));
    dbg!(args.get_arg("keyA"));
    assert!(args.get_arg("keyA").is_none());
    assert!(args.get_arg("keyB").is_none());
}

#[test]
fn args_argument_equal() {
    let args = vec_of_strings!("test", "-op", "-t", "--key1=value1", "--key2=value2");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert!(args.has_opt("o"));
    assert!(args.has_opt("p"));
    assert!(args.has_opt("t"));
    assert!(args.has_arg("key1"));
    assert!(args.has_arg("key2"));
    assert_eq!("value1", args.get_arg("key1").unwrap());
    assert_eq!("value2", args.get_arg("key2").unwrap());
}

#[test]
fn args_argument_minus() {
    let args = vec_of_strings!("test", "-o", "-pt", "--key-1", "value-1", "--key-2", "value-2");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert!(args.has_opt("o"));
    assert!(args.has_opt("p"));
    assert!(args.has_opt("t"));
    assert!(args.has_arg("key-1"));
    assert!(args.has_arg("key-2"));
    assert_eq!("value-1", args.get_arg("key-1").unwrap());
    assert_eq!("value-2", args.get_arg("key-2").unwrap());
}

#[test]
fn args_argument_mixed() {
    let args = vec_of_strings!("test", "-op", "-t", "--key1=value1", "--key2=value2");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert!(args.has_opt("o"));
    assert!(args.has_opt("p"));
    assert!(args.has_opt("t"));
    assert!(args.has_arg("key1"));
    assert!(args.has_arg("key2"));
    assert_eq!("value1", args.get_arg("key1").unwrap());
    assert_eq!("value2", args.get_arg("key2").unwrap());
}

#[test]
fn args_option_multi() {
    let args = vec_of_strings!("test", "-o", "-op", "-t", "-opt", "-o", "-p", "--key", "value");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert!(args.has_opt("o"));
    assert!(args.has_opt("p"));
    assert!(args.has_opt("t"));
    assert!(args.has_arg("key"));
    assert_eq!("value", args.get_arg("key").unwrap());
}

#[test]
fn args_ads_target() {
    let args = vec_of_strings!("test", "--target-ip=192.168.178.38", "--target-ams=192.168.178.38.1.1");
    let mut args = AdsArgs::new(args);
    assert_eq!("test", args.take_command().unwrap());
    assert_eq!(Ipv4Addr::new(192, 168, 178, 38), args.get_target_ip().unwrap());
    assert_eq!(AdsNetId::new(192, 168, 178, 38, 1, 1), args.get_target_ads().unwrap());
}
