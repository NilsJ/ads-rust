// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_def::*;
use byteorder::{ReadBytesExt, LE};

#[test]
fn encode_u16() {
    let mut buf = AdsBuffer::with_capacity(4);
    buf.enc_u16(1234).unwrap();
    buf.enc_u16(9876).unwrap();

    assert_eq!(4, buf.len());

    let data = buf.to_vec();
    assert_eq!(1234, data.get(0..2).unwrap().read_i16::<LE>().unwrap());
    assert_eq!(9876, data.get(2..4).unwrap().read_i16::<LE>().unwrap());
}

#[test]
fn encode_u32() {
    let mut buf = AdsBuffer::with_capacity(4);
    buf.enc_u32(1234).unwrap();
    buf.enc_u32(9876).unwrap();

    assert_eq!(8, buf.len());

    let data = buf.to_vec();
    assert_eq!(1234, data.get(0..4).unwrap().read_i32::<LE>().unwrap());
    assert_eq!(9876, data.get(4..8).unwrap().read_i32::<LE>().unwrap());
}

#[test]
fn encode_f32() {
    let mut buf = AdsBuffer::with_capacity(4);
    buf.enc_f32(654.321).unwrap();
    buf.enc_f32(456.789).unwrap();

    assert_eq!(8, buf.len());

    let data = buf.to_vec();
    assert!(654.321 == data.get(0..4).unwrap().read_f32::<LE>().unwrap(), "f32#1");
    assert!(456.789 == data.get(4..8).unwrap().read_f32::<LE>().unwrap(), "f32#2");
}

#[test]
fn encode_bool() {
    let mut buf = AdsBuffer::with_capacity(4);
    buf.enc_bool(true).unwrap();
    buf.enc_bool(false).unwrap();
    buf.enc_bool(true).unwrap();
    buf.enc_bool(true).unwrap();

    assert_eq!(4, buf.len());

    let data = buf.to_vec();
    assert_eq!(1, data.get(0..1).unwrap().read_u8().unwrap());
    assert_eq!(0, data.get(1..2).unwrap().read_u8().unwrap());
    assert_eq!(1, data.get(2..3).unwrap().read_u8().unwrap());
    assert_eq!(1, data.get(3..4).unwrap().read_u8().unwrap());
}

#[test]
fn encode_char() {
    let mut buf = AdsBuffer::with_capacity(4);
    buf.enc_char('W').unwrap();
    buf.enc_char('e').unwrap();
    buf.enc_char('l').unwrap();
    buf.enc_char('t').unwrap();

    assert_eq!(4, buf.len());

    let data = buf.to_vec();
    assert_eq!('W', data.get(0..1).unwrap().read_u8().unwrap() as char);
    assert_eq!('e', data.get(1..2).unwrap().read_u8().unwrap() as char);
    assert_eq!('l', data.get(2..3).unwrap().read_u8().unwrap() as char);
    assert_eq!('t', data.get(3..4).unwrap().read_u8().unwrap() as char);
}

#[test]
fn encode_mix() {
    let mut buf = AdsBuffer::with_capacity(38);
    buf.enc_i8(1).unwrap();
    buf.enc_i16(12).unwrap();
    buf.enc_i32(123).unwrap();
    buf.enc_i64(1234).unwrap();
    buf.enc_u8(9).unwrap();
    buf.enc_u16(98).unwrap();
    buf.enc_u32(987).unwrap();
    buf.enc_u64(9876).unwrap();
    buf.enc_f32(12.34).unwrap();
    buf.enc_f64(123.456).unwrap();

    assert_eq!(42, buf.len());

    let data = buf.to_vec();
    assert_eq!(1, data.get(0..1).unwrap().read_i8().unwrap());
    assert_eq!(12, data.get(1..3).unwrap().read_i16::<LE>().unwrap());
    assert_eq!(123, data.get(3..7).unwrap().read_i32::<LE>().unwrap());
    assert_eq!(1234, data.get(7..15).unwrap().read_i64::<LE>().unwrap());
    assert_eq!(9, data.get(15..16).unwrap().read_u8().unwrap());
    assert_eq!(98, data.get(16..18).unwrap().read_u16::<LE>().unwrap());
    assert_eq!(987, data.get(18..22).unwrap().read_u32::<LE>().unwrap());
    assert_eq!(9876, data.get(22..30).unwrap().read_u64::<LE>().unwrap());
    assert!(12.34f32 == data.get(30..34).unwrap().read_f32::<LE>().unwrap(), "f32");
    assert!(123.456f64 == data.get(34..42).unwrap().read_f64::<LE>().unwrap(), "f64");
}
