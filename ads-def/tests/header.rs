// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
use ads_def::header::*;
use ads_def::*;
use std::str::FromStr;

const ReadRequestDump: [u8; 44] = [
    0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x2c, 0x01, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x47, 0x80, 0x02, 0x00, 0x04, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x65, 0x00, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
];

#[test]
fn read_request_decode() {
    let hdr = AdsHeaderRef::from_slice(&ReadRequestDump);
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:300").unwrap(), hdr.target());
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(), hdr.source());
    assert_eq!(AdsCommandId::Read as u16, hdr.command());
    assert_eq!(AdsStateFlag::Request as u16, hdr.state());
    assert_eq!(12, hdr.length());
    assert_eq!(0, hdr.error());
    assert_eq!(101, hdr.invoke());
}
