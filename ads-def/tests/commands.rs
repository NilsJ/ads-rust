// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
use ads_def::commands::*;
use ads_def::header::*;
use ads_def::*;
use byteorder::{WriteBytesExt, LE};
use std::str::FromStr;

const ReadRequestDump: [u8; 44] = [
    0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x2c, 0x01, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x47, 0x80, 0x02, 0x00, 0x04, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x65, 0x00, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
];

#[test]
fn read_request_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("5.58.179.244.1.1:300").unwrap(),
        source: AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(),
        command: AdsCommandId::Read as u16,
        state: AdsStateFlag::Request as u16,
        length: 12,
        error: 0,
        invoke: 101,
    };

    let cmd = AdsReadRequest { grp: 20480, off: 39, len: 4 };
    assert_eq!(12, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(44);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(ReadRequestDump[0..32].to_vec(), *buf.as_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(44, buf.len());
    assert_eq!(ReadRequestDump.to_vec(), *buf.as_vec());
}

#[test]
fn read_request_decode() {
    let mut buf = AdsBuffer::from_slice(&ReadRequestDump);
    assert_eq!(44, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:300").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::Read as u16, hdr.command);
    assert_eq!(AdsStateFlag::Request as u16, hdr.state);
    assert_eq!(12, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(101, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsReadRequest>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(20480, cmd.grp);
    assert_eq!(39, cmd.off);
    assert_eq!(4, cmd.len);
}

const ReadResponseDump: [u8; 44] = [
    0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x47, 0x80, 0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x2c, 0x01, 0x02, 0x00, 0x05, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x65, 0x00, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
];

#[test]
fn read_response_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(),
        source: AdsAddr::from_str("5.58.179.244.1.1:300").unwrap(),
        command: AdsCommandId::Read as u16,
        state: AdsStateFlag::Response as u16,
        length: 12,
        error: 6,
        invoke: 101,
    };

    // let mut cmd = AdsReadResponse {
    //     result: 0,
    //     data: Vec::new(),
    // };
    // cmd.data.write_u64::<LE>(0).unwrap();
    // assert_eq!(16, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(44);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(ReadResponseDump[0..32].to_vec(), *buf.as_vec());
    // buf.enc_type(&cmd).unwrap();
    // assert_eq!(44, buf.len());
    // assert_eq!(ReadResponseDump[0..24].to_vec(), *buf.as_vec());
}

#[test]
fn read_response_decode() {
    let mut buf = AdsBuffer::from_slice(&ReadResponseDump);
    assert_eq!(44, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:300").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::Read as u16, hdr.command);
    assert_eq!(AdsStateFlag::Response as u16, hdr.state);
    assert_eq!(12, hdr.length);
    assert_eq!(6, hdr.error);
    assert_eq!(101, hdr.invoke);

    // let cmd = buf.dec_into_type::<AdsReadResponse>().unwrap();
    // assert_eq!(hdr.length, cmd.size() as u32);
    // assert_eq!(0, cmd.result);
    // assert!(!cmd.data.is_empty());
    // TODO check data content
}

const WriteRequestDump: [u8; 76] = [
    0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x5e, 0x01, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x74, 0x80, 0x03, 0x00, 0x04, 0x00, 0x2c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x04, 0x00, 0x00, 0x00, 0x20, 0x50, 0x08, 0xf8, 0xdf, 0x05, 0x81, 0x20, 0x00, 0x00, 0x00, 0x48, 0x61, 0x6c, 0x6c, 0x6f,
    0x20, 0x57, 0x65, 0x6c, 0x74, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
];

#[test]
fn write_request_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("5.58.179.244.1.1:350").unwrap(),
        source: AdsAddr::from_str("15.12.19.86.1.1:32884").unwrap(),
        command: AdsCommandId::Write as u16,
        state: AdsStateFlag::Request as u16,
        length: 44,
        error: 0,
        invoke: 1152,
    };

    let mut cmd = AdsWriteRequest { grp: 139468800, off: 2164645880, data: Vec::new() };
    let mut buf = AdsBuffer::from_vec(cmd.data);
    buf.enc_utf8_len("Hallo Welt!", 32).unwrap();
    cmd.data = buf.to_vec();
    assert_eq!(44, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(76);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(WriteRequestDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(76, buf.len());
    assert_eq!(WriteRequestDump.to_vec(), *buf.to_vec());
}

#[test]
fn write_request_decode() {
    let mut buf = AdsBuffer::from_slice(&WriteRequestDump);
    assert_eq!(76, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:350").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32884").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::Write as u16, hdr.command);
    assert_eq!(AdsStateFlag::Request as u16, hdr.state);
    assert_eq!(44, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(1152, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsWriteRequest>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(139468800, cmd.grp);
    assert_eq!(2164645880, cmd.off);
    assert!(!cmd.data.is_empty());
    assert_eq!(32, cmd.data.len());
    // TODO check data content
}

const WriteResponseDump: [u8; 36] = [
    0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x74, 0x80, 0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x5e, 0x01, 0x03, 0x00, 0x05, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
];

#[test]
fn write_response_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("15.12.19.86.1.1:32884").unwrap(),
        source: AdsAddr::from_str("5.58.179.244.1.1:350").unwrap(),
        command: AdsCommandId::Write as u16,
        state: AdsStateFlag::Response as u16,
        length: 4,
        error: 0,
        invoke: 1152,
    };

    let cmd = AdsWriteResponse { result: 0 };
    assert_eq!(4, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(40);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(WriteResponseDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(36, buf.len());
    assert_eq!(WriteResponseDump.to_vec(), *buf.to_vec());
}

#[test]
fn write_response_decode() {
    let mut buf = AdsBuffer::from_slice(&WriteResponseDump);
    assert_eq!(36, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32884").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:350").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::Write as u16, hdr.command);
    assert_eq!(AdsStateFlag::Response as u16, hdr.state);
    assert_eq!(4, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(1152, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsWriteResponse>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(0, cmd.result);
}

const ReadWriteRequestDump: [u8; 56] = [
    0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x64, 0x00, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x48, 0x80, 0x09, 0x00, 0x04, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x90, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x0f,
    0x0c, 0x13, 0x56, 0x01, 0x01, 0x48, 0x80,
];

#[test]
fn read_write_request_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("5.58.179.244.1.1:100").unwrap(),
        source: AdsAddr::from_str("15.12.19.86.1.1:32840").unwrap(),
        command: AdsCommandId::ReadWrite as u16,
        state: AdsStateFlag::Request as u16,
        length: 24,
        error: 0,
        invoke: 1,
    };

    let mut cmd = AdsReadWriteRequest {
        grp: 61584,
        off: 0,
        read_len: 4,
        write_data: Vec::new(),
    };
    // TODO add data content
    cmd.write_data.write_u64::<LE>(0).unwrap();
    assert_eq!(24, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(40);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(ReadWriteRequestDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(56, buf.len());
    // assert_eq!(ReadWriteRequestDump.to_vec(), *buf.to_vec());
}

#[test]
fn read_write_request_decode() {
    let mut buf = AdsBuffer::from_slice(&ReadWriteRequestDump);
    assert_eq!(56, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:100").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32840").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::ReadWrite as u16, hdr.command);
    assert_eq!(AdsStateFlag::Request as u16, hdr.state);
    assert_eq!(24, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(1, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsReadWriteRequest>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(61584, cmd.grp);
    assert_eq!(0, cmd.off);
    assert_eq!(4, cmd.read_len);
    assert!(!cmd.write_data.is_empty());
    assert_eq!(8, cmd.write_data.len());
    // TODO check data content
}

const ReadWriteResponseDump: [u8; 44] = [
    0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x48, 0x80, 0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x64, 0x00, 0x09, 0x00, 0x05, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
];

#[test]
fn read_write_response_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("15.12.19.86.1.1:32840").unwrap(),
        source: AdsAddr::from_str("5.58.179.244.1.1:100").unwrap(),
        command: AdsCommandId::ReadWrite as u16,
        state: AdsStateFlag::Response as u16,
        length: 12,
        error: 0,
        invoke: 1,
    };

    let mut cmd = AdsReadWriteResponse { result: 0, read_data: Vec::new() };
    cmd.read_data.write_u32::<LE>(1).unwrap();
    assert_eq!(12, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(40);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(ReadWriteResponseDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(44, buf.len());
    assert_eq!(ReadWriteResponseDump.to_vec(), *buf.to_vec());
}

#[test]
fn read_write_response_decode() {
    let mut buf = AdsBuffer::from_slice(&ReadWriteResponseDump);
    assert_eq!(44, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32840").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:100").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::ReadWrite as u16, hdr.command);
    assert_eq!(AdsStateFlag::Response as u16, hdr.state);
    assert_eq!(12, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(1, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsReadWriteResponse>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(0, cmd.result);
    assert!(!cmd.read_data.is_empty());
    assert_eq!(4, cmd.read_data.len());
    // TODO check data content
}

const ReadStateRequestDump: [u8; 32] = [
    0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x84, 0x00, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x4c, 0x80, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
];

#[test]
fn read_state_request_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("5.58.179.244.1.1:132").unwrap(),
        source: AdsAddr::from_str("15.12.19.86.1.1:32844").unwrap(),
        command: AdsCommandId::ReadState as u16,
        state: AdsStateFlag::Request as u16,
        length: 0,
        error: 0,
        invoke: 1,
    };

    let cmd = AdsReadStateRequest::default();
    assert_eq!(0, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(32);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(ReadStateRequestDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(32, buf.len());
    assert_eq!(ReadStateRequestDump.to_vec(), *buf.to_vec());
}

#[test]
fn read_state_request_decode() {
    let mut buf = AdsBuffer::from_slice(&ReadStateRequestDump);
    assert_eq!(32, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:132").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32844").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::ReadState as u16, hdr.command);
    assert_eq!(AdsStateFlag::Request as u16, hdr.state);
    assert_eq!(0, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(1, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsReadStateRequest>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
}

const ReadStateResponseDump: [u8; 40] = [
    0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x4c, 0x80, 0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x84, 0x00, 0x04, 0x00, 0x05, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
];

#[test]
fn read_state_response_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("15.12.19.86.1.1:32844").unwrap(),
        source: AdsAddr::from_str("5.58.179.244.1.1:132").unwrap(),
        command: AdsCommandId::ReadState as u16,
        state: AdsStateFlag::Response as u16,
        length: 8,
        error: 0,
        invoke: 1,
    };

    let cmd = AdsReadStateResponse { result: 0, ads_state: 0, dev_state: 0 };
    assert_eq!(8, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(40);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(ReadStateResponseDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(40, buf.len());
    assert_eq!(ReadStateResponseDump.to_vec(), *buf.to_vec());
}

#[test]
fn read_state_response_decode() {
    let mut buf = AdsBuffer::from_slice(&ReadStateResponseDump);
    assert_eq!(40, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32844").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:132").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::ReadState as u16, hdr.command);
    assert_eq!(AdsStateFlag::Response as u16, hdr.state);
    assert_eq!(8, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(1, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsReadStateResponse>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(0, cmd.result);
    assert_eq!(0, cmd.ads_state);
    assert_eq!(0, cmd.dev_state);
}

const WriteControlRequestDump: [u8; 40] = [
    0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x10, 0x27, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x47, 0x80, 0x05, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
];

#[test]
fn write_control_request_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("5.58.179.244.1.1:10000").unwrap(),
        source: AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(),
        command: AdsCommandId::WriteControl as u16,
        state: AdsStateFlag::Request as u16,
        length: 8,
        error: 0,
        invoke: 100,
    };

    let cmd = AdsWriteControlRequest { ads_state: 16, dev_state: 0, data: Vec::new() };
    assert_eq!(8, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(40);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(WriteControlRequestDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(40, buf.len());
    assert_eq!(WriteControlRequestDump.to_vec(), *buf.to_vec());
}

#[test]
fn write_control_request_decode() {
    let mut buf = AdsBuffer::from_slice(&WriteControlRequestDump);
    assert_eq!(40, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:10000").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::WriteControl as u16, hdr.command);
    assert_eq!(AdsStateFlag::Request as u16, hdr.state);
    assert_eq!(8, hdr.length);
    assert_eq!(0, hdr.error);
    assert_eq!(100, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsWriteControlRequest>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(16, cmd.ads_state);
    assert_eq!(0, cmd.dev_state);
    assert!(cmd.data.is_empty());
    assert_eq!(0, cmd.data.len());
}

const WriteControlResponseDump: [u8; 36] = [
    0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x47, 0x80, 0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x10, 0x27, 0x05, 0x00, 0x05, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
];

#[test]
fn write_control_response_encode() {
    let hdr = AdsHeader {
        target: AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(),
        source: AdsAddr::from_str("5.58.179.244.1.1:10000").unwrap(),
        command: AdsCommandId::WriteControl as u16,
        state: AdsStateFlag::Response as u16,
        length: 4,
        error: 0,
        invoke: 100,
    };

    let cmd = AdsWriteControlResponse { result: 0 };
    assert_eq!(4, cmd.size() as u32);

    let mut buf = AdsBuffer::with_capacity(40);
    buf.enc_type(&hdr).unwrap();
    assert_eq!(WriteControlResponseDump[0..32].to_vec(), *buf.to_vec());
    buf.enc_type(&cmd).unwrap();
    assert_eq!(36, buf.len());
    assert_eq!(WriteControlResponseDump.to_vec(), *buf.to_vec());
}

#[test]
fn write_control_response_decode() {
    let mut buf = AdsBuffer::from_slice(&WriteControlResponseDump);
    assert_eq!(36, buf.len());

    let hdr = buf.dec_into_type::<AdsHeader>().unwrap();
    assert_eq!(AdsAddr::from_str("15.12.19.86.1.1:32839").unwrap(), hdr.target);
    assert_eq!(AdsAddr::from_str("5.58.179.244.1.1:10000").unwrap(), hdr.source);
    assert_eq!(AdsCommandId::WriteControl as u16, hdr.command);
    assert_eq!(AdsStateFlag::Response as u16, hdr.state);
    assert_eq!(4, hdr.length); // AdsWriteControlRes::size()
    assert_eq!(0, hdr.error);
    assert_eq!(100, hdr.invoke);

    let cmd = buf.dec_into_type::<AdsWriteControlResponse>().unwrap();
    assert_eq!(hdr.length, cmd.size() as u32);
    assert_eq!(0, cmd.result);
}
