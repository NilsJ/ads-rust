// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_tcp::{AdsAddr, AdsClient, AdsConnection, AdsTcpClient};
use std::{env::args, error::Error, net::Ipv4Addr, str::FromStr};
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let args: Vec<String> = args().collect();
    if args.len() <= 1 {
        panic!("Target IP address not defined!");
    }

    let addr = AdsAddr::default();
    let ip_addr = Ipv4Addr::from_str(&args[1])?;
    let mut client = AdsTcpClient::new(ip_addr)?;

    println!("# Client");
    println!("Local: {} ({})", client.get_local()?, client.local_ip()?);
    println!("Target: {} ({})", addr, ip_addr);

    println!("# State");
    let (ads_state, dev_state) = client.read_state(addr)?;
    println!("AdsState: {}", ads_state);
    println!("DevState: {}", dev_state);

    println!("# Info");
    let (version, name) = client.read_info(addr)?;
    println!("Name: {}", name);
    println!("Version: {}", version);

    client.disconnect();
    Ok(())
}
