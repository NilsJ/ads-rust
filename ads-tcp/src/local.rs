// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use super::{AdsTcpConnection, AdsTcpHeader};
use ads_def::*;
use log::debug;
use std::mem::size_of;

#[allow(non_snake_case)]
mod AdsTcpFlags {
    //pub const Command: u16 = 0x0000; // AMS_TCP_PORT_AMS_CMD
    pub const PortClose: u16 = 0x0000; // AMS_TCP_PORT_CLOSE
    pub const PortOpen: u16 = 0x1000; // AMS_TCP_PORT_CONNECT
    pub const RouterNote: u16 = 0x1001; // AMS_TCP_PORT_ROUTER_NOTE
    pub const GetLocalNetId: u16 = 0x1002; // AMS_TCP_PORT_GET_NET_ID
    pub const GetRuntimeType: u16 = 0x1003; // AMS_TCP_PORT_GET_RUNTIME_TYPE
}

pub struct AdsLocalClient;

impl AdsLocalClient {
    pub(crate) fn open_port(con: &mut AdsTcpConnection, port: u16) -> AdsResult<AdsAddr> {
        let req_cmd = AdsTcpPortOpenRequest { port };
        let req_hdr = AdsTcpHeader {
            flags: AdsTcpFlags::PortOpen,
            length: req_cmd.size() as u32,
        };
        let mut buf = AdsBuffer::with_capacity(req_hdr.size() + req_cmd.size());
        buf.enc_type(&req_hdr)?;
        buf.enc_type(&req_cmd)?;
        con.send_dat(buf.as_slice())?;

        let mut res_hdr = AdsTcpHeader::default();
        buf.resize(6);
        con.recv_dat(buf.as_mut_slice())?;
        buf.dec_type(&mut res_hdr)?;

        if res_hdr.length != 8 {
            debug!("AdsLocalClient.open_port: {:?}", &res_hdr);
            return Err(AdsError::from(0x000E));
        }
        if res_hdr.flags != AdsTcpFlags::PortOpen {
            debug!("AdsLocalClient.open_port: {:?}", &res_hdr);
            return Err(AdsError::from(0x001C));
        }

        buf.resize(res_hdr.length as usize);
        con.recv_dat(buf.as_mut_slice())?;
        let res_cmd = buf.dec_into_type::<AdsTcpPortOpenResponse>()?;
        Ok(res_cmd.addr)
    }

    pub(crate) fn close_port(con: &mut AdsTcpConnection) -> AdsResult<()> {
        let req_hdr = AdsTcpHeader { flags: AdsTcpFlags::PortClose, length: 0 };
        let mut buf = AdsBuffer::with_capacity(req_hdr.size());
        buf.enc_type(&req_hdr)?;
        con.send_dat(buf.as_slice())?;

        let mut res_hdr = AdsTcpHeader::default();
        buf.resize(6);
        con.recv_dat(buf.as_mut_slice())?;
        buf.dec_type(&mut res_hdr)?;

        if res_hdr.flags == AdsTcpFlags::RouterNote {
            //buf.resize(resHdr.length as usize);
            //self.con.recv_dat(buf.as_mut_slice())?;
            //let note = buf.dec_into_type::<AdsTcpPortRouterNote>()?;
            //println!("AdsTcpPortRouterNote: flags=0x{:#X}, length={}, state={}", resHdr.flags, resHdr.length, note.state);
            return Ok(());
        }
        if res_hdr.length != 0 {
            debug!("AdsLocalClient.close_port: {:?}", &res_hdr);
            return Err(AdsError::from(0x000E));
        }
        if res_hdr.flags != AdsTcpFlags::PortClose {
            debug!("AdsLocalClient.close_port: {:?}", &res_hdr);
            return Err(AdsError::from(0x001C));
        }
        Ok(())
    }

    fn send_recv<TReq: AdsCodable, TRes: AdsCodable>(con: &mut AdsTcpConnection, flags: u16, req: TReq) -> AdsResult<TRes> {
        let mut hdr = AdsTcpHeader { flags, length: req.size() as u32 };
        let mut buf = AdsBuffer::with_capacity(hdr.size() + hdr.size());
        buf.enc_type(&hdr)?;
        buf.enc_type(&req)?;
        con.send_dat(buf.as_slice())?;

        buf.resize(6);
        con.recv_dat(buf.as_mut_slice())?;
        buf.dec_type(&mut hdr)?;

        let mut res = TRes::default();
        if hdr.length != res.size() as u32 {
            debug!("AdsLocalClient.send_recv: {:?}", &hdr);
            return Err(AdsError::from(0x000E));
        }
        if hdr.flags != flags {
            debug!("AdsLocalClient.send_recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }

        buf.resize(hdr.length as usize);
        con.recv_dat(buf.as_mut_slice())?;
        buf.dec_type(&mut res)?;
        Ok(res)
    }

    pub(crate) fn get_net_id(con: &mut AdsTcpConnection) -> AdsResult<AdsNetId> {
        let req = AdsTcpPortGetNetIdRequest { flags: 0 };
        let res: AdsTcpPortGetNetIdResponse = Self::send_recv(con, AdsTcpFlags::GetLocalNetId, req)?;
        Ok(res.id)
    }

    pub(crate) fn get_runtime_type(con: &mut AdsTcpConnection) -> AdsResult<u32> {
        let req = AdsTcpPortGetRuntimeTypeRequest {};
        let res: AdsTcpPortGetRuntimeTypeResponse = Self::send_recv(con, AdsTcpFlags::GetRuntimeType, req)?;
        Ok(res.runtime)
    }
}

#[derive(Default)]
struct AdsTcpPortOpenRequest {
    port: u16,
}

impl AdsCodable for AdsTcpPortOpenRequest {
    fn size(&self) -> usize {
        size_of::<u16>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u16(self.port)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u16(&mut self.port)
    }
}

#[derive(Default)]
struct AdsTcpPortOpenResponse {
    addr: AdsAddr,
}

impl AdsCodable for AdsTcpPortOpenResponse {
    fn size(&self) -> usize {
        self.addr.size()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_type(&self.addr)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_type(&mut self.addr)
    }
}

#[derive(Default)]
struct AdsTcpPortRouterNote {
    state: u32,
}

impl AdsCodable for AdsTcpPortRouterNote {
    fn size(&self) -> usize {
        size_of::<u32>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.state)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.state)
    }
}

#[derive(Default)]
struct AdsTcpPortGetNetIdRequest {
    flags: u32,
}

impl AdsCodable for AdsTcpPortGetNetIdRequest {
    fn size(&self) -> usize {
        size_of::<u32>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.flags)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.flags)
    }
}

#[derive(Default)]
struct AdsTcpPortGetNetIdResponse {
    id: AdsNetId,
}

impl AdsCodable for AdsTcpPortGetNetIdResponse {
    fn size(&self) -> usize {
        self.id.size()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_type(&self.id)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_type(&mut self.id)
    }
}

#[allow(non_snake_case)]
pub mod AdsTcpRuntimeType {
    pub const RT: u32 = 0;
    pub const UM: u32 = 1; // also TcOs like TC/BSD, TC/RTOS?
}

#[derive(Default)]
struct AdsTcpPortGetRuntimeTypeRequest {}

impl AdsCodable for AdsTcpPortGetRuntimeTypeRequest {
    fn size(&self) -> usize {
        0
    }
    fn encode(&self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        Ok(())
    }
    fn decode(&mut self, _buf: &mut AdsBuffer) -> AdsResult<()> {
        Ok(())
    }
}

#[derive(Default)]
struct AdsTcpPortGetRuntimeTypeResponse {
    runtime: u32,
}

impl AdsCodable for AdsTcpPortGetRuntimeTypeResponse {
    fn size(&self) -> usize {
        size_of::<u32>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(self.runtime)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u32(&mut self.runtime)
    }
}
