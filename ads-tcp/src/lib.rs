// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
#![allow(dead_code)] // TODO

mod local;

use crate::local::AdsLocalClient;
pub use ads_def::*;
use ads_def::{commands::*, header::*};
use log::{debug, trace};
use std::{
    io::{ErrorKind, Read, Write},
    mem::size_of,
    net::{IpAddr, Ipv4Addr, Shutdown, SocketAddr, TcpStream},
    time::Duration,
};

const AdsTcpPort: u16 = 48898;
const AdsTcpTimeout: u64 = 5;

#[derive(Default, Debug)]
struct AdsTcpHeader {
    pub flags: u16,
    pub length: u32,
}

impl AdsTcpHeader {
    fn new(flags: u16, length: usize) -> Self {
        AdsTcpHeader { flags, length: length as u32 }
    }
}

impl AdsCodable for AdsTcpHeader {
    fn size(&self) -> usize {
        size_of::<u16>() + size_of::<u32>()
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u16(self.flags)?;
        buf.enc_u32(self.length)
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.dec_u16(&mut self.flags)?;
        buf.dec_u32(&mut self.length)
    }
}

type AdsTcpResult<T> = Result<T, std::io::Error>;

struct AdsTcpConnection {
    stream: TcpStream,
    target: SocketAddr,
    send_buf: AdsBuffer,
    recv_buf: AdsBuffer,
}

impl AdsTcpConnection {
    pub fn new(ip: Ipv4Addr) -> AdsTcpResult<Self> {
        let target = SocketAddr::from((ip, AdsTcpPort));
        let stream = Self::connect_stream(&target, Duration::from_secs(AdsTcpTimeout))?;

        Ok(AdsTcpConnection {
            stream,
            target,
            send_buf: AdsBuffer::with_capacity(2048),
            recv_buf: AdsBuffer::with_capacity(2048),
        })
    }

    pub fn connect(&mut self) -> AdsTcpResult<()> {
        let _ = self.disconnect();
        self.stream = Self::connect_stream(&self.target, Duration::from_secs(AdsTcpTimeout))?;
        Ok(())
    }

    fn connect_stream(addr: &SocketAddr, timeout: Duration) -> AdsTcpResult<TcpStream> {
        trace!("AdsTcpConnection::connect: addr={}", addr);
        let stream = TcpStream::connect_timeout(addr, timeout)?;
        stream.set_nodelay(true)?;
        stream.set_read_timeout(Some(Duration::from_secs(AdsTcpTimeout)))?;
        stream.set_write_timeout(Some(Duration::from_secs(AdsTcpTimeout)))?;
        Ok(stream)
    }

    pub fn disconnect(&mut self) {
        let _ = self.stream.shutdown(Shutdown::Both);
    }

    pub fn local_ip(&self) -> AdsResult<Ipv4Addr> {
        match self.stream.local_addr()?.ip() {
            IpAddr::V4(local) => Ok(local),
            IpAddr::V6(ip) => ip.to_ipv4().ok_or(AdsError::from(0x0749)),
        }
    }

    fn send(stream: &mut TcpStream, data: &[u8]) -> AdsResult<()> {
        match stream.write_all(data) {
            Ok(()) => Ok(()),
            Err(e) => {
                debug!("AdsTcpConnection.send: {:?}", &e);
                Err(AdsError::from_io(e, 0x001A))
            }
        }
    }

    fn send_dat(&mut self, data: &[u8]) -> AdsResult<()> {
        Self::send(&mut self.stream, data)
    }

    fn send_req<TAdsReq: AdsRequest>(&mut self, hdr: &AdsHeader, req: &TAdsReq) -> AdsResult<()> {
        let buf = &mut self.send_buf;
        let tcp_hdr = AdsTcpHeader::new(0, hdr.size() + req.size());
        buf.resize(0);
        buf.enc_type(&tcp_hdr)?;
        buf.enc_type(hdr)?;
        buf.enc_type(req)?;
        Self::send(&mut self.stream, self.send_buf.as_slice())
    }

    fn recv(stream: &mut TcpStream, data: &mut [u8]) -> AdsResult<()> {
        loop {
            match stream.read_exact(data) {
                Ok(_) => return Ok(()),
                Err(e) => {
                    return match e.kind() {
                        ErrorKind::Interrupted => continue,                      // retry
                        ErrorKind::UnexpectedEof => Err(AdsError::from(0x001B)), // disconnect
                        ErrorKind::WouldBlock => Err(AdsError::from(0x0015)),    // timeout
                        _ => {
                            debug!("AdsTcpConnection.recv: {:?}", &e);
                            Err(AdsError::from_io(e, 0x0005))
                        }
                    };
                }
            }
        }
    }

    fn recv_dat(&mut self, data: &mut [u8]) -> AdsResult<()> {
        Self::recv(&mut self.stream, data)
    }

    fn recv_res<TAdsRes: AdsResponse>(&mut self, hdr: &mut AdsHeader, res: &mut TAdsRes) -> AdsResult<()> {
        let mut tcp_hdr = AdsTcpHeader::default();
        self.recv_buf.resize(tcp_hdr.size() as usize);
        Self::recv(&mut self.stream, self.recv_buf.as_mut_slice())?;
        self.recv_buf.dec_type(&mut tcp_hdr)?;
        if tcp_hdr.flags != 0 {
            debug!("AdsTcpConnection.recv_res: {:?}", &tcp_hdr);
            return Err(AdsError::from(0x001C));
        }
        if tcp_hdr.length == 0 {
            debug!("AdsTcpConnection.recv_res: {:?}", &tcp_hdr);
            return Err(AdsError::from(0x001C));
        }
        self.recv_buf.resize(tcp_hdr.length as usize);
        Self::recv(&mut self.stream, self.recv_buf.as_mut_slice())?;
        self.recv_buf.dec_type(hdr)?;
        if hdr.error != 0 {
            debug!("AdsTcpConnection.recv_res: {:?}", &hdr);
            return Err(AdsError::from(hdr.error));
        }
        self.recv_buf.dec_type(res)?;
        Ok(())
    }
}

pub struct AdsTcpClient {
    con: AdsTcpConnection,
    local: AdsAddr,
}

impl AdsTcpClient {
    pub fn new(ip: Ipv4Addr) -> AdsResult<AdsTcpClient> {
        let con = AdsTcpConnection::new(ip)?;
        let local_ip = con.local_ip()?;
        let local = AdsAddr::new(AdsNetId::from_ipv4(&local_ip), 10000);
        Ok(AdsTcpClient { con, local })
    }

    pub fn new_local() -> AdsResult<AdsTcpClient> {
        let mut con = AdsTcpConnection::new(Ipv4Addr::LOCALHOST)?;
        let local_ip = con.local_ip()?;
        let local = AdsAddr::new(AdsNetId::from_ipv4(&local_ip), 10000);
        AdsLocalClient::open_port(&mut con, 0)?;
        Ok(AdsTcpClient { con, local })
    }

    pub fn local_ip(&self) -> AdsResult<Ipv4Addr> {
        self.con.local_ip()
    }

    pub fn set_local(&mut self, addr: AdsAddr) {
        self.local = addr;
    }

    fn send<TAdsReq: AdsRequest>(&mut self, target: AdsAddr, req: &TAdsReq) -> AdsResult<u32> {
        let hdr = AdsHeader::new_req(target, self.local, req.command(), req.size() as u32);
        self.con.send_req(&hdr, req)?;
        Ok(hdr.invoke)
    }

    fn recv<TAdsRes: AdsResponse>(&mut self, target: AdsAddr, invoke: u32) -> AdsResult<TAdsRes> {
        let mut hdr = AdsHeader::default();
        let mut res = TAdsRes::default();
        self.con.recv_res(&mut hdr, &mut res)?;
        if hdr.source != target && !target.id.is_empty() {
            debug!("AdsTcpClient.recv: {:?}", &hdr);
            return Err(AdsError::from(0x000F));
        }
        if hdr.target != self.local {
            debug!("AdsTcpClient.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        if hdr.state != AdsStateFlag::Response as u16 {
            debug!("AdsTcpClient.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        if hdr.command != res.command() {
            debug!("AdsTcpClient.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        if hdr.invoke != invoke {
            debug!("AdsTcpClient.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        Ok(res)
    }

    fn send_recv<TAdsReq: AdsRequest, TAdsRes: AdsResponse>(&mut self, target: AdsAddr, req: TAdsReq) -> AdsResult<TAdsRes> {
        let invoke = self.send(target, &req)?;
        self.recv(target, invoke)
    }
}

impl AdsConnection for AdsTcpClient {
    fn local_addr(&self) -> AdsResult<AdsAddr> {
        Ok(self.local)
    }

    fn connect(&mut self) -> AdsResult<()> {
        match self.con.connect() {
            Ok(_) => Ok(()),
            Err(e) => {
                debug!("AdsTcpClient.connect: {:?}", &e);
                Err(AdsError::from_io(e, 0x000D))
            }
        }
    }

    fn disconnect(&mut self) {
        self.con.disconnect()
    }

    fn send_recv(&mut self, target: AdsAddr, req: AdsReq) -> AdsResult<AdsRes> {
        match req {
            AdsReq::Read(r) => Ok(AdsRes::Read(self.send_recv(target, r)?)),
            AdsReq::Write(r) => Ok(AdsRes::Write(self.send_recv(target, r)?)),
            AdsReq::ReadWrite(r) => Ok(AdsRes::ReadWrite(self.send_recv(target, r)?)),
            AdsReq::ReadState(r) => Ok(AdsRes::ReadState(self.send_recv(target, r)?)),
            AdsReq::WriteControl(r) => Ok(AdsRes::WriteControl(self.send_recv(target, r)?)),
            AdsReq::ReadInfo(r) => Ok(AdsRes::ReadInfo(self.send_recv(target, r)?)),
        }
    }
}
