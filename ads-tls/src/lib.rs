// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
pub use ads_def::*;
use ads_def::{commands::*, header::*};
use log::debug;
use native_tls::{Identity, Protocol, TlsConnector, TlsStream};
use std::{
    error::Error,
    fmt::{self, Display},
    fs::File,
    io::{ErrorKind, Read, Write},
    mem::size_of,
    net::{IpAddr, Ipv4Addr, SocketAddr, TcpStream},
    time::Duration,
};
//use native_tls::Certificate;

pub const AdsTlsPort: u16 = 8016;
pub const AdsTlsTimeout: u64 = 5;

#[allow(non_snake_case)]
pub mod AdsTlsFlags {
    pub const Response: u16 = 0x0001; // TlsConnectInfoFlag_Response
    pub const AmsAllowed: u16 = 0x0002; // TlsConnectInfoFlag_AmsAllowed - route accepted
    pub const ServerInfo: u16 = 0x0004; // TlsConnectInfoFlag_ServerInfo - save server info
    pub const OwnFile: u16 = 0x0008; // TlsConnectInfoFlag_OwnFile
    pub const SelfSigned: u16 = 0x0010; // TlsConnectInfoFlag_SelfSigned
    pub const IpAddr: u16 = 0x0020; // TlsConnectInfoFlag_IpAddr
    pub const IgnoreCn: u16 = 0x0040; // TlsConnectInfoFlag_IgnoreCn
    pub const AddRemote: u16 = 0x0080; // TlsConnectInfoFlag_AddRemote
}

#[allow(non_snake_case)]
pub mod AdsTlsErrors {
    pub const None: u8 = 0; // TlsConnectInfoError_NoError
    pub const Version: u8 = 1; // TlsConnectInfoError_Version
    pub const CnMismatch: u8 = 2; // TlsConnectInfoError_CnMismatch
    pub const UnknownCert: u8 = 3; // TlsConnectInfoError_UnkownCert
    pub const UnknownUser: u8 = 4; // TlsConnectInfoError_UnkownUser
}

fn error_into_message(e: u8) -> &'static str {
    match e {
        0 => "None",
        1 => "Version",
        2 => "CN mismatch",
        3 => "Cert unknown",
        4 => "User unknown",
        _ => "Unknown",
    }
}

#[derive(Default, Debug)]
pub struct AdsTlsConnectInfo {
    pub flags: u16,
    pub version: u8,
    pub error: u8,
    pub id: AdsNetId,
    pub host: String,
    pub user: String,
    pub pass: String,
}

impl AdsCodable for AdsTlsConnectInfo {
    fn size(&self) -> usize {
        let mut size = size_of::<u16>()
            + size_of::<u16>()
            + size_of::<u8>()
            + size_of::<u8>()
            + self.id.size()
            + size_of::<u8>() // user len
            + size_of::<u8>() // pass len
            + size_of::<u16>() // host len
            + 16; // reserved
        if self.host.len() >= 32 {
            size += self.host.len() + 1;
        } else {
            size += 32;
        }
        if !self.user.is_empty() {
            size += self.user.len() + 1;
        }
        if !self.pass.is_empty() {
            size += self.pass.len() + 1;
        }
        size
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u16(self.size() as u16)?;
        buf.enc_u16(self.flags)?;
        buf.enc_u8(self.version)?;
        buf.enc_u8(self.error)?;
        buf.enc_type(&self.id)?;
        if !self.user.is_empty() {
            buf.enc_u8(self.user.len() as u8 + 1)?;
        } else {
            buf.enc_u8(0)?;
        }
        if !self.pass.is_empty() {
            buf.enc_u8(self.pass.len() as u8 + 1)?;
        } else {
            buf.enc_u8(0)?;
        }
        if self.host.len() >= 32 {
            buf.enc_u16(self.host.len() as u16 + 1)?;
        } else {
            buf.enc_u16(0)?;
        }
        buf.enc_slice(&[0; 16])?;
        if self.host.len() >= 32 {
            buf.enc_utf8(&self.host)?;
        } else {
            buf.enc_utf8_len(&self.host, 32)?;
        }
        if !self.user.is_empty() {
            buf.enc_utf8(&self.user)?;
        }
        if !self.pass.is_empty() {
            buf.enc_utf8(&self.pass)?;
        }
        Ok(())
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        let length = buf.dec_into_u16()? as usize;
        if buf.len() != length {
            debug!("AdsTlsConnectInfo.decode: invalid length");
            return Err(AdsError::from(0x000E));
        }
        buf.dec_u16(&mut self.flags)?;
        buf.dec_u8(&mut self.version)?;
        buf.dec_u8(&mut self.error)?;
        buf.dec_type(&mut self.id)?;
        let user_len = buf.dec_into_u8()?;
        let pass_len = buf.dec_into_u8()?;
        let host_len = buf.dec_into_u16()?;
        buf.skip(16);
        if host_len >= 32 {
            buf.dec_utf8_len(&mut self.host, host_len as usize)?;
        } else {
            buf.dec_utf8_len(&mut self.host, 32)?;
        }
        if user_len != 0 {
            buf.dec_utf8_len(&mut self.user, user_len as usize)?;
        } else {
            self.user = String::new()
        }
        if pass_len != 0 {
            buf.dec_utf8_len(&mut self.pass, pass_len as usize)?;
        } else {
            self.pass = String::new()
        }
        Ok(())
    }
}

#[derive(Debug)]
pub enum AdsTlsError {
    Connect(u8),
    Ads(ads_def::AdsError),
    Io(std::io::Error),
    Tls(native_tls::Error),
    Handshake(native_tls::HandshakeError<TcpStream>),
}

impl Error for AdsTlsError {}

impl Display for AdsTlsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AdsTlsError::Connect(e) => {
                write!(f, "ADS TLS Connect Error: {} ({:#x})", error_into_message(*e), *e)
            }
            AdsTlsError::Ads(e) => e.fmt(f),
            AdsTlsError::Io(e) => e.fmt(f),
            AdsTlsError::Tls(e) => e.fmt(f),
            AdsTlsError::Handshake(e) => e.fmt(f),
        }
    }
}

impl From<ads_def::AdsError> for AdsTlsError {
    fn from(e: ads_def::AdsError) -> AdsTlsError {
        AdsTlsError::Ads(e)
    }
}

impl From<std::io::Error> for AdsTlsError {
    fn from(e: std::io::Error) -> AdsTlsError {
        AdsTlsError::Io(e)
    }
}

impl From<native_tls::Error> for AdsTlsError {
    fn from(e: native_tls::Error) -> AdsTlsError {
        AdsTlsError::Tls(e)
    }
}

type AdsTlsResult<T> = Result<T, AdsTlsError>;

pub struct AdsTlsConnection {
    stream: TlsStream<TcpStream>,
    target: SocketAddr,
    send_buf: AdsBuffer,
    recv_buf: AdsBuffer,
}

impl AdsTlsConnection {
    pub fn new(addr: Ipv4Addr) -> AdsTlsResult<AdsTlsConnection> {
        let target = SocketAddr::from((addr, AdsTlsPort));
        let stream = Self::connect_stream(&target, Duration::from_secs(AdsTlsTimeout))?;

        Ok(AdsTlsConnection {
            stream,
            target,
            send_buf: AdsBuffer::with_capacity(2048),
            recv_buf: AdsBuffer::with_capacity(2048),
        })
    }

    //    fn open_cert(path: &str) -> AdsTlsResult<Certificate> {
    //        let mut file = File::open(path)?;
    //        let mut content = Vec::new();
    //        file.read_to_end(&mut content)?;
    //        Certificate::from_pem(&content).map_err(AdsError::Tls)
    //    }

    fn open_identity(path: &str) -> AdsTlsResult<Identity> {
        let mut file = File::open(path)?;
        let mut content = Vec::new();
        file.read_to_end(&mut content)?;
        Identity::from_pkcs12(&content, "").map_err(AdsTlsError::Tls)
    }

    pub fn connect(&mut self) -> AdsTlsResult<()> {
        let _ = self.disconnect();
        self.stream = Self::connect_stream(&self.target, Duration::from_secs(AdsTlsTimeout))?;
        Ok(())
    }

    fn connect_stream(addr: &SocketAddr, timeout: Duration) -> AdsTlsResult<TlsStream<TcpStream>> {
        let ident = Self::open_identity("/usr/share/certs/NilsJ-TP.pfx")?;

        let stream = TcpStream::connect_timeout(addr, timeout)?;
        stream.set_nodelay(true)?;
        stream.set_read_timeout(Some(Duration::from_secs(AdsTlsTimeout)))?;
        stream.set_write_timeout(Some(Duration::from_secs(AdsTlsTimeout)))?;

        let mut builder = TlsConnector::builder();
        builder.min_protocol_version(Some(Protocol::Tlsv12));
        builder.max_protocol_version(Some(Protocol::Tlsv12));
        builder.identity(ident);
        builder.disable_built_in_roots(true);
        builder.danger_accept_invalid_hostnames(true);
        builder.danger_accept_invalid_certs(true);
        //builder.add_root_certificate(cert);
        let connector = builder.build()?;
        let stream = connector.connect(&addr.to_string(), stream);

        match stream {
            Ok(s) => Ok(s),
            Err(e) => {
                debug!("AdsTlsConnection.connect_strea: {:?}", &e);
                Err(AdsTlsError::Handshake(e))
            }
        }
    }

    pub fn disconnect(&mut self) {
        let _ = self.stream.shutdown();
    }

    pub fn local_ip(&self) -> AdsResult<Ipv4Addr> {
        match self.stream.get_ref().local_addr()?.ip() {
            IpAddr::V4(local) => Ok(local),
            IpAddr::V6(ip) => ip.to_ipv4().ok_or(AdsError::from(0x0749)),
        }
    }

    fn send(stream: &mut TlsStream<TcpStream>, data: &[u8]) -> AdsResult<()> {
        match stream.write_all(data) {
            Ok(()) => Ok(()),
            Err(e) => {
                debug!("AdsTlsConnection.send: {:?}", &e);
                Err(AdsError::from_io(e, 0x001D))
            }
        }
    }

    pub fn send_dat(&mut self, data: &[u8]) -> AdsResult<()> {
        Self::send(&mut self.stream, data)
    }

    pub fn send_req<TAdsReq: AdsRequest>(&mut self, hdr: &AdsHeader, req: &TAdsReq) -> AdsResult<()> {
        let buf = &mut self.send_buf;
        buf.resize(0);
        buf.enc_type(hdr)?;
        buf.enc_type(req)?;
        Self::send(&mut self.stream, self.send_buf.as_slice())?;
        Ok(())
    }

    fn recv(stream: &mut TlsStream<TcpStream>, data: &mut [u8]) -> AdsResult<()> {
        loop {
            match stream.read(data) {
                Ok(0) => {
                    println!("? AdsTlsConnection.recv(0)");
                    continue;
                }
                Ok(n) => {
                    assert_eq!(data.len(), n, "? AdsTlsConnection: Invalid length readen!");
                    return Ok(());
                }
                Err(e) => {
                    debug!("AdsTlsConnection.recv: {:?}", &e);
                    match e.kind() {
                        ErrorKind::Interrupted => return Err(AdsError::from(0x0015)),
                        ErrorKind::UnexpectedEof => return Err(AdsError::from(0x001b)),
                        _ => return Err(AdsError::from_io(e, 0x0005)),
                    }
                }
            }
        }
        //match stream.read_exact(data) // TODO
    }

    pub fn recv_dat(&mut self, data: &mut [u8]) -> AdsResult<()> {
        Self::recv(&mut self.stream, data)
    }

    pub fn recv_res<TAdsRes: AdsResponse>(&mut self, hdr: &mut AdsHeader, res: &mut TAdsRes) -> AdsResult<()> {
        self.recv_buf.resize(hdr.size());
        Self::recv(&mut self.stream, self.recv_buf.as_mut_slice())?;
        self.recv_buf.dec_type(hdr)?;
        if hdr.length == 0 {
            debug!("AdsTlsConnection.recv_res: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        self.recv_buf.resize(hdr.length as usize);
        Self::recv(&mut self.stream, self.recv_buf.as_mut_slice())?;
        if hdr.error != 0 {
            debug!("AdsTlsConnection.recv_res: {:?}", &hdr);
            return Err(AdsError::from(hdr.error));
        }
        self.recv_buf.dec_type(res)?;
        Ok(())
    }
}

pub struct AdsTlsClient {
    con: AdsTlsConnection,
    local: AdsAddr,
}

impl AdsTlsClient {
    pub fn new(addr: Ipv4Addr) -> AdsTlsResult<AdsTlsClient> {
        Self::new_route(addr, "", "", "")
    }

    pub fn new_route(addr: Ipv4Addr, host_name: &str, user_name: &str, user_pass: &str) -> AdsTlsResult<AdsTlsClient> {
        let mut con = AdsTlsConnection::new(addr)?;

        let local_ip = con.local_ip()?;
        let local = AdsAddr::new(AdsNetId::from_ipv4(&local_ip), 10000);

        Self::connect_client(&mut con, &local.id, String::from(host_name), String::from(user_name), String::from(user_pass))?;

        Ok(AdsTlsClient { con, local })
    }

    fn connect_client(con: &mut AdsTlsConnection, local: &AdsNetId, host_name: String, user_name: String, user_pass: String) -> AdsTlsResult<()> {
        let req: AdsTlsConnectInfo;
        if host_name.is_empty() && user_name.is_empty() && user_pass.is_empty() {
            req = AdsTlsConnectInfo {
                flags: 0,
                version: 1,
                error: 0,
                id: *local,
                host: String::new(),
                user: String::new(),
                pass: String::new(),
            };
        } else {
            req = AdsTlsConnectInfo {
                flags: AdsTlsFlags::SelfSigned | AdsTlsFlags::AddRemote | AdsTlsFlags::OwnFile,
                version: 1,
                error: 0,
                id: *local,
                host: host_name,
                user: user_name,
                pass: user_pass,
            };
        }

        let buf = AdsBuffer::from_type(&req);
        con.send_dat(buf.as_slice())?;

        let mut res = AdsTlsConnectInfo::default();
        let mut buf = AdsBuffer::with_size(res.size());
        con.recv_dat(buf.as_mut_slice())?;
        buf.dec_type(&mut res)?;

        assert_eq!(res.version, 1, "AdsTlsConnectInfo with invalid version!");
        assert_eq!(res.flags & AdsTlsFlags::Response, AdsTlsFlags::Response, "AdsTlsConnectInfo is not a response!");

        if res.error != 0 {
            debug!("AdsTlsConnection.connect_client: {:?}", &res);
            return Err(AdsTlsError::Connect(res.error));
        }
        if (res.flags & AdsTlsFlags::AmsAllowed) == 0 {
            debug!("AdsTlsConnection.connect_client: {:?}", &res);
            return Err(AdsTlsError::Ads(AdsError::from(0x001E)));
        }
        Ok(())
    }

    pub fn local_ip(&self) -> AdsResult<Ipv4Addr> {
        self.con.local_ip()
    }

    pub fn set_local(&mut self, addr: AdsAddr) {
        self.local = addr;
    }

    fn send<TAdsReq: AdsRequest>(&mut self, target: AdsAddr, req: &TAdsReq) -> AdsResult<u32> {
        let hdr = AdsHeader::new_req(target, self.local, req.command(), req.size() as u32);
        self.con.send_req(&hdr, req)?;
        Ok(hdr.invoke)
    }

    fn recv<TAdsRes: AdsResponse>(&mut self, target: AdsAddr, invoke: u32) -> AdsResult<TAdsRes> {
        let mut hdr = AdsHeader::default();
        let mut res = TAdsRes::default();
        self.con.recv_res(&mut hdr, &mut res)?;
        if hdr.source != target && !target.id.is_empty() {
            debug!("AdsTlsConnection.recv: {:?}", &hdr);
            return Err(AdsError::from(0x000F));
        }
        if hdr.target != self.local {
            debug!("AdsTlsConnection.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        if hdr.command != res.command() {
            debug!("AdsTlsConnection.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        if hdr.state != AdsStateFlag::Response as u16 {
            debug!("AdsTlsConnection.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        if hdr.invoke != invoke {
            debug!("AdsTlsConnection.recv: {:?}", &hdr);
            return Err(AdsError::from(0x001C));
        }
        Ok(res)
    }

    fn send_recv<TAdsReq: AdsRequest, TAdsRes: AdsResponse>(&mut self, target: AdsAddr, req: TAdsReq) -> AdsResult<TAdsRes> {
        let invoke = self.send(target, &req)?;
        self.recv(target, invoke)
    }
}

impl AdsConnection for AdsTlsClient {
    fn local_addr(&self) -> AdsResult<AdsAddr> {
        Ok(self.local)
    }

    fn connect(&mut self) -> AdsResult<()> {
        self.con.connect().map_err(|e| {
            debug!("AdsTlsClient.connect: {:?}", &e);
            match e {
                AdsTlsError::Ads(e) => e,
                AdsTlsError::Io(e) => AdsError::from_io(e, 0x001D),
                AdsTlsError::Tls(_) => AdsError::from(0x001E),
                AdsTlsError::Handshake(_) => AdsError::from(0x001E),
                _ => AdsError::from(0x000D),
            }
        })
    }

    fn disconnect(&mut self) {
        self.con.disconnect()
    }

    fn send_recv(&mut self, target: AdsAddr, req: AdsReq) -> AdsResult<AdsRes> {
        match req {
            AdsReq::Read(r) => Ok(AdsRes::Read(self.send_recv(target, r)?)),
            AdsReq::Write(r) => Ok(AdsRes::Write(self.send_recv(target, r)?)),
            AdsReq::ReadWrite(r) => Ok(AdsRes::ReadWrite(self.send_recv(target, r)?)),
            AdsReq::ReadState(r) => Ok(AdsRes::ReadState(self.send_recv(target, r)?)),
            AdsReq::WriteControl(r) => Ok(AdsRes::WriteControl(self.send_recv(target, r)?)),
            AdsReq::ReadInfo(r) => Ok(AdsRes::ReadInfo(self.send_recv(target, r)?)),
        }
    }
}
