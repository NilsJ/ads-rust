// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_tls::{AdsAddr, AdsClient, AdsConnection, AdsTlsClient};
use std::{env::args, error::Error, net::Ipv4Addr};
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let args: Vec<String> = args().collect();
    if args.len() <= 1 {
        panic!("Usage: {} <ip> [<host> <user> <pass>]", args[0]);
    }

    let addr = AdsAddr::default();
    let ip_addr = args[1].as_str().parse::<Ipv4Addr>()?;

    // --------------------------------------------------
    //
    if args.len() >= 4 {
        let host_name = &args[2];
        let user_name = &args[3];
        let user_pass = &args[4];

        println!("# Connect (add route)...");
        let mut client = AdsTlsClient::new_route(ip_addr, host_name, user_name, user_pass)?;

        println!("# Client");
        println!("Local: {} ({})", client.get_local()?, client.local_ip()?);
        println!("Target: {} ({})", addr, ip_addr);

        println!("# State");
        let (ads_state, dev_state) = client.read_state(addr)?;
        println!("AdsState: {}", ads_state);
        println!("DevState: {}", dev_state);

        println!("# Info");
        let (version, name) = client.read_info(addr)?;
        println!("Name: {}", name);
        println!("Version: {}", version);

        client.disconnect();
        println!();
    }
    // --------------------------------------------------
    {
        println!("# Connect (use route)");
        let mut client = AdsTlsClient::new(ip_addr)?;

        println!("# Client");
        println!("Local: {} ({})", client.get_local()?, client.local_ip()?);
        println!("Target: {} ({})", addr, ip_addr);

        println!("# State");
        let (ads_state, dev_state) = client.read_state(addr)?;
        println!("AdsState: {}", ads_state);
        println!("DevState: {}", dev_state);

        println!("# Info");
        let (version, name) = client.read_info(addr)?;
        println!("Name: {}", name);
        println!("Version: {}", version);

        client.disconnect();
        println!();
    }
    // --------------------------------------------------
    Ok(())
}
