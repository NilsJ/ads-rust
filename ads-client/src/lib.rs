// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>

pub use ads_def::*;
use std::{env::args, error::Error, str::FromStr};

pub struct AdsClient {}

impl AdsClient {
    #[cfg(feature = "lib")]
    pub fn connect() -> Result<(AdsClientRef, AdsNetId), Box<dyn Error>> {
        use ads_lib::AdsLibClient;

        let args: Vec<String> = args().collect();
        let addr = AdsAddr::from_str(&args[1])?;

        let mut client = AdsLibClient::new();
        client.set_timeout(1000)?; // timeout of 1s
        Ok((AdsClientRef::new(client), addr.id))
    }

    #[cfg(any(feature = "tcp", not(feature = "lib")))]
    pub fn connect() -> Result<(AdsClientRef, AdsNetId), Box<dyn Error>> {
        use ads_tcp::AdsTcpClient;
        use std::net::Ipv4Addr;

        let args: Vec<String> = args().collect();
        let addr = Ipv4Addr::from_str(&args[1])?;

        let client = AdsTcpClient::new(addr)?;
        Ok((AdsClientRef::new(client), AdsNetId::default()))
    }
}
