#!/bin/sh
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>

set -eu
#set -x

target_ip="${1:-"192.168.178.38"}"
target_ads="${2:-"5.69.183.127.1.1"}"

user="Administrator"
pass="qwe"

## transfer binaries to target
pwd="$(realpath "$(dirname "${0}")")"
boot="$(realpath "${pwd}/../../.test/TcRustTest/_Boot/TwinCAT RT (x64)/")"
scp -r "${boot}" "${user}@${target_ip}:~/Boot/" > /dev/null 2>&1
ssh ${user}@${target_ip} 'doas rm -rf /usr/local/etc/TwinCAT/3.1/Boot'
ssh ${user}@${target_ip} 'doas mv -f ~/Boot /usr/local/etc/TwinCAT/3.1/Boot'

## restart TwinCAT on target
ssh ${user}@${target_ip} doas TcSysExe.exe --run
sleep 2

## execute ADS client
export RUST_BACKTRACE=full
cargo run --example ads-client-example -- \
	--target-ip="${target_ip}" --target-ads="${target_ads}" \
	--host="$(hostname)" --user="${user}" --pass="${pass}"

