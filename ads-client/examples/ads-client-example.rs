// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(clippy::float_cmp)]
use ads_devices::*;
use ads_disco::*;
use ads_tcp::*;
use std::{error::Error, mem::size_of, net::Ipv4Addr, result::Result};
use stderrlog::*;

fn get_arg<'a>(args: &'a AdsArgs, key: &str) -> &'a String {
    let arg = args.get_arg(key);
    if arg.is_none() {
        usage(key)
    }
    arg.unwrap()
}

fn usage(key: &str) {
    eprint!("Arguments: --target=<ip> [--target-ads=<net-id>] [--route=<route-name> --user=<user-name> --pass=<user-pass>]");
    panic!("Argument '{}' not valid!", key);
}

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let args = AdsArgs::default();

    let target_ip = args.get_target_ip();
    if target_ip.is_none() {
        usage("target")
    }
    let target_ip = target_ip.unwrap();

    let mut target_ads = args.get_target_ads();
    if target_ads.is_none() {
        target_ads = Some(AdsNetId::default());
    }
    let target_ads = target_ads.unwrap();
    let addr = AdsAddr::from_id(target_ads);

    let local_ip = AdsDiscoClient::local_addr(target_ip)?;
    let local_id = AdsNetId::from_ipv4(&local_ip);

    let mut tcp_client_result = AdsTcpClient::new(target_ip);
    if tcp_client_result.is_err() && target_ip != Ipv4Addr::LOCALHOST {
        let route_name = get_arg(&args, "route");
        let user_name = get_arg(&args, "user");
        let user_pass = get_arg(&args, "pass");

        println!("# Route");
        let udp_client = AdsDiscoClient::new(local_id)?;
        udp_client.add_route(target_ip, route_name, &local_ip.to_string(), user_name, user_pass)?;

        tcp_client_result = AdsTcpClient::new(target_ip);
    }

    println!("# Client");
    println!("Local: {} ({})", local_id, local_ip);
    println!("Target: {} ({})", addr, target_ip);

    let mut client = AdsClientRef::new(tcp_client_result?);

    // ------------------------------------------------------------
    // AdsClient
    {
        let (ads_state, _dev_state) = client.read_state(addr)?;
        assert_eq!(AdsState::Run, ads_state);

        let (name, version) = client.read_info(addr)?;
        assert_eq!("TwinCAT System", name);
        assert_eq!("3.1.4024", version.to_string());
    }
    // ------------------------------------------------------------
    // AdsSystemClient
    {
        let mut system_client = AdsSystemClient::new(client.clone(), addr.id);
        assert_eq!(AdsState::Run, system_client.get_state()?);

        assert_eq!("3.1.4024.29", system_client.product_version()?);

        let system_state = system_client.system_state()?;
        assert_eq!(AdsState::Run, AdsState::from(system_state.ads_state));
        assert_eq!(1, system_state.dev_state);
        //assert_eq!(1, system_state.restarts);
        assert_eq!("3.1.4024", system_state.version.to_string());
        assert_eq!(AdsPlatformIds::x64 as u8, system_state.platform);
        assert_eq!(AdsOsTypeIds::TC as u8, system_state.os_type);
        assert_eq!(0, system_state.flags);
        assert_eq!(0, system_state.timeout);

        let target_info = system_client.target_info()?;
        assert_eq!("TC/BSD-OS", target_info.target_type);
        assert_eq!("CB6263", target_info.hardware_model);
        assert_eq!("17470219210062", target_info.hardware_serial);
        assert_eq!("1.0", target_info.hardware_version);
        assert_eq!("20.5.19", target_info.hardware_date);
        assert_eq!("AMDx64", target_info.hardware_cpu);
        assert_eq!("CB6263", target_info.image_device);
        assert_eq!("13.0.11.3,2", target_info.image_version);
        assert_eq!("", target_info.image_level);
        assert_eq!("TwinCAT/BSD", target_info.image_os_name);
        assert_eq!("13.0", target_info.image_os_version);
        assert_eq!("3", target_info.twincat_version);
        assert_eq!("1", target_info.twincat_revision);
        assert_eq!("4024", target_info.twincat_build);
        assert_eq!("", target_info.twincat_level);
        if !addr.id.is_empty() {
            assert_eq!(addr.id.to_string(), target_info.net_id);
        }

        assert_eq!("CX", system_client.target_type()?);
        assert_eq!("TwinCAT OS (x64)", system_client.target_platform()?);
        assert_eq!("e077f076fe24ce7cdb23f148470181f3df520c27c2a07224779f4868ded53dbe", system_client.target_fingerprint()?);
        assert_eq!("1F15DAD6-F2F6-4CF9-9EF1-9EA30A8F4655", system_client.target_project_guid()?);
        assert_eq!("00000000-0000-0000-0000-000000000000", system_client.target_project_version()?);
        assert_eq!("TcRustTest", system_client.target_project_name()?);
    }
    // ------------------------------------------------------------
    // AdsSymbolClient
    {
        let mut symbol_client = AdsSymbolClient::new(client.clone(), AdsAddr::new(addr.id, 851));

        let plc_state = symbol_client.get_state()?;
        assert_eq!(AdsState::Run, plc_state);

        assert_eq!(1, symbol_client.read_version()?);

        let symbol_info = symbol_client.read_symbol_info("MAIN.bTest")?;
        assert_eq!(61488, symbol_info.grp);
        assert_eq!(385080, symbol_info.off);
        assert_eq!(1, symbol_info.size);
        assert_eq!(AdsTypeId::Bool, symbol_info.type_id);
        assert_eq!(4104, symbol_info.flags);
        assert_eq!("MAIN.bTest", symbol_info.name);
        assert_eq!("BOOL", symbol_info.type_name);
        assert_eq!("", symbol_info.comment);

        let handle = symbol_client.create_handle("MAIN.iTest")?;
        assert_ne!(0, handle);

        let write_buf = AdsBuffer::from_i16(76);
        symbol_client.write_by_handle(handle, write_buf.as_slice())?;

        let mut read_buf = AdsBuffer::with_size_of::<i16>();
        symbol_client.read_by_handle(handle, read_buf.as_mut_slice())?;
        assert_eq!(76, read_buf.dec_into_i16()?);

        symbol_client.release_handle(handle)?;

        let mut read_buf = AdsBuffer::with_size_of::<i16>();
        symbol_client.read_by_name("MAIN.iTest", read_buf.as_mut_slice())?;
        assert_eq!(76, read_buf.dec_into_i16()?);
    }
    // ------------------------------------------------------------
    // AdsSumReadClient, AdsSumWriteClient, AdsSumReadWriteClient
    {
        let sum_addr = AdsAddr::new(addr.id, 851);

        let mut sum_handle = AdsSumReadWriteClient::new(client.clone(), sum_addr);
        sum_handle.add(0xf003, 0, size_of::<u32>(), AdsBuffer::from_utf8("MAIN.bTest").as_slice());
        sum_handle.add(0xf003, 0, size_of::<u32>(), AdsBuffer::from_utf8("MAIN.iTest").as_slice());
        sum_handle.add(0xf003, 0, size_of::<u32>(), AdsBuffer::from_utf8("MAIN.nTest").as_slice());
        sum_handle.execute()?;
        sum_handle.chk_res()?;
        let handle1 = sum_handle.get_res_u32(0)?;
        assert_ne!(0, handle1);
        let handle2 = sum_handle.get_res_u32(1)?;
        assert_ne!(0, handle2);
        let handle3 = sum_handle.get_res_u32(2)?;
        assert_ne!(0, handle3);

        let mut sum_write = AdsSumWriteClient::new(client.clone(), sum_addr);
        sum_write.add(0xf005, handle1, AdsBuffer::from_bool(false).as_slice());
        sum_write.add(0xf005, handle2, AdsBuffer::from_i16(67).as_slice());
        sum_write.add(0xf005, handle3, AdsBuffer::from_u32(789).as_slice());
        sum_write.execute()?;
        sum_write.chk_res()?;

        let mut sum_read = AdsSumReadClient::new(client.clone(), sum_addr);
        sum_read.add(0xf005, handle1, size_of::<bool>() as u32);
        sum_read.add(0xf005, handle2, size_of::<i16>() as u32);
        sum_read.add(0xf005, handle3, size_of::<u32>() as u32);
        sum_read.execute()?;
        sum_read.chk_res()?;
        assert!(!sum_read.get_res_bool(0)?);
        assert_eq!(67, sum_read.get_res_i16(1)?);
        assert_eq!(789, sum_read.get_res_u32(2)?);

        let mut sum_release = AdsSumWriteClient::new(client.clone(), sum_addr);
        sum_release.add(0xf006, 0, AdsBuffer::from_u32(handle1).as_slice());
        sum_release.add(0xf006, 0, AdsBuffer::from_u32(handle2).as_slice());
        sum_release.add(0xf006, 0, AdsBuffer::from_u32(handle3).as_slice());
        sum_release.execute()?;
        sum_release.chk_res()?;
    }
    // ------------------------------------------------------------
    // AdsRouterClient
    {
        let mut router_client = AdsRouterClient::new(client.clone(), addr.id);
        assert_eq!(AdsState::Run, router_client.get_state()?);

        let info = router_client.read_info()?;
        assert!(info.memory_max >= (16 * 1024 * 1024));
        assert!(info.memory_avl >= (15 * 1024 * 1024));
        assert!(info.ports >= 25);
        assert!(info.drivers >= 1);
        assert!(info.routers >= 1);
        assert_eq!(info.debug, 0);
        //assert!(info.mailbox_size != 0);
        //assert!(info.mailbox_used != 0);

        let ports = router_client.read_ports()?;
        assert!(ports.len() > 25);
        assert!(ports.iter().any(|e| e.port == 1));
        assert!(ports.iter().any(|e| e.port == 10));
        assert!(ports.iter().any(|e| e.port == 11));
        assert!(ports.iter().any(|e| e.port == 12));
        assert!(ports.iter().any(|e| e.port == 30));
        assert!(ports.iter().any(|e| e.port == 50));
        assert!(ports.iter().any(|e| e.port == 100));
        assert!(ports.iter().any(|e| e.port == 350));
        assert!(ports.iter().any(|e| e.port == 851));
        assert!(ports.iter().any(|e| e.port == 10000));
    }
    // ------------------------------------------------------------
    // AdsClient Symbols
    {
        let target_task = AdsAddr::new(addr.id, 350);
        let target_grp: u32 = 0x08502000;

        let target_off: u32 = 0x8105dfe8;
        let target_val: bool = true;
        let val = client.read_bool(target_task, target_grp, target_off)?;
        assert_ne!(target_val, val);
        client.write_bool(target_task, target_grp, target_off, target_val)?;
        let val = client.read_bool(target_task, target_grp, target_off)?;
        assert_eq!(target_val, val);

        let target_off: u32 = 0x8105dfea;
        let target_val: i16 = 123;
        let val = client.read_i16(target_task, target_grp, target_off)?;
        assert_ne!(target_val, val);
        client.write_i16(target_task, target_grp, target_off, target_val)?;
        let val = client.read_i16(target_task, target_grp, target_off)?;
        assert_eq!(target_val, val);

        let target_off: u32 = 0x8105dfec;
        let target_val: u32 = 12345;
        let val = client.read_u32(target_task, target_grp, target_off)?;
        assert_ne!(target_val, val);
        client.write_u32(target_task, target_grp, target_off, target_val)?;
        let val = client.read_u32(target_task, target_grp, target_off)?;
        assert_eq!(target_val, val);

        let target_off: u32 = 0x8105dff0;
        let target_val: f64 = 123.456;
        let val = client.read_f64(target_task, target_grp, target_off)?;
        assert_ne!(target_val, val);
        client.write_f64(target_task, target_grp, target_off, target_val)?;
        let val = client.read_f64(target_task, target_grp, target_off)?;
        assert_eq!(target_val, val);

        let target_off: u32 = 0x8105dff8;
        let target_val: &str = "Hello World!";
        let val = client.read_utf8(target_task, target_grp, target_off, 32)?;
        assert_ne!(target_val, val);
        client.write_utf8(target_task, target_grp, target_off, target_val)?;
        let val = client.read_utf8(target_task, target_grp, target_off, 32)?;
        assert_eq!(target_val, val);
    }
    // ------------------------------------------------------------
    // Reconnect after restart
    //{
    //    let mut system_client = AdsSystemClient::new(client.clone(), addr.id);
    //    system_client.set_config()?;
    //    std::thread::sleep(std::time::Duration::from_secs(2));

    //    let adsState = client.reconnect(addr, 5000)?;
    //    assert_eq!(AdsState::Config, adsState);
    //}
    // ------------------------------------------------------------
    // Reconnect after reboot
    //{
    //    let mut system_client = AdsSystemClient::new(client.clone(), addr.id);
    //    system_client.set_reboot_delayed(4)?;
    //    std::thread::sleep(std::time::Duration::from_secs(5));

    //    let adsState = client.reconnect(addr, 60000)?;
    //    assert_eq!(AdsState::Config, adsState);
    //}
    // ------------------------------------------------------------
    Ok(())
}
