// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
use ads_def::{AdsBuffer, AdsNetId};
use ads_disco::{message::*, password::*};

const InfoRequest: [u8; 24] = [
    0x03, 0x66, 0x14, 0x71, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x10, 0x27, 0x00, 0x00, 0x00, 0x00,
];
const InfoResponse: [u8; 267] = [
    0x03, 0x66, 0x14, 0x71, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x80, 0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x10, 0x27, 0x04, 0x00, 0x00, 0x00, 0x05, 0x00, 0x0a, 0x00, 0x43, 0x50, 0x2d, 0x33, 0x41, 0x42, 0x33, 0x46, 0x34, 0x00, 0x04, 0x00, 0x94, 0x00, 0x94, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x54, 0x43, 0x2f, 0x42, 0x53, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x04, 0x00, 0x03, 0x01,
    0xb8, 0x0f, 0x12, 0x00, 0x41, 0x00, 0x37, 0x38, 0x31, 0x62, 0x61, 0x38, 0x64, 0x63, 0x35, 0x35, 0x37, 0x38, 0x37, 0x33, 0x61, 0x39, 0x33, 0x64, 0x30, 0x63, 0x31, 0x38, 0x64, 0x61, 0x31, 0x66, 0x63, 0x65, 0x37, 0x66, 0x62, 0x37, 0x63, 0x64, 0x62, 0x64, 0x64, 0x61, 0x30, 0x64, 0x30, 0x35, 0x35,
    0x32, 0x63, 0x30, 0x62, 0x34, 0x34, 0x65, 0x31, 0x30, 0x34, 0x65, 0x33, 0x61, 0x64, 0x65, 0x30, 0x39, 0x32, 0x37, 0x63, 0x63, 0x00,
];
const AddRouteRequest: [u8; 108] = [
    0x03, 0x66, 0x14, 0x71, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01, 0x10, 0x27, 0x05, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x0b, 0x00, 0x4e, 0x49, 0x4c, 0x53, 0x4a, 0x2d, 0x4e, 0x42, 0x30, 0x33, 0x00, 0x07, 0x00, 0x06, 0x00, 0x0f, 0x0c, 0x13, 0x56, 0x01, 0x01,
    0x0f, 0x00, 0x10, 0x00, 0xb3, 0xce, 0xfa, 0x9d, 0xf1, 0x4c, 0xdf, 0x60, 0xcc, 0x32, 0xdf, 0xef, 0x27, 0xe2, 0xd7, 0x91, 0x0e, 0x00, 0x10, 0x00, 0x6c, 0x16, 0xbf, 0x52, 0x20, 0x91, 0xed, 0x5c, 0xde, 0x07, 0x73, 0x49, 0xc3, 0x56, 0x31, 0x0b, 0x05, 0x00, 0x0f, 0x00, 0x31, 0x39, 0x32, 0x2e, 0x31,
    0x36, 0x38, 0x2e, 0x31, 0x37, 0x38, 0x2e, 0x32, 0x37, 0x00,
];
const AddRouteResponse: [u8; 32] = [
    0x03, 0x66, 0x14, 0x71, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x80, 0x05, 0x3a, 0xb3, 0xf4, 0x01, 0x01, 0x10, 0x27, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,
];

#[test]
fn info_req_from_slice() {
    let message = AdsDiscoMessage::from_slice(&InfoRequest).unwrap();
    assert_eq!(AdsDiscoServiceId::Info as u32, message.service); // Service
    assert_eq!(AdsNetId::new(15, 12, 19, 86, 1, 1), message.id); // NetId
    assert_eq!(10000, message.port); // Port
    assert_eq!(0, message.tags.len()); // TagCount
}

#[test]
fn info_req_into_bytes() {
    assert_eq!(InfoRequest.to_vec(), AdsDiscoMessage::new_info_request(AdsNetId::new(15, 12, 19, 86, 1, 1)).into_bytes().unwrap());
}

#[test]
fn info_req_decode() {
    let mut buf = AdsBuffer::from_slice(&InfoRequest);
    assert_eq!(AdsDiscoMagic, buf.dec_into_u32().unwrap()); // Cookie
    assert_eq!(0, buf.dec_into_u32().unwrap()); // Invoke
    assert_eq!(AdsDiscoServiceId::Info as u32, buf.dec_into_u32().unwrap()); // Service
    assert_eq!(AdsNetId::new(15, 12, 19, 86, 1, 1), buf.dec_into_type::<AdsNetId>().unwrap()); // NetId
    assert_eq!(10000, buf.dec_into_u16().unwrap()); // Port
    assert_eq!(0, buf.dec_into_u32().unwrap()); // TagCount
}

#[test]
fn info_res_from_slice() {
    let message = AdsDiscoMessage::from_slice(&InfoResponse).unwrap();
    assert_eq!(AdsDiscoServiceId::Info as u32 | AdsDiscoServiceId::Response as u32, message.service); // Service
    assert_eq!(AdsNetId::new(5, 58, 179, 244, 1, 1), message.id); // NetId
    assert_eq!(10000, message.port); // Port
    assert_eq!(4, message.tags.len()); // TagCount

    assert_eq!("CP-3AB3F4", message.get_host_name().unwrap());
    assert_eq!("TC/BSD (12.2.0)", message.get_os_version().unwrap());
    assert_eq!("3.1.4024", message.get_tc_version().unwrap());
    assert_eq!("781BA8DC557873A93D0C18DA1FCE7FB7CDBDDA0D0552C0B44E104E3ADE0927CC".to_lowercase(), message.get_fingerprint().unwrap());
}

#[test]
fn info_res_decode() {
    let mut buf = AdsBuffer::from_slice(&InfoResponse);
    assert_eq!(AdsDiscoMagic, buf.dec_into_u32().unwrap()); // Cookie
    assert_eq!(0, buf.dec_into_u32().unwrap()); // Invoke
    assert_eq!(AdsDiscoServiceId::Info as u32 | AdsDiscoServiceId::Response as u32, buf.dec_into_u32().unwrap()); // Service
    assert_eq!(AdsNetId::new(5, 58, 179, 244, 1, 1), buf.dec_into_type::<AdsNetId>().unwrap()); // NetId
    assert_eq!(10000, buf.dec_into_u16().unwrap()); // Port
    assert_eq!(4, buf.dec_into_u32().unwrap()); // TagCount

    // Tag#1
    assert_eq!(AdsDiscoTagId::Host as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(10, buf.dec_into_u16().unwrap()); // Length
    assert_eq!("CP-3AB3F4", buf.dec_into_utf8_len(10).unwrap()); // Hostname

    // Tag#2
    assert_eq!(AdsDiscoTagId::OsVersion as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(148, buf.dec_into_u16().unwrap()); // Length
    assert_eq!(148, buf.dec_into_u32().unwrap()); // Size
    assert_eq!(12, buf.dec_into_u32().unwrap()); // Major
    assert_eq!(2, buf.dec_into_u32().unwrap()); // Minor
    assert_eq!(0, buf.dec_into_u32().unwrap()); // Build
    assert_eq!(1, buf.dec_into_u32().unwrap()); // Platform
    assert_eq!("TC/BSD", buf.dec_into_utf8_len(128).unwrap()); // Info

    // Tag#3
    assert_eq!(AdsDiscoTagId::TcVersion as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(4, buf.dec_into_u16().unwrap()); // Length
    assert_eq!(3, buf.dec_into_u8().unwrap()); // Version
    assert_eq!(1, buf.dec_into_u8().unwrap()); // Revision
    assert_eq!(4024, buf.dec_into_u16().unwrap()); // Build

    // Tag#18
    assert_eq!(AdsDiscoTagId::Fingerprint as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(65, buf.dec_into_u16().unwrap()); // Length
    assert_eq!("781BA8DC557873A93D0C18DA1FCE7FB7CDBDDA0D0552C0B44E104E3ADE0927CC".to_lowercase(), buf.dec_into_utf8_len(64).unwrap().to_lowercase());
}

#[test]
fn add_route_req_from_slice() {
    let message = AdsDiscoMessage::from_slice(&AddRouteRequest).unwrap();
    assert_eq!(AdsDiscoServiceId::AddRoute as u32, message.service);
    assert_eq!(AdsNetId::new(15, 12, 19, 86, 1, 1), message.id);
    assert_eq!(10000, message.port);
    assert_eq!(5, message.tags.len());

    assert_eq!("NILSJ-NB03", message.get_route_name().unwrap());
    assert_eq!(AdsNetId::from([15, 12, 19, 86, 1, 1]), message.get_net_id().unwrap());
    //assert_eq!(AdsEncrypt::from("Administrator").data(), message.getUsernameEnc());
    //assert_eq!(AdsEncrypt::from("1").data(), message.getPasswordEnc());
    assert_eq!("192.168.178.27", message.get_host_name().unwrap());
}

#[test]
fn add_route_req_into_bytes() {
    // let mut message = AdsDiscoMessage::new_add_route_request(
    //    AdsNetId::new(15, 12, 19, 86, 1, 1),
    //    "NILSJ-NB03", "192.168.178.27",
    //    "Administrator", "1");

    let mut m = AdsDiscoMessage {
        invoke: 0,
        id: AdsNetId::new(15, 12, 19, 86, 1, 1),
        port: 10000,
        service: AdsDiscoServiceId::AddRoute as u32,
        tags: Vec::new(),
    };
    m.tags.push(AdsDiscoTag::RouteName(String::from("NILSJ-NB03")));
    m.tags.push(AdsDiscoTag::NetId(AdsNetId::new(15, 12, 19, 86, 1, 1)));
    m.tags.push(AdsDiscoTag::UsernameEnc(String::from("Administrator")));
    m.tags.push(AdsDiscoTag::PasswordEnc(String::from("1")));
    m.tags.push(AdsDiscoTag::HostName(String::from("192.168.178.27")));
    assert_eq!(AddRouteRequest.to_vec(), m.into_bytes().unwrap());
}

#[test]
fn add_route_req_decode() {
    let mut buf = AdsBuffer::from_slice(&AddRouteRequest);
    assert_eq!(AdsDiscoMagic, buf.dec_into_u32().unwrap()); // Cookie
    assert_eq!(0, buf.dec_into_u32().unwrap()); // Invoke
    assert_eq!(AdsDiscoServiceId::AddRoute as u32, buf.dec_into_u32().unwrap()); // Service
    assert_eq!(AdsNetId::new(15, 12, 19, 86, 1, 1), buf.dec_into_type::<AdsNetId>().unwrap()); // NetId
    assert_eq!(10000, buf.dec_into_u16().unwrap()); // Port
    assert_eq!(5, buf.dec_into_u32().unwrap()); // TagCount

    // Tag#1
    assert_eq!(AdsDiscoTagId::RouteName as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(11, buf.dec_into_u16().unwrap()); // Length
    assert_eq!("NILSJ-NB03", buf.dec_into_utf8_len(11).unwrap()); // RouteName

    // Tag#2
    assert_eq!(AdsDiscoTagId::NetId as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(6, buf.dec_into_u16().unwrap()); // Length
    assert_eq!(AdsNetId::from([15, 12, 19, 86, 1, 1]), buf.dec_into_type::<AdsNetId>().unwrap()); // NetId

    // Tag#3
    assert_eq!(AdsDiscoTagId::UsernameEnc as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(16, buf.dec_into_u16().unwrap()); // Length
    assert_eq!(AdsEncrypt::from("Administrator").data(), buf.dec_into_slice(16).unwrap()); // UsernameEnc

    // Tag#4
    assert_eq!(AdsDiscoTagId::PasswordEnc as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(16, buf.dec_into_u16().unwrap()); // Length
    assert_eq!(AdsEncrypt::from("1").data(), buf.dec_into_slice(16).unwrap()); // PasswordEnc

    // Tag#5
    assert_eq!(AdsDiscoTagId::Host as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(15, buf.dec_into_u16().unwrap()); // Length
    assert_eq!("192.168.178.27", buf.dec_into_utf8_len(15).unwrap()); // Hostname
}

#[test]
fn add_route_res_decode() {
    let mut buf = AdsBuffer::from_slice(&AddRouteResponse);
    assert_eq!(AdsDiscoMagic, buf.dec_into_u32().unwrap()); // Cookie
    assert_eq!(0, buf.dec_into_u32().unwrap()); // Invoke
    assert_eq!(AdsDiscoServiceId::AddRoute as u32 | AdsDiscoServiceId::Response as u32, buf.dec_into_u32().unwrap()); // Service
    assert_eq!(AdsNetId::new(5, 58, 179, 244, 1, 1), buf.dec_into_type::<AdsNetId>().unwrap()); // NetId
    assert_eq!(10000, buf.dec_into_u16().unwrap()); // Port
    assert_eq!(1, buf.dec_into_u32().unwrap()); // TagCount

    // Tag#1 Result
    assert_eq!(AdsDiscoTagId::Result as u16, buf.dec_into_u16().unwrap()); // ID
    assert_eq!(4, buf.dec_into_u16().unwrap()); // Length
    assert_eq!(0, buf.dec_into_u32().unwrap()); // Result
}
