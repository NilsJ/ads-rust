// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]

pub mod message;
pub mod password;

use crate::message::AdsDiscoMessage;
pub use ads_def::{AdsError, AdsNetId, AdsResult};
use log::debug;
use std::{
    error::Error,
    fmt::{self, Display},
    net::{IpAddr, Ipv4Addr, SocketAddr, UdpSocket},
    result::Result,
    time::Duration,
};

const AdsDiscoPort: u16 = 48899;
const AdsDiscoMaxSize: usize = 1536;

pub struct AdsDiscoInfo {
    pub addr: SocketAddr,
    pub net_id: AdsNetId,
    pub host_name: String,
    pub full_name: String,
    pub os_version: String,
    pub tc_version: String,
    pub fingerprint: String,
}

#[derive(Debug)]
pub enum AdsUdpError {
    Internal(String),
    Ads(ads_def::AdsError),
    Io(std::io::Error),
}

impl Error for AdsUdpError {}

impl Display for AdsUdpError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AdsUdpError::Internal(e) => write!(f, "{}", e),
            AdsUdpError::Ads(e) => e.fmt(f),
            AdsUdpError::Io(e) => e.fmt(f),
        }
    }
}

impl From<ads_def::AdsError> for AdsUdpError {
    fn from(e: ads_def::AdsError) -> AdsUdpError {
        AdsUdpError::Ads(e)
    }
}

impl From<std::io::Error> for AdsUdpError {
    fn from(e: std::io::Error) -> AdsUdpError {
        AdsUdpError::Io(e)
    }
}

pub type AdsUdpResult<T> = Result<T, AdsUdpError>;

pub struct AdsDiscoClient {
    socket: UdpSocket,
    id: AdsNetId,
}

impl AdsDiscoClient {
    pub fn new(id: AdsNetId) -> AdsUdpResult<Self> {
        //  bind_addr: Ipv4Addr => UdpSocket::bind((bind_addr, 0))?;
        let socket: UdpSocket = UdpSocket::bind(SocketAddr::from(([0, 0, 0, 0], 0)))?;
        socket.set_read_timeout(Some(Duration::from_secs(5)))?;
        socket.set_broadcast(true)?;

        Ok(AdsDiscoClient { socket, id })
    }

    pub fn local_addr(addr: Ipv4Addr) -> AdsUdpResult<Ipv4Addr> {
        // open a unused udp socket and bind it to a remote ip address to read the local ip address
        let socket = UdpSocket::bind("0.0.0.0:0")?;
        socket.connect((addr, AdsDiscoPort))?;
        match socket.local_addr()?.ip() {
            IpAddr::V4(ip) => Ok(ip),
            IpAddr::V6(ip) => ip.to_ipv4().ok_or(AdsUdpError::Internal(String::from("Invalid local address!"))),
        }
    }

    pub fn request(&self, addr: Ipv4Addr) -> AdsUdpResult<AdsDiscoInfo> {
        let request = AdsDiscoMessage::new_info_request(self.id).into_bytes()?;
        self.socket.send_to(&request, (addr, AdsDiscoPort))?;

        let mut response = [0u8; AdsDiscoMaxSize];
        let (n, addr) = self.socket.recv_from(&mut response)?;

        let info = AdsDiscoMessage::from_slice(&response[..n])?;
        Ok(AdsDiscoInfo {
            addr,
            net_id: info.id,
            host_name: info.get_host_name().unwrap_or_default(),
            full_name: info.get_full_name().unwrap_or_default(),
            os_version: info.get_os_version().unwrap_or_default(),
            tc_version: info.get_tc_version().unwrap_or_default(),
            fingerprint: info.get_fingerprint().unwrap_or_default(),
        })
    }

    pub fn broadcast(&self, addr: Ipv4Addr) -> AdsUdpResult<Vec<AdsDiscoInfo>> {
        let request = AdsDiscoMessage::new_info_request(self.id).into_bytes()?;
        self.socket.send_to(&request, (addr, AdsDiscoPort))?;

        let mut devices = Vec::<AdsDiscoInfo>::new();
        let mut response = [0u8; AdsDiscoMaxSize];
        while let Ok((n, addr)) = self.socket.recv_from(&mut response) {
            let info = AdsDiscoMessage::from_slice(&response[..n])?;
            devices.push(AdsDiscoInfo {
                addr,
                net_id: info.id,
                host_name: info.get_host_name().unwrap_or_default(),
                full_name: info.get_full_name().unwrap_or_default(),
                os_version: info.get_os_version().unwrap_or_default(),
                tc_version: info.get_tc_version().unwrap_or_default(),
                fingerprint: info.get_fingerprint().unwrap_or_default(),
            });
        }
        Ok(devices)
    }

    pub fn add_route(&self, addr: Ipv4Addr, route_name: &str, route_addr: &str, user_name: &str, user_pass: &str) -> AdsUdpResult<()> {
        let request = AdsDiscoMessage::new_add_route_request(self.id, route_name, route_addr, user_name, user_pass, true).into_bytes()?;
        self.socket.send_to(&request, (addr, AdsDiscoPort))?;

        let mut response = [0u8; AdsDiscoMaxSize];
        let (n, recv_addr) = self.socket.recv_from(&mut response)?;
        match recv_addr.ip() {
            IpAddr::V4(recv_addr) => {
                if addr != recv_addr {
                    return Err(AdsUdpError::Internal(String::from("Received response with wrong address!")));
                }
            }
            IpAddr::V6(_) => {
                return Err(AdsUdpError::Internal(String::from("Received response with wrong address!")));
            }
        }
        let response = AdsDiscoMessage::from_slice(&response[..n])?;
        match response.get_result() {
            Some(0) => Ok(()),
            Some(e) => {
                debug!("AdsDiscoClient.add_route: {:?}", &response);
                Err(AdsUdpError::Ads(AdsError::from(e)))
            }
            None => {
                debug!("AdsDiscoClient.add_route: {:?}", &response);
                Err(AdsUdpError::Ads(AdsError::from(0x0001C)))
            }
        }
    }
}
