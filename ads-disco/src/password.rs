// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)]
use aes::{
    cipher::{generic_array::GenericArray, BlockEncrypt, KeyInit},
    Aes128,
};

// ADS_DISCO_PWDKEY, GUID_AES1_DISCO_PWDKEY, GUID_AES2_DISCO_PWDKEY
const AdsPassKey: [u8; 16] = [0x6A, 0xC9, 0x0D, 0x67, 0x62, 0x6A, 0xE7, 0x3C, 0x32, 0x6D, 0xF6, 0x88, 0xF9, 0x1A, 0xC7, 0x72];

pub struct AdsEncrypt(Vec<u8>);

impl AdsEncrypt {
    pub fn from(str: &str) -> AdsEncrypt {
        let cipher = Aes128::new_from_slice(&AdsPassKey).unwrap();
        let mut vec = str.as_bytes().to_vec();
        vec.resize(16, 0);
        let mut block = GenericArray::clone_from_slice(&vec);
        cipher.encrypt_block(&mut block);
        AdsEncrypt(block.as_slice().try_into().unwrap())
    }

    pub fn from_data(data: Vec<u8>) -> AdsEncrypt {
        let cipher = Aes128::new_from_slice(&AdsPassKey).unwrap();
        let mut block = GenericArray::clone_from_slice(&data);
        cipher.encrypt_block(&mut block);
        AdsEncrypt(block.as_slice().try_into().unwrap())
    }

    pub fn data(&self) -> &[u8] {
        &self.0
    }
}
