// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)]
use crate::password::AdsEncrypt;
use ads_def::{AdsBuffer, AdsCodable, AdsError, AdsNetId, AdsResult};
use log::debug;

pub const AdsDiscoMagic: u32 = 0x71146603;

#[repr(u32)]
pub enum AdsDiscoServiceId {
    Info = 1,
    AddRoute = 6,
    //DelRoute = 7,
    //ReadRoutes = 8,
    Response = 0x80000000,
}

#[repr(u16)]
pub enum AdsDiscoTagId {
    Result = 1,         // UDP_DISCO_TAG_RESULT
    Password = 2,       // UDP_DISCO_TAG_PASSWORD
    TcVersion = 3,      // UDP_DISCO_TAG_TCVERSION
    OsVersion = 4,      // UDP_DISCO_TAG_TARGETOS
    Host = 5,           // UDP_DISCO_TAG_COMPUTERNAME
    Description = 6,    // UDP_DISCO_TAG_DESCRIPTION
    NetId = 7,          // UDP_DISCO_TAG_NETID
    IpAddress = 8,      // UDP_DISCO_TAG_IPADDR
    Temporary = 9,      // UDP_DISCO_TAG_TEMPORARY
    RouteIndex = 10,    // UDP_DISCO_TAG_ROUTEINDEX
    RouteInfo = 11,     // UDP_DISCO_TAG_ROUTEINFO
    RouteName = 12,     // UDP_DISCO_TAG_ROUTENAME
    Username = 13,      // UDP_DISCO_TAG_USERNAME
    PasswordEnc = 14,   // UDP_DISCO_TAG_PASSWORD_AES
    UsernameEnc = 15,   // UDP_DISCO_TAG_USERNAME_AES
    MaxFragment = 16,   // UDP_DISCO_TAG_MAXFRAGMENT
    SecurityFlags = 17, // UDP_DISCO_TAG_SECURITYFLAGS
    Fingerprint = 18,   // UDP_DISCO_TAG_SSCERT_FINGERPRINT
    SearchServer = 19,  // UDP_DISCO_TAG_SEARCH_SERVER
    FullName = 20,      // UDP_DISCO_TAG_FQDNNAME
}

#[derive(Debug)]
pub enum AdsDiscoTag {
    Unknown(u16),
    Result(u32),
    TcVersion(String),
    OsVersion(String),
    HostName(String),
    NetId(AdsNetId),
    Temporary(bool),
    RouteName(String),
    PasswordEnc(String),
    UsernameEnc(String),
    Fingerprint(String),
    FullName(String),
}

impl Default for AdsDiscoTag {
    fn default() -> Self {
        AdsDiscoTag::Unknown(0)
    }
}

impl AdsCodable for AdsDiscoTag {
    fn size(&self) -> usize {
        4 + match self {
            AdsDiscoTag::Unknown(_) => 0,
            AdsDiscoTag::Result(_) => 4,
            AdsDiscoTag::TcVersion(_) => 4,
            AdsDiscoTag::OsVersion(_) => 128,
            AdsDiscoTag::HostName(v) => v.len() + 1,
            AdsDiscoTag::NetId(v) => v.size(),
            AdsDiscoTag::Temporary(_) => 1,
            AdsDiscoTag::RouteName(v) => v.len() + 1,
            AdsDiscoTag::PasswordEnc(v) => v.len() + 1,
            AdsDiscoTag::UsernameEnc(v) => v.len() + 1,
            AdsDiscoTag::Fingerprint(v) => v.len() + 1,
            AdsDiscoTag::FullName(v) => v.len() + 1,
        }
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        match self {
            AdsDiscoTag::Result(v) => Self::enc_tag_u32(buf, 1, v),
            AdsDiscoTag::NetId(v) => Self::enc_tag_net_id(buf, 7, v),
            AdsDiscoTag::Temporary(v) => Self::enc_tag_bool(buf, 9, v),
            AdsDiscoTag::HostName(v) => Self::enc_tag_str(buf, 5, v),
            AdsDiscoTag::RouteName(v) => Self::enc_tag_str(buf, 12, v),
            AdsDiscoTag::PasswordEnc(v) => Self::enc_tag_encrypt(buf, 14, v),
            AdsDiscoTag::UsernameEnc(v) => Self::enc_tag_encrypt(buf, 15, v),
            _ => {
                debug!("AdsDiscoTag.encode: {:?}", self);
                Err(AdsError::from(0x0706))
            }
        }
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        let tag_id = buf.dec_into_u16()?;
        let tag_len = buf.dec_into_u16()? as usize;
        *self = match tag_id {
            1 => AdsDiscoTag::Result(Self::dec_tag_u32(buf, tag_len)?),
            3 => AdsDiscoTag::TcVersion(Self::dec_tag_tc_version(buf, tag_len)?),
            4 => AdsDiscoTag::OsVersion(Self::dec_tag_os_version(buf, tag_len)?),
            5 => AdsDiscoTag::HostName(Self::dec_tag_utf8(buf, tag_len)?),
            7 => AdsDiscoTag::NetId(Self::dec_tag_net_id(buf, tag_len)?),
            9 => AdsDiscoTag::Temporary(Self::dec_tag_bool(buf, tag_len)?),
            12 => AdsDiscoTag::RouteName(Self::dec_tag_utf8(buf, tag_len)?),
            18 => AdsDiscoTag::Fingerprint(Self::dec_tag_utf8(buf, tag_len)?),
            20 => AdsDiscoTag::FullName(Self::dec_tag_utf8(buf, tag_len)?),
            _ => {
                debug!("AdsDiscoTag.decode: id={}", tag_id);
                buf.skip(tag_len);
                AdsDiscoTag::Unknown(tag_id)
            }
        };
        Ok(())
    }
}

impl AdsDiscoTag {
    fn enc_tag_bool(buf: &mut AdsBuffer, id: u16, val: &bool) -> AdsResult<()> {
        buf.enc_u16(id)?;
        buf.enc_u16(1)?;
        buf.enc_bool(*val)
    }

    fn enc_tag_u32(buf: &mut AdsBuffer, id: u16, val: &u32) -> AdsResult<()> {
        buf.enc_u16(id)?;
        buf.enc_u16(4)?;
        buf.enc_u32(*val)
    }

    fn enc_tag_str(buf: &mut AdsBuffer, id: u16, val: &str) -> AdsResult<()> {
        buf.enc_u16(id)?;
        buf.enc_u16(val.len() as u16 + 1)?;
        buf.enc_utf8(val)
    }

    fn enc_tag_encrypt(buf: &mut AdsBuffer, id: u16, val: &str) -> AdsResult<()> {
        let bytes = AdsEncrypt::from(val);
        buf.enc_u16(id)?;
        buf.enc_u16(16)?;
        buf.enc_slice(bytes.data())
    }

    fn enc_tag_net_id(buf: &mut AdsBuffer, id: u16, val: &AdsNetId) -> AdsResult<()> {
        buf.enc_u16(id)?;
        buf.enc_u16(6)?;
        buf.enc_type(val)
    }

    fn dec_tag_bool(buf: &mut AdsBuffer, len: usize) -> AdsResult<bool> {
        if len != 1 {
            buf.skip(len);
            return Err(AdsError::from(0x0705));
        }
        buf.dec_into_bool()
    }

    fn dec_tag_u32(buf: &mut AdsBuffer, len: usize) -> AdsResult<u32> {
        if len != 4 {
            buf.skip(len);
            return Err(AdsError::from(0x0705));
        }
        buf.dec_into_u32()
    }

    fn dec_tag_utf8(buf: &mut AdsBuffer, len: usize) -> AdsResult<String> {
        buf.dec_into_utf8_len(len)
    }

    fn dec_tag_tc_version(buf: &mut AdsBuffer, len: usize) -> AdsResult<String> {
        if len != 4 {
            buf.skip(len);
            return Err(AdsError::from(0x0705));
        }
        let version = buf.dec_into_u8()?;
        let revision = buf.dec_into_u8()?;
        let build = buf.dec_into_u16()?;
        Ok(format!("{}.{}.{}", version, revision, build))
    }

    fn dec_tag_net_id(buf: &mut AdsBuffer, len: usize) -> AdsResult<AdsNetId> {
        if len != 6 {
            buf.skip(len);
            return Err(AdsError::from(0x0705));
        }
        buf.dec_into_type::<AdsNetId>()
    }

    fn dec_tag_os_version(buf: &mut AdsBuffer, len: usize) -> AdsResult<String> {
        if len < 4 {
            buf.skip(len);
            return Err(AdsError::from(0x0705));
        }
        let size = buf.dec_into_u32()? as usize;
        if len < size {
            buf.skip(len);
            return Err(AdsError::from(0x0705));
        }
        let major = buf.dec_into_u32()?;
        let minor = buf.dec_into_u32()?;
        let build = buf.dec_into_u32()?;
        let version = format!("{}.{}.{}", major, minor, build);
        let platform = buf.dec_into_u32()?;
        let info = buf.dec_into_utf8_len(size - 20)?;
        if len > size {
            println!("OsVersion: len={}, size={}", len, size);
            buf.skip(len - size);
        }
        match platform {
            2 => {
                // MD_OS_WIN32_NT
                match major {
                    10 => Ok(format!("Windows 10 ({})", version)),
                    6 => match minor {
                        3 => Ok(String::from("Windows 8.1")),
                        2 => Ok(String::from("Windows 8")),
                        1 => Ok(String::from("Windows 7")),
                        _ => Ok(format!("Windows NT ({})", version)),
                    },
                    _ => Ok(format!("Windows NT ({})", version)),
                }
            }
            3 => {
                // MD_OS_WIN32_CE
                Ok(format!("Windows CE ({})", version))
            }
            _ => Ok(format!("{} ({})", info, version)),
        }
    }
}

#[derive(Default, Debug)]
pub struct AdsDiscoMessage {
    pub invoke: u32,
    pub id: AdsNetId,
    pub port: u16,
    pub service: u32,
    pub tags: Vec<AdsDiscoTag>,
}

impl AdsCodable for AdsDiscoMessage {
    fn size(&self) -> usize {
        let mut size = 16;
        for tag in &self.tags {
            size += 4;
            size += tag.size();
        }
        size
    }
    fn encode(&self, buf: &mut AdsBuffer) -> AdsResult<()> {
        buf.enc_u32(AdsDiscoMagic)?;
        buf.enc_u32(self.invoke)?;
        buf.enc_u32(self.service)?;
        buf.enc_type(&self.id)?;
        buf.enc_u16(self.port)?;

        buf.enc_u32(self.tags.len() as u32)?;
        for tag in &self.tags {
            buf.enc_type(tag)?;
        }
        Ok(())
    }
    fn decode(&mut self, buf: &mut AdsBuffer) -> AdsResult<()> {
        let magic = buf.dec_into_u32()?;
        if magic != AdsDiscoMagic {
            debug!("AdsDiscoMessage.decode: magic={:#X}", magic);
            return Err(AdsError::from(0x001C));
        }
        buf.dec_u32(&mut self.invoke)?;
        buf.dec_u32(&mut self.service)?;
        buf.dec_type(&mut self.id)?;
        buf.dec_u16(&mut self.port)?;

        let tag_count = buf.dec_into_u32()?;
        self.tags.clear();
        for _ in 0..tag_count {
            let tag = buf.dec_into_type::<AdsDiscoTag>()?;
            self.tags.push(tag);
        }
        Ok(())
    }
}

impl AdsDiscoMessage {
    pub fn new_info_request(id: AdsNetId) -> Self {
        AdsDiscoMessage {
            invoke: 0,
            id,
            port: 10000,
            service: AdsDiscoServiceId::Info as u32,
            tags: Vec::new(),
        }
    }

    pub fn new_add_route_request(id: AdsNetId, name: &str, addr: &str, user: &str, pass: &str, temp: bool) -> Self {
        let mut m = AdsDiscoMessage {
            invoke: 0,
            id,
            port: 10000,
            service: AdsDiscoServiceId::AddRoute as u32,
            tags: Vec::new(),
        };
        m.add_tag(AdsDiscoTag::NetId(id));
        m.add_tag(AdsDiscoTag::RouteName(String::from(name)));
        m.add_tag(AdsDiscoTag::HostName(String::from(addr)));
        m.add_tag(AdsDiscoTag::UsernameEnc(String::from(user)));
        m.add_tag(AdsDiscoTag::PasswordEnc(String::from(pass)));
        if temp {
            m.add_tag(AdsDiscoTag::Temporary(true));
        }
        m
    }

    pub fn from_buf(buf: &mut AdsBuffer) -> AdsResult<AdsDiscoMessage> {
        buf.dec_into_type::<AdsDiscoMessage>()
    }

    pub fn from_slice(data: &[u8]) -> AdsResult<AdsDiscoMessage> {
        Self::from_buf(&mut AdsBuffer::from_slice(data))
    }

    pub fn from_vec(data: Vec<u8>) -> AdsResult<AdsDiscoMessage> {
        Self::from_buf(&mut AdsBuffer::from_vec(data))
    }

    pub fn into_bytes(&self) -> AdsResult<Vec<u8>> {
        let mut buf = AdsBuffer::default();
        buf.enc_type(self)?;
        Ok(buf.to_vec())
    }

    pub fn add_tag(&mut self, tag: AdsDiscoTag) {
        self.tags.push(tag)
    }

    pub fn get_result(&self) -> Option<u32> {
        for tag in &self.tags {
            if let AdsDiscoTag::Result(ref x) = tag {
                return Some(*x);
            }
        }
        None
    }

    pub fn get_tc_version(&self) -> Option<String> {
        for tag in &self.tags {
            if let AdsDiscoTag::TcVersion(ref x) = tag {
                return Some(x.to_string());
            }
        }
        None
    }

    pub fn get_os_version(&self) -> Option<String> {
        for tag in &self.tags {
            if let AdsDiscoTag::OsVersion(ref x) = tag {
                return Some(x.to_string());
            }
        }
        None
    }

    pub fn get_host_name(&self) -> Option<String> {
        for tag in &self.tags {
            if let AdsDiscoTag::HostName(ref x) = tag {
                return Some(x.to_string());
            }
        }
        None
    }

    pub fn get_net_id(&self) -> Option<AdsNetId> {
        for tag in &self.tags {
            if let AdsDiscoTag::NetId(ref x) = tag {
                return Some(*x);
            }
        }
        None
    }

    pub fn get_route_name(&self) -> Option<String> {
        for tag in &self.tags {
            if let AdsDiscoTag::RouteName(ref x) = tag {
                return Some(x.to_string());
            }
        }
        None
    }

    pub fn get_fingerprint(&self) -> Option<String> {
        for tag in &self.tags {
            if let AdsDiscoTag::Fingerprint(ref x) = tag {
                return Some(x.to_string());
            }
        }
        None
    }

    pub fn get_full_name(&self) -> Option<String> {
        for tag in &self.tags {
            if let AdsDiscoTag::FullName(ref x) = tag {
                return Some(x.to_string());
            }
        }
        None
    }
}
