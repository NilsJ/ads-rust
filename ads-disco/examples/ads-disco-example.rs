// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(non_upper_case_globals)]
use ads_def::AdsNetId;
use ads_disco::{AdsDiscoClient, AdsDiscoInfo, AdsUdpError, AdsUdpResult};
use std::{
    env::args,
    error::Error,
    net::{IpAddr, Ipv4Addr, SocketAddr},
    result::Result,
};
use stderrlog::*;

fn print_device(device: &AdsDiscoInfo) {
    println!("# {}", device.addr);
    println!("\tNetID: {}", device.net_id);
    println!("\tHost: {}", device.host_name);
    if !device.full_name.is_empty() {
        println!("\tFullName: {}", device.full_name);
    }
    if !device.os_version.is_empty() {
        println!("\tOsVersion: {}", device.os_version);
    }
    if !device.tc_version.is_empty() {
        println!("\tTcVersion: {}", device.tc_version);
    }
    if !device.fingerprint.is_empty() {
        println!("\tFingerprint: {}", device.fingerprint);
    }
}

fn add_route(client: &AdsDiscoClient, target: SocketAddr, name: &str, addr: &str, user: &str, pass: &str) -> AdsUdpResult<()> {
    let ip = match target.ip() {
        IpAddr::V4(ip) => ip,
        IpAddr::V6(ip) => ip.to_ipv4().ok_or(AdsUdpError::Internal(String::from("Invalid target address!")))?,
    };
    client.add_route(ip, name, addr, user, pass)
}

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let args: Vec<String> = args().collect();
    if args.len() <= 1 {
        panic!("Usage: AdsDisco <ip> [<host> <user> <pass>]");
    }

    let target_ip = args[1].as_str().parse::<Ipv4Addr>()?;
    let local_ip = AdsDiscoClient::local_addr(target_ip)?;
    let local_id = AdsNetId::from_ipv4(&local_ip);

    let host = if args.len() > 2 { args[2].as_str() } else { "" };
    let user = if args.len() > 3 { args[3].as_str() } else { "" };
    let pass = if args.len() > 4 { args[4].as_str() } else { "" };
    let do_add = !host.is_empty() && !user.is_empty() && !pass.is_empty();

    let client = AdsDiscoClient::new(local_id)?;
    if target_ip.octets()[3] == 255 {
        let devices = client.broadcast(target_ip)?;
        for device in devices.iter() {
            print_device(device);
            if do_add {
                match add_route(&client, device.addr, host, &local_ip.to_string(), user, pass) {
                    Ok(()) => println!("\t=> Add route succeeded"),
                    Err(e) => eprintln!("\t=> Add route failed with result {}", e),
                }
            }
        }
    } else {
        let device = client.request(target_ip)?;
        print_device(&device);
        if do_add {
            match add_route(&client, device.addr, host, &local_ip.to_string(), user, pass) {
                Ok(()) => println!("\t=> Add route succeeded"),
                Err(e) => eprintln!("\t=> Add route failed with result {}", e),
            }
        }
    }
    Ok(())
}
