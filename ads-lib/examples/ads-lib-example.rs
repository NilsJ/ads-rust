// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_lib::{AdsAddr, AdsClient, AdsClientExt, AdsLibClient};
use std::error::Error;
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let addr = AdsAddr::default();
    let mut client = AdsLibClient::new();

    println!("# State");
    let (ads_state, dev_state) = client.read_state(addr)?;
    println!("AdsState: {}", ads_state);
    println!("DevState: {}", dev_state);

    println!("# SystemState");
    let time = client.read_u64(addr, 400, 7)?;
    println!("Timestamp: {}", time);

    Ok(())
}
