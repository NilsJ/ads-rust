// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_lib::{AdsAddr, AdsClient, AdsClientExt, AdsLibClient};
use std::{error::Error, str::FromStr};
use stderrlog::*;

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new().verbosity(5).timestamp(Timestamp::Millisecond).init().unwrap();

    let ads_addr = AdsAddr::from_str("5.69.183.127.1.1:10000")?;

    println!("# Client");
    let mut client = AdsLibClient::new();
    println!("Local: {}", client.get_local()?);
    println!("Target: {}", ads_addr);

    println!("# State");
    let (ads_state, dev_state) = client.read_state(ads_addr)?;
    println!("AdsState: {}", ads_state);
    println!("DevState: {}", dev_state);

    println!("# SystemState");
    let time = client.read_u64(ads_addr, 400, 7)?;
    println!("Timestamp: {}", time);

    Ok(())
}
