// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_lib::ffi::*;
use ads_lib::AdsAddr;

#[test]
fn get_dll_version() {
    let version = unsafe { AdsGetDllVersion() };
    #[cfg(target_family = "windows")]
    assert_eq!(0x29001F, version);
    #[cfg(target_family = "unix")]
    assert_eq!(0x1001f, version);
}

#[test]
fn port_open_close() {
    let port = unsafe { AdsPortOpenEx() };
    assert_ne!(0, port);
    let result = unsafe { AdsPortCloseEx(port) };
    assert_eq!(0, result);
}

#[test]
fn get_local_address() {
    let port = unsafe { AdsPortOpenEx() };
    assert_ne!(0, port);
    let mut addr = AdsAddr::default();
    let result = unsafe { AdsGetLocalAddressEx(port, &mut addr) };
    assert_eq!(0, result);

    assert_ne!(0, addr.id.byte(0));
    assert_ne!(0, addr.id.byte(1));
    assert_ne!(0, addr.id.byte(2));
    assert_ne!(0, addr.id.byte(3));
    assert_eq!(1, addr.id.byte(4));
    assert_eq!(1, addr.id.byte(5));
    assert_ne!(0, addr.port);
}

#[test]
fn read_state() {
    let port = unsafe { AdsPortOpenEx() };
    assert_ne!(0, port);
    let addr = AdsAddr::default();
    let mut ads_state: u16 = 0;
    let mut dev_state: u16 = 0;
    let result = unsafe { AdsSyncReadStateReqEx(port, &addr, &mut ads_state, &mut dev_state) };
    assert_eq!(0, result);
    assert_eq!(15, ads_state);
    assert_eq!(1, dev_state);
}
