// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
use ads_def::{AdsAddr, AdsNetId, AdsState};
use ads_lib::{AdsClient, AdsLibClient};

#[test]
fn get_local_port() {
    let client = AdsLibClient::new();
    let port = client.get_local_port().unwrap();
    assert_ne!(0, port);
}

#[test]
fn get_local_addr() {
    let client = AdsLibClient::new();
    let port = client.get_local_port().unwrap();
    let addr = client.get_local_addr().unwrap();
    assert_ne!(0, addr.id.byte(0));
    assert_ne!(0, addr.id.byte(1));
    assert_ne!(0, addr.id.byte(2));
    assert_ne!(0, addr.id.byte(3));
    assert_eq!(1, addr.id.byte(4));
    assert_eq!(1, addr.id.byte(5));
    assert_eq!(port, addr.port);
}

#[test]
fn read_state() {
    let mut client = AdsLibClient::new();
    let addr = AdsAddr { id: AdsNetId::new(0, 0, 0, 0, 1, 1), port: 10000 };
    let result = client.read_state(addr);
    assert!(result.is_ok());
    if let Ok((ads_state, dev_state)) = result {
        assert_eq!(AdsState::Config, ads_state);
        assert_eq!(1, dev_state);
    }
}
