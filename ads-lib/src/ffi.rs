// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
extern crate libc;
use ads_def::{AdsAddr, AdsVersion};
use libc::{c_uchar, c_uint, c_ushort};

#[link(name = "TcAdsDll")]
extern "C" {
    pub fn AdsGetDllVersion() -> c_uint;
    pub fn AdsPortOpenEx() -> c_uint;
    pub fn AdsPortCloseEx(port: c_uint) -> c_uint;
    pub fn AdsGetLocalAddressEx(port: c_uint, addr: *const AdsAddr) -> c_uint;
    pub fn AdsSyncWriteReqEx(port: c_uint, addr: *const AdsAddr, grp: c_uint, off: c_uint, len: c_uint, data: *const c_uchar) -> c_uint;
    pub fn AdsSyncReadReqEx2(port: c_uint, addr: *const AdsAddr, grp: c_uint, off: c_uint, len: c_uint, data: *mut c_uchar, readen: *mut c_uint) -> c_uint;
    pub fn AdsSyncReadWriteReqEx2(port: c_uint, addr: *const AdsAddr, grp: c_uint, off: c_uint, read_len: c_uint, read_data: *mut c_uchar, write_len: c_uint, write_data: *const c_uchar, readen: *mut c_uint) -> c_uint;
    pub fn AdsSyncReadDeviceInfoReqEx(port: c_uint, addr: *const AdsAddr, name: *mut c_uchar, version: *mut AdsVersion) -> c_uint;
    pub fn AdsSyncWriteControlReqEx(port: c_uint, addr: *const AdsAddr, ads_state: c_ushort, dev_state: c_ushort, len: c_uint, data: *const c_uchar) -> c_uint;
    pub fn AdsSyncReadStateReqEx(port: c_uint, addr: *const AdsAddr, ads_state: *mut c_ushort, dev_state: *mut c_ushort) -> c_uint;
    pub fn AdsSyncSetTimeoutEx(port: c_uint, timeout_ms: c_uint) -> c_uint;
    pub fn AdsSyncGetTimeoutEx(port: c_uint, timeout_ms: *mut c_uint) -> c_uint;
    //pub fn AdsAmsPortEnabledEx(port: c_uint, enabled: *mut c_uint) -> c_uint;
}
