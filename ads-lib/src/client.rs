// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2022 Nils Johannsen <n.johannsen@beckhoff.com>
#![allow(dead_code)] // TODO
use crate::ffi::*;
use ads_def::{AdsAddr, AdsClient, AdsError, AdsName, AdsResult, AdsState, AdsVersion, DevState};
use log::debug;

pub struct AdsLibClient {
    port: u32,
}

impl AdsLibClient {
    pub fn new() -> Self {
        let port = unsafe { AdsPortOpenEx() };
        assert_ne!(0, port, "Open ADS Port failed!");
        AdsLibClient { port }
    }

    pub fn get_local_port(&self) -> AdsResult<u16> {
        Ok(self.port as u16)
    }

    pub fn get_local_addr(&self) -> AdsResult<AdsAddr> {
        let addr = AdsAddr::default();
        let result = unsafe { AdsGetLocalAddressEx(self.port, &addr) };
        if result != 0 {
            debug!("AdsLibClient.get_local_addr: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(addr)
        }
    }

    pub fn set_timeout(&mut self, timeout_ms: u32) -> AdsResult<()> {
        let result = unsafe { AdsSyncSetTimeoutEx(self.port, timeout_ms) };
        if result != 0 {
            debug!("AdsLibClient.get_timeout: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(())
        }
    }

    pub fn get_timeout(&mut self, timeout_ms: &mut u32) -> AdsResult<()> {
        let result = unsafe { AdsSyncGetTimeoutEx(self.port, timeout_ms as *mut u32) };
        if result != 0 {
            debug!("AdsLibClient.get_timeout: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(())
        }
    }
}

impl Drop for AdsLibClient {
    fn drop(&mut self) {
        if self.port != 0 {
            unsafe { AdsPortCloseEx(self.port) };
        }
    }
}

impl Default for AdsLibClient {
    fn default() -> Self {
        Self::new()
    }
}

impl AdsClient for AdsLibClient {
    fn get_local(&self) -> AdsResult<AdsAddr> {
        let addr = AdsAddr::default();
        let result = unsafe { AdsGetLocalAddressEx(self.port, &addr) };
        if result != 0 {
            debug!("AdsLibClient.get_local: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(addr)
        }
    }

    fn read_info(&mut self, target: AdsAddr) -> AdsResult<(AdsName, AdsVersion)> {
        let mut name = [0u8; 32];
        let mut version = AdsVersion::default();
        let result = unsafe { AdsSyncReadDeviceInfoReqEx(self.port, &target, &mut name as *mut u8, &mut version) };
        if result != 0 {
            debug!("AdsLibClient.read_info: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            let mut name = std::str::from_utf8(&name)?;
            if let Some(n) = name.find(char::from(0)) {
                name = &name[..n];
            }
            Ok((name.to_string(), version))
        }
    }

    fn read_state(&mut self, target: AdsAddr) -> AdsResult<(AdsState, DevState)> {
        let mut ads_state: u16 = 0;
        let mut dev_state: u16 = 0;
        let result = unsafe { AdsSyncReadStateReqEx(self.port, &target, &mut ads_state, &mut dev_state) };
        if result != 0 {
            debug!("AdsLibClient.read_state: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok((AdsState::from(ads_state), dev_state))
        }
    }

    fn write_control(&mut self, target: AdsAddr, ads_state: AdsState, dev_state: DevState, data: &[u8]) -> AdsResult<()> {
        let result = unsafe { AdsSyncWriteControlReqEx(self.port, &target, ads_state as u16, dev_state as u16, data.len() as u32, data.as_ptr()) };
        if result != 0 {
            debug!("AdsLibClient.write_control: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(())
        }
    }

    fn read(&mut self, target: AdsAddr, grp: u32, off: u32, data: &mut [u8]) -> AdsResult<usize> {
        let mut readen: u32 = 0;
        let result = unsafe { AdsSyncReadReqEx2(self.port, &target, grp, off, data.len() as u32, data.as_mut_ptr(), &mut readen) };
        if result != 0 {
            debug!("AdsLibClient.read: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(readen as usize)
        }
    }

    fn write(&mut self, target: AdsAddr, grp: u32, off: u32, data: &[u8]) -> AdsResult<()> {
        let result = unsafe { AdsSyncWriteReqEx(self.port, &target, grp, off, data.len() as u32, data.as_ptr()) };
        if result != 0 {
            debug!("AdsLibClient.write: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(())
        }
    }

    fn read_write(&mut self, target: AdsAddr, grp: u32, off: u32, write_data: &[u8], read_data: &mut [u8]) -> AdsResult<usize> {
        let mut readen: u32 = 0;
        let result = unsafe { AdsSyncReadWriteReqEx2(self.port, &target, grp, off, read_data.len() as u32, read_data.as_mut_ptr(), write_data.len() as u32, write_data.as_ptr(), &mut readen) };
        if result != 0 {
            debug!("AdsLibClient.read_write: {}", AdsError::from(result));
            Err(AdsError::from(result))
        } else {
            Ok(readen as usize)
        }
    }
}
