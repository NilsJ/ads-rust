# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Nils Johannsen <n.johannsen@beckhoff.com>

all: build

build:
	@mkdir -p ./target/debug/deps
	@cp -f /usr/local/lib/libTcAdsDll.so ./target/debug/deps/libTcAdsDll.so
	@cargo build

release:
	@mkdir -p ./target/release/deps
	@cp -f /usr/local/lib/libTcAdsDll.so ./target/release/deps/libTcAdsDll.so
	@cargo build --release

check: build
	@cargo check
	@cargo clippy

test: build
	@cargo test

clean:
	@rm -rf ./target

.PHONY: all build
